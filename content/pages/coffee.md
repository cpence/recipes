---
title: Current Coffee Methods
author: pencechp
url: /coffee/
---

This page will be continually updated, as I refine and alter my current favorite
brewing methods for coffee. (I take this probably far, far too seriously.)

### Pour-Over

For most coffees that I tend to drink (lighter to medium roasts, often Latin
American), I've been finding that the following produces the best results.
(**Update:** I've increased my dosing by about 4g of coffee over the last couple
of years.) Get your scale, your stopwatch, filters, kettle, and beans:

*   **Grind:** Slightly coarser than table salt, a 14 on my old Baratza Maestro
    Plus or a 16 on my new Encore is usually right. Adjust it to make the brew
    timing come out right below (we're looking for a three-minute brew process).
*   **Weight:** 26g
*   **Water:** at 92C/200F [Thankfully, my current source of tap water makes
    phenomenal coffee, so I'm not doing anything to adjust mineral content.]
*   **Method:** [This comes from the "Barismo" method [over at Prima
    Coffee](https://prima-coffee.com/blog/v60-brewmethod-review), though it
    looks like Barismo themselves have now changed their tune. Oh well. It's
    been working fantastically for me.]
    1.  Fold cone filter and rinse, as per usual.
    2.  Grind coffee and add to cone. [I have a knock-off porcelain model, not
        even a real V60.]
    3.  Bloom the coffee with 60g water, waiting for 35 seconds to let the process finish.
    4.  Slowly (aiming for 45 seconds) pour in 180g of water. Don't pour right
        down the side of the filter (leaving the grounds intact down the sides),
        but do circulate your pouring to wet all the grounds in the middle.
    5.  Wait for 30 seconds for the water to draw down.
    6.  Repeat step 4, for a total of 360g of water.
    7.  Wait for 35 seconds. If the grind is right, that's what it should take
        for all of the water to filter through. I often wind up over-extracting
        (running too long), but this drives the coffee in an acidic direction
        that I actually rather like. (Sue me.)

### Aeropress

It took me a really long time to get this thing to make a coffee that I like,
but I finally did, thanks particularly to some chats with Bryce Huebner on
Twitter.

*   **Grind:** A bit finer than for pour-over, but I do this with the ceramic
    grinder in my office so I don't have very clear parameters there
*   **Weight:** 20g
*   **Water:** just off the boil (don't have a variable-temp kettle in the
    office)
*   **Method:**
    1.  Add enough water to bloom the coffee, wait for 30 seconds
    2.  Add 200g of water, and start the timer. Put on the plunger, then pull
        back just a bit to set a vacuum lock. (I don't flip the thing over; I'm
        too worried I will detonate coffee all over my office.)
    3.  Wait until the clock reads 1:45.
    4.  Push down *very* slowly, until 2:15.

### Iced Pour-Over

I spent a long time living in the Southern US, where iced coffee is the order of
the day, about nine months of the year. This method is *delicious.* I owe it to
the fine folks over at [Counter
Culture.](https://counterculturecoffee.com/learn/brewing-guides/iced-coffee)

*   **Grind:** Same as for pour-over, 14 on the Maestro Plus, 16 on the Encore
*   **Weight:** 30g
*   **Water:** at 92C/200F
*   **Method:**
    1.  Get a large glass tumbler; you're going to wind up with a bit more than
        500 mL of iced coffee when you're done, and you'll want to add more ice
        to it after that.
    2.  Fold cone filter and rinse.
    3.  Grind coffee and add to cone.
    4.  Place your tumbler on your scale and add 165g of ice to it. Zero the
        scale.
    5.  Bloom the coffee with enough water to wet it fully. Wait 30 or 45
        seconds to let it finish.
    6.  Slowly (aiming for around 2:30) pour in water until the scale reads a
        total of 335g.
    7.  That should almost exactly melt the initial load of ice in the cup.
    8.  Add more ice, and serve.

### Iced Aeropress

I haven't much tested this, but it seems great so I'm saving it for posterity.
Borrowed from [Serious
Eats.](https://drinks.seriouseats.com/2012/09/best-iced-coffee-cold-brew-aeropress-technique-tips.html)

*   **Grind:** Same as for pour-over
*   **Weight:** 15g
*   **Water:** at 92C/100F
*   **Method:**
    1.  Put 4 ounces of ice into your glass.
    2.  Put the plunger in the Aeropress and invert it (plunger down, filter
        up). Add the ground coffee.
    3.  Add 4 ounces (120 mL) of water to the grounds. Stir. Screw on the filter
        cap with a filter.
    4.  Wait for 1:30.
    5.  Invert over the cup with ice and press gently (aiming for 30 seconds).
    6.  Stir well, and you should have a cup of iced coffee.

### French Press Cold Brew

I got used to having around cold brew or cold-brew concentrate after I got
hooked on [coffee mocktails like this one here]( {{< relref
"coffee-ginger-mocktail" >}} ). I don't have a Filtron brewer yet (though I may
get one soon), so I've been using my French Press.

*   **Grind:** French-press grade, something like a 35 on any of the Baratza
    grinders.
*   **Weight:** A 1:7 ratio with your water. (FIXME: Haven't made a full
    french-press worth yet, so I don't know these numbers for my actual full
    press.)
*   **Water:** Cold. Duh.
*   **Method:**
    1.  Grind.
    2.  Pour beans into french press.
    3.  Add cold water.
    4.  Give one good stir.
    5.  Let sit somewhere for at least 16 but not more than 18 hours.
    6.  Plunge, extremely gently. Try to avoid hitting the bottom, and whatever
        you do, don't squeeze on the grounds at the bottom.
    7.  Pour off gently into something, and store it for up to a week or two in
        the refrigerator.
    8.  To serve, treat this as *slightly* concentrated cold brew: always dilute
        with some ice and either a splash of water or milk.

### Grinds for Other Methods

Other methods of brewing don't really need me to keep track of anything but how
much coffee to use and which grinder setting to put it on.

*   **3-cup Moka Pot:** 20g, grinder 14 (*n.b.* I'm still dialing this one in,
    not yet very happy with my Moka performance!)
*   **8-cup Chemex:** 42g (I keep the Chemex at my office, so the grind is the
    pourover grind that I've tweaked from my [Porlex
    Mini](https://www.porlexgrinders.com/).)
    -   I like the Stumptown method for the brew itself: bloom with 150g, then
        add water to 450g, wait for it to come down a bit, then add water to
        700g (which should be right at the very brim).

