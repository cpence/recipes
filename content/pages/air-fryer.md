---
title: Air-Fryer Guidelines
author: pencechp
---

We have a fancy [combination air-fryer/toaster oven/microwave
device.](https://www.samsung.com/be_fr/microwave-ovens/convection/mw8000r-hotblast-mc35r8088cc-en/)
Here’s where we keep recipe notes about how we can best use it for standard
preps.

- **Toast:** Put the crisping plate on either of the grills. Air fryer only,
  200C. Preheat until it beeps, then cook for 4 minutes, flipping halfway
  through.
- **Bacon:** Put the crisping plate on the higher grill. Broiler + microwave
  (600W). Preheat for 3 minutes, then cook for 2-4 minutes.


