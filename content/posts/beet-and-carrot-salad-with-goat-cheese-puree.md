---
title: "Beet and Carrot Salad with Goat Cheese Puree"
author: "pencechp"
date: "2017-03-30"
categories:
  - side dish
tags:
  - beet
  - carrot
  - chevre
  - salad
---

## Ingredients

_Puree_

*   1 pound cauliflower florets
*   3/4 cup heavy cream, warmed
*   8 ounce goat cheese

_Beets_

*   2 pounds beets
*   4 parts red wine vinegar
*   3 parts extra virgin olive oil

_Carrots_

*   1 pound carrots, shaved on a mandolin

_Shallots_

*   2 shallots, peeled and thinly sliced into rings
*   1/2 cup vegetable oil

_Oil_

*   2 parts canola oil
*   1 part cumin seeds

## Preparation

_Cauliflower:_ Simmer cauliflower in water with salt 10 minutes or until tender. Drain cauliflower and purée with the warm cream and goat cheese until smooth.

_Beets:_ Preheat oven to 350F. Place beets in foil and roast 40 minutes or until tender. Cool, peel and cut into quarters. Combine vinegar and olive oil. Pour over beets. Marinate 2 hours.

_Shallots:_ Slowly fry until golden brown, strain and salt

_Cumin oil:_ Toast cumin seeds in a skillet until fragrant, about 8 minutes. Place in a blender with the oil, blend and strain. Toss with carrots.

To serve, arrange beets on top of cauliflower puree. Top with carrots and shallots.

_Edible Nashville_
