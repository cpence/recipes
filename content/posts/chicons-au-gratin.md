---
title: "Chicons au gratin"
author: "pencechp"
date: "2018-12-26"
categories:
  - main course
  - side dish
tags:
  - belgian
---

## Ingredients

*   8 endives
*   8 slices of ham
*   40cl milk
*   flour
*   150g butter
*   300g grated Gruyere cheese
*   1 tsp sugar
*   grated nutmeg
*   salt and pepper

## Preparation

Wash the endives and remove any damaged outer leaves. Trim the bottom of the endives and remove the bitter root base. Heat 50g of butter in a saucepan and add the endives with a few drops of water and the sugar. Leave to braise for 15 minutes lid on. The endives are cooked when tender and when they start to caramelize slightly. Remove them from the saucepan and leave them to drain for half an hour. In the meantime, prepare a Béchamel sauce and stir in some of the endive cooking juice. Season with salt, pepper and nutmeg, and add 200g of the grated Gruyere cheese little by little. Wrap each endive in a slice of ham. Put them into a gratin dish. Pour the Béchamel sauce over the wrapped endives and place the gratin dish in a 180°C oven for about 20 minutes. After 10 minutes in the oven, add the remaining grated Gruyere cheese over the preparation and leave until the sauce is bubbly and golden brown.

_visit.brussels_
