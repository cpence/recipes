---
title: "Vegetarian Cassoulet"
author: "pencechp"
date: "2010-01-08"
categories:
  - main course
tags:
  - bean
  - french
  - stew
  - vegan
  - vegetarian
---

## Ingredients

_For cassoulet:_

*   3 medium leeks (white and pale green parts only)
*   4 medium carrots, halved lengthwise and cut into 1-inch-wide pieces
*   3 celery ribs, cut into 1-inch-wide pieces
*   4 garlic cloves, chopped 1/4 cup olive oil
*   4 thyme sprigs
*   2 parsley sprigs
*   1 Turkish or 1/2 California bay leaf
*   1/8 teaspoon ground cloves
*   3 (19-ounce) cans cannellini or Great Northern beans, rinsed and drained
*   1 quart water

_For garlic crumbs:_

*   4 cups coarse fresh bread crumbs from a baguette
*   1/3 cup olive oil
*   1 tablespoon chopped garlic
*   1/4 cup chopped parsley

## Preparation

_Make cassoulet:_

Halve leeks lengthwise and cut crosswise into 1/2-inch pieces, then wash well and pat dry.

\[To add 8 oz. bacon, slice, then fry until fat has rendered.  Then add veggies and proceed with the recipe, omitting the 1/4 c. olive oil.\]

Cook leeks, carrots, celery, and garlic in oil with herb sprigs, bay leaf, cloves, and 1/2 teaspoon each of salt and pepper in a large heavy pot over medium heat, stirring occasionally, until softened and golden, about 15 minutes. Stir in beans, then water, and simmer, partially covered, stirring occasionally, until carrots are tender but not falling apart, about 30 minutes.

_Make garlic crumbs while cassoulet simmers:_

Preheat oven to 350°F with rack in middle.

Toss bread crumbs with oil, garlic, and 1/4 teaspoon each of salt and pepper in a bowl until well coated. Spread in a baking pan and toast in oven, stirring once halfway through, until crisp and golden, 12 to 15 minutes.

Cool crumbs in pan, then return to bowl and stir in parsley.

_Finish cassoulet:_

Discard herb sprigs and bay leaf. Mash some of beans in pot with a potato masher or back of a spoon to thicken broth. Season with salt and pepper. Just before serving, sprinkle with garlic crumbs.

_Gourmet, March 2008_
