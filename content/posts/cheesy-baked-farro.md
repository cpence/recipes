---
title: "Cheesy Baked Farro"
author: "pencechp"
date: "2012-05-23"
categories:
  - side dish
tags:
  - cheese
  - farro
---

## Ingredients

_Sauce:_

*   1/2 stick (1/4 cup) unsalted butter
*   1/4 cup all-purpose flour
*   2 cups warm whole milk
*   Kosher salt and freshly ground black pepper

_Farro:_

*   2 1/2 cups grated Parmesan
*   1 cup grated Gruyere
*   1/2 cup fontina cheese, grated
*   6 cups chicken broth
*   2 cups faro or barley, rinsed and drained
*   1 teaspoon chopped fresh thyme leaves
*   Kosher salt and freshly ground black pepper
*   1/2 cup plain dried bread crumbs
*   Olive oil, for drizzling

[**N.B.:** The recipe size here is for a 13x9" dish, which is *a lot* given how
heavy it is. You should strongly consider halving it.]

## Preparation

Preheat the oven to 400 degrees F. Spray a 13 by 9-inch baking dish with cooking
spray.

For the sauce: In a 2-quart saucepan, melt the butter over medium heat. Add the
flour and whisk until smooth. Gradually add the warm milk, whisking constantly
to prevent lumps. Simmer over medium heat, whisking constantly, until the sauce
is thick and smooth, about 8 minutes (do not allow the mixture to boil). Remove
from the heat and season with salt and pepper, to taste.

For the farro: In a large bowl, add the cheeses and stir to combine. Remove 1/2
cup of the mixture and reserve. In a large stock pot, add the chicken broth and
bring to a boil over medium-high heat. Add the farro, reduce heat and simmer,
stirring occasionally, until the faro is tender, about 25 minutes. Drain, if
necessary. Add the farro, thyme, and sauce to the bowl with the cheese. Stir
until combined and season with salt and pepper, to taste. Pour the mixture into
the prepared baking dish and top with the reserved 1/2 cup of cheese. Sprinkle
the top with bread crumbs and drizzle with olive oil.

Bake until the top is golden brown and forms a crust, about 25 to 30
minutes. Remove from oven and let stand for 5 minutes before serving.

_Giada de Laurentiis, FoodNetwork.com_
