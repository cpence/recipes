---
title: "Green Chile Chicken Stew"
author: "pencechp"
date: "2012-12-20"
categories:
  - main course
  - side dish
tags:
  - chicken
  - corn
  - greenchile
  - soup
---

## Ingredients

*   1 lg. onion, diced
*   3 cloves garlic, minced
*   2 chicken breasts, diced
*   1 lg. baking potato, diced (optional)
*   2 c. corn kernels
*   14 oz. can diced tomato
*   4 green chiles, diced (vary based on heat)
*   1 can cream of chicken soup
*   shredded jack cheese (optional)
*   fried tortilla strips (optional)

## Preparation

Sautée the onion and garlic in butter or oil.  Add the chicken and brown.  Add the potato, cover with water, and simmer until the potato is tender.  Add corn and/or tomato, green chile, and the cream of chicken soup.  Simmer for a while to blend the flavors.  Top with jack cheese and tortilla strips, if you're serving it as a main course.
