---
title: "Lemongrass Grilled Shrimp"
author: "pencechp"
date: "2013-12-04"
categories:
  - main course
tags:
  - shrimp
  - untested
---

## Ingredients

*   1 lb tiger prawns or jumbo prawns, peeled, deveined, tails intact
*   6 metal skewers or bamboo kewers (soak in water before using)
*   Oil, for brushing

_Marinade_

*   2 tablespoons fish sauce
*   1 lemongrass, white part only, grated
*   1 tablespoon powdered sugar
*   1 teaspoon Sriracha
*   1 (big) clove garlic, finely minced

_Chili-Calamansi Dipping Sauce (optional)_

*   1 1/2 tablespoons chili garlic sauce
*   1 tablespoon water
*   1/2 tablespoon chopped cilantro leaves
*   1 small calamansi, extract the juice (or 1 wedge lime)

## Preparation

Clean the shrimp with cold running water. Pat dry with paper towels and transfer the shrimp into a bowl.

Add all the ingredients in the Marinade into the bowl. Stir to combine well with the shrimp. Marinate for 15 minutes. Thread three shrimp into each skewer. Brush the surface of the shrimp with some oil. Grill the shrimp on both sides until they are charred and cooked through. Serve immediately with the dipping sauce (optional).

_Rasa Malaysia_
