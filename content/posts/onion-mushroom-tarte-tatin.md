---
title: "Onion Mushroom Tarte Tatin"
date: 2020-01-27T18:47:45+01:00
categories:
    - main course
    - side dish
    - appetizer
tags:
    - onion
    - pie
    - mushroom
---

## Ingredients

*   3 tablespoons extra-virgin olive oil
*   4 or 5 small yellow onions, cut in half through ends
*   8 ounces frozen/defrosted puff pastry dough
*   1 1/2 tablespoons dark brown sugar
*   Kosher salt
*   8 ounces mushrooms, preferably a mix, such as chanterelles, king trumpet and oyster, stemmed, rinsed well and chopped if large
*   1/2 cup walnut halves
*   6 ounces blue cheese, preferably Cabrales

## Preparation

Heat the oil in a 9-inch, heavy, ovenproof skillet (not nonstick) over medium heat. Once the oil shimmers, arrange the onion halves around the pan, cut sides down, with their stem ends pointing toward the center. Place one at the center of the pan. Reduce the heat to medium-low; cook for about 20 minutes, until softened and colored slightly on the bottom.

Preheat the oven to 375 degrees. Unfold the puff pastry dough and roll out between two sheets of wax paper or parchment paper to a thin, 10-inch round; it’s okay if a foldover seam is showing or if the dough is not completely smooth.

Sprinkle the brown sugar around the pan with the onions, then season lightly with salt. Once the brown sugar looks like it is melting into the oil, distribute the mushroom pieces and walnuts around, using them to fill in nooks and crannies. Remove from the heat.

Follow suit with the pinches of the blue cheese, dotting the pan with them.

Discard the top parchment from the puff pastry dough. Invert the dough over the contents of the pan, pressing and tucking the dough around the edges in the pan. Transfer to the oven; roast (middle rack) for 25 to 35 minutes, turning the pan from front to back halfway through to promote even pastry browning.

The pastry should look flaky and nicely browned, with a mixure underneath that you can hear sizzling. Transfer to a wire rack to cool for 5 minutes, then invert a serving plate over the pastry.

Carefully invert the pan to reveal the tarte tatin; if some pieces do not quite make the transfer, just pop them back into place, straightening up the onions etc, as needed. The mixture should be nicely caramelized; if not, slide the tarte tatin onto a foil-lined baking sheet and roast for another 5 minutes or so.

Cut into wedges; serve warm.

*WaPo/BBC*

