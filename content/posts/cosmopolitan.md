---
title: "Cosmopolitan"
author: "pencechp"
date: "2009-12-05"
categories:
  - cocktails
tags:
  - vodka
---

## IBA Standard Recipe

*good for sweeter juices, like cran-pomegranate*

*   3 meas. vodka (technically, citron vodka)
*   1 meas. Cointreau [JP: or Chambord]
*   1 meas. fresh lime juice
*   2 meas. cranberry juice

## CP Standard Recipe

*   2 1/2 meas. vodka
*   1 1/2 meas. Cointreau
*   1 meas. fresh lime juice
*   2 meas. cranberry juice

## 100% Cranberry Juice

*   40 mL vodka
*   30 mL 100% cranberry juice
*   20 mL orange liqueur
*   10 mL lime juice
