---
title: "Lemon or Orange Marmalades"
author: "pencechp"
date: "2014-01-24"
categories:
  - miscellaneous
tags:
  - jam
  - untested
---

## Ingredients

_For all:_

*   2 pounds lemons, scrubbed and cut lengthwise into 8 wedges
*   1/4 c. fresh lemon juice
*   10 half-pint canning jars with lids and rings, sterilized

_Lemon marmalade:_

*   2 lb. lemons, scrubbed and cut lengthwise into 8 wedges, then seeded and thinly sliced crosswise
*   8 1/2 c. sugar (3 3/4 lb.)

_Meyer lemon marmalade:_

*   2 lb. Meyer lemons, scrubbed and cut lengthwise into 8 wedges, then seeded and thinly sliced crosswise
*   7 1/2 c. sugar (3 1/4 lb.)

_Valencia orange marmalade:_

*   2 lb. Valencia oranges, scrubbed and cut lengthwise into 8 wedges, then seeded and thinly sliced crosswise
*   8 c. sugar (3 1/2 lb.)

## Preparation

_Day 1:_

_For all of the marmalades:_ in a large, nonreactive saucepan, cover the first 2 lb. of lemons with 2 inches of water (about 8 cups). Leave to stand at room temperature overnight.

_For the orange marmalade:_ In another nonreactive saucepan, cover the orange wedges with 2 inches of water (about 8 cups) and leave to stand at room temperature overnight.

_Day 2:_

_For all of the marmalades: _Bring the lemon wedges to a boil. Simmer over moderate heat, stirring every 30 minutes, until the lemons are very tender and the liquid is reduced by half, about 2 hours and 15 minutes. Pour the lemon wedges into a fine sieve set over a large heatproof bowl; let cool completely. Wrap the sieve and bowl with plastic and let drain overnight at room temperature; discard the lemon wedges.

_For the lemon marmalade:_ In a large nonreactive saucepan, cover the lemon slices with 2 inches of water (about 8 cups) and bring to a boil. Simmer over moderately high heat for 5 minutes, stirring occasionally. Drain the lemon slices in a fine strainer; discard the cooking liquid. Return the lemon slices to the saucepan and cover with 1 inch of water (about 4 cups). Bring to a boil and simmer over moderate heat, stirring occasionally, until the lemons are very tender and the liquid is slightly reduced, about 40 minutes; let stand at room temperature overnight.

_For the Meyer lemon marmalade:_ In a large saucepan, cover the Meyer lemon slices with 1 inch of water (about 4 cups) and let stand at room temperature overnight.

_For the orange marmalade: _Bring the oranges to a boil and simmer over moderate heat, stirring occasionally, until the oranges are very tender and the liquid is slightly reduced, about 40 minutes. Let stand covered at room temperature overnight.

_Day 3:_

_For all the marmalades:_ Add the strained lemon-wedge liquid to the slices in the saucepan. Stir in the sugar and lemon juice and bring to a boil. Simmer over moderate heat, without stirring, until the marmalade darkens slightly, about 30 minutes; skim off any foam as necessary. Test for doneness: Spoon 1 tablespoon of the marmalade onto a chilled plate and refrigerate until it is room temperature, about 3 minutes; the marmalade is ready when it thickens like jelly and a spoon leaves a trail when dragged through it. If not, continue simmering and testing every 10 minutes until it passes the test, up to 1 hour and 30 minutes.

Spoon the marmalade into the canning jars, leaving 1/4 inch of space at the top. Screw on the lids. Using canning tongs, lower the jars into a large pot of boiling water and boil for 15 minutes. Remove the jars with the tongs and let stand until the lids seal (they will look concave). Store the marmalade in a cool, dark place for up to 6 months.

_Food and Wine, December 2013_
