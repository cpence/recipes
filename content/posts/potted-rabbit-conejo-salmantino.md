---
title: "Potted Rabbit (Conejo Salmantino)"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - rabbit
  - spanish
---

## Ingredients

*   2 tbl olive oil
*   2 1/2 - 3 lb rabbit, cut in small serving pieces
*   1/4 c vinegar
*   1 med onion, finely chopped
*   salt and pepper
*   1 bay leaf
*   1/2 head garlic, separated but unpeeled
*   arugula
*   3 tbsp olive oil
*   1 1/2 tsp lemon juice

## Preparation

Heat the oil in a deep casserole, preferably earthenware, and brown the rabbit on all sides. Add the vinegar, onion, salt, pepper, bay leaf, and garlic. Cover tightly and cook in the oven at 300-325 F for 2 h, uncover for the last 15 min. Most of the liquid should be evaporated. If it evaporates too quickly, add a little water.

Dress the arugula with 3 tsp olive oil, lemon juice, and salt and pepper to taste.

Serve the rabbit over the arugula.
