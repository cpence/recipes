---
title: "Smashed Chinese Cucumber Salad"
author: "pencechp"
date: "2015-07-31"
categories:
  - side dish
tags:
  - chinese
  - cucumber
  - salad
---

## Ingredients

*   5 cucumbers
*   1/2-1 tsp salt
*   2 cloves garlic, minced
*   1 tsp sesame oil
*   1/2 tsp granulated sugar
*   1 tsp red pepper flakes

## Preparation

1\. Slice ends off cucumbers and then smash them until they break \[CP: just beat them with a chef's knife until they break apart\]. Cut the cucumbers into thirds. Then slice into bite sized pieces approximately 1/2 inch wide and 1 inch long.

2\. Add in all the remaining ingredients and mix until cucumbers are evenly coated in seasonings. Taste and adjust as needed. You can eat right away or let the cucumbers sit in the fridge to further develop the flavors.

_Kirbie Cravings_
