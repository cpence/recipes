---
title: "Hearty Meat Ragu"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - beef
  - crockpot
  - untested
---

## Ingredients

*   2 lb boneless stewing beef, cut into 1 in cubes
*   2 tbl olive oil
*   1 lg onion, finely chopped
*   1 med carrot, finely chopped
*   1 med celery rib with leaves, finely chopped
*   4 garlic cloves, chopped
*   1 c dry red wine
*   28 oz can crushed tomatoes in juice
*   28 oz can crushed tomatoes in puree
*   1 1/2 tsp dried basil
*   1 1/2 tsp dried oregano
*   1/2 tsp crushed red pepper flakes
*   salt and pepper to taste
*   pasta for serving

## Preparation

Heat oil in a large skillet over medium-high heat. Add the meat in batches and brown on all sides, being careful not to crowd the pan. With a slotted spoon, transfer to crockpot.

Pour off all but 2 tbl fat from the pan or add more if needed. Add the onion, carrot, celery and garlic, and cook, stirring occasionally until the vegetables soften, about 5 min. Add wine and bring to a boil, stirring to release the browned bits in the skillet. Pour into crock pot. Add the tomatoes and spices.

Cover and turn on to low setting. Cook without removing lid for 6 to 7 hours until meat is tender. Season to taste with salt and pepper and serve over pasta.

_Joy of Cooking_
