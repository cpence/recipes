---
title: "Banana Walnut Bread"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - bread
  - breakfast
tags:
  - banana
  - untested
  - walnut
---

## Ingredients

*   2 cups flour
*   1 tsp baking soda
*   1/4 tsp salt
*   1 egg
*   1 1/8 c sugar
*   1/2 c vegetable oil
*   2 tbsp buttermilk
*   1/2 tsp vanilla
*   3 ripe medium-large bananas, mashed
*   1/2 + 1/3 cup chopped walnuts

## Preparation

Preheat the oven to 325º. Grease a 9 x 5 x 3 loaf pan and dust with flour. Blend together the flour, baking soda and salt and set aside. Mix together the egg, sugar and vegetable oil until combined. Add the flour mixture and when blended add the buttermilk, vanilla and mashed bananas and mix until combined. Fold in 1/2 cup chopped walnuts and pour batter into prepared loaf pan. Top batter with remaining 1/3 cup chopped nuts. Bake for 45 to 60 minutes, until a toothpick inserted in the center comes out clean. Cool for 10 minutes on a wire reack before removing from pan.

_Starbucks_
