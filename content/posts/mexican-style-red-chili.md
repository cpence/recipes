---
title: "Mexican-Style Red Chili"
author: "pencechp"
date: "2015-03-13"
categories:
  - main course
tags:
  - chile
  - mexican
  - newmexican
  - stew
---

## Ingredients

*   3 medium ancho chiles, toasted and ground
*   3 tablespoons NM red chili (medium)
*   2 tablespoons cumin seeds, toasted and ground
*   2 teaspoons Mexican oregano
*   7 1/2 c. water
*   4 lb. beef chuck roast, cut into 1" cubes
*   2 tsp. salt
*   1/2 lb. bacon, chopped
*   1 medium onion, chopped
*   5 cloves garlic, chopped
*   4-5 small jalapeños \[CP: yes, that many\], cored, seeded, and minced
*   1 c. crushed tomatoes
*   2 tbsp. lime juice
*   5 tbsp. masa

## Preparation

Mix chili powder, cumin, oregano, and 1/2 cup water to form paste. Toss beef cubes with salt.

Fry bacon in large Dutch oven over medium-low heat until fat renders and bacon crisps, about 10 minutes. Remove bacon with slotted spoon and drain. Pour all but 2 teaspoons grease into small bowl; set aside. Increase heat to medium-high; brown on all sides, about 5 minutes per batch, adding additional bacon fat as necessary. Reduce heat to medium, add 3 tbsp. bacon fat to empty pan. Add onion; sauté until softened, 5 to 6 minutes. Add garlic and jalapeños, sauté 1 minute. Add chili paste, sauté 2 to 3 minutes. Add bacon and beef, tomatoes, lime juice, and 7 c. water, bring to simmer. Cook at a steady simmer until meat is tender and juices are dark, rich, and starting to thicken, about 2 hours.

Mix masa with 2/3 cup water in a small bowl. Increase heat to medium; stir in paste and simmer until thickened, 5 to 10 minutes. Add salt and pepper to taste.
