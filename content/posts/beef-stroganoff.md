---
title: "Beef Stroganoff"
author: "pencechp"
date: "2010-04-02"
categories:
  - main course
tags:
  - beef
  - pasta
  - pence
---

## Ingredients

*   butter
*   1 medium onion
*   an equivalent amount of mushrooms (around 1 box, or 2 small cartons pre-cut mushrooms)
*   salt and pepper to taste
*   1 lb, thin-cut round steak
*   2 c. container sour cream
*   3 tbsp. flour
*   white wine (around ½c)
*   1 box noodles

## Preparation

1\. Cook a box of noodles according to package directions, stir a little butter through to keep them from sticking, and set aside.

2\. Sautee mushrooms and onions in a few tablespoons of butter. Set aside.

3\. Cut the steak into strips, and brown it in a few tablespoons of butter.

4\. Return the mushroom and onion to the pan, and add the wine. Bring back to a boil, and simmer for a few minutes to cook down the sauce.

5\. Add a few spoonfuls of sour cream from the container to the pan, stirring to combine with the beef juices.

6\. Take a few tablespoons of flour (around three), and mix them into the rest of the container of the sour cream. Stir completely to prevent lumps.

7\. Stir the container of sour cream (with flour) into the pan.

8\. Bring the sauce back to a simmer to allow the flour to thicken the sauce, and serve it on the noodles.
