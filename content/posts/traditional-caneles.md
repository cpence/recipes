---
title: "Traditional Canelés"
author: "pencechp"
date: "2015-09-05"
categories:
  - dessert
tags:
  - custard
---

## Ingredients

*   4 c. milk
*   50 mL (3 1/4 tbsp.) butter
*   1 vanilla bean
*   2 c. pastry flour
*   2 c. sugar
*   1 whole egg
*   9 egg yolks
*   50 mL rum

## Preparation

Heat the milk, butter, and vanilla bean (split) to 115F (just warm), and leave to steep. In a bowl, mix the flour and sugar. Add the egg yolks and whole egg. Mix all with the liquid mixture. Chill for 24 hours.

Just before cooking, add the rum and remove the vanilla pod. Pour into canelé moulds. Cook at 280F for 15 minutes, then raise the temperature to 355F and cook for 55 minutes more.

This prepares about 60 mini canelés.

_That shop where we got the canelé moulds, Bordeaux_
