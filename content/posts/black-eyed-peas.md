---
title: "Black-Eyed Peas"
author: "pencechp"
date: "2010-08-10"
categories:
  - side dish
tags:
  - bean
---

## Ingredients

*   1 lb. black-eyed peas, picked over
*   2 tbsp. rendered bacon fat
*   1 small onion, peeled

## Preparation

In a bowl combine black-eyed peas with water to cover and let stand overnight.

Drain peas and in a 4-quart saucepan, combine with water to cover by 2 inches. Add bacon fat and onion. Simmer mixture, covered, 30 to 40 minutes, or until peas are tender, and drain well, discarding onion.

_Gourmet, February 1995_
