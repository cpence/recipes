---
title: "Sage Tea"
author: "pencechp"
date: "2014-09-02"
categories:
  - miscellaneous
tags:
  - mediterranean
  - tea
---

## Ingredients

*   2 teaspoons loose black tea
*   8 dried sage leaves
*   sugar, for serving

## Preparation

Bring 6 cups water to a boil in a medium saucepan. Remove the pan from the heat, add the tea and sage leaves, and cover the pan. Wrap a kitchen towel around the pan and set it aside for 10 minutes to allow the tea to steep. Then strain the liquid into a teapot and serve hot, with a bowl of sugar on the side.

_Epicurious_
