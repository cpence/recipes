---
title: "Lemon Caper Vinaigrette"
author: "pencechp"
date: "2011-03-06"
categories:
  - miscellaneous
tags:
  - asparagus
  - dressing
  - lemon
---

## Ingredients

*   2 tbsp. finely chopped shallots
*   2 tbsp. lemon zest
*   2 tbsp. lemon juice
*   1 tbsp. capers, drained
*   2 tbsp. chopped parsley
*   3 tbsp. olive oil
*   salt and pepper

## Preparation

Mix the shallots, lemon zest and juice, capers, and parsley in a small bowl.  Whisk in the olive oil, and season to taste with salt and pepper.

\[CP: This is particularly good on asparagus.\]

_Dole_
