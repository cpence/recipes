---
title: "Cobb Salad"
author: "pencechp"
date: "2015-12-28"
categories:
  - main course
tags:
  - bacon
  - dressing
  - salad
---

## Ingredients

_Dressing:_

*   3⁄4 cup canola oil
*   1⁄4 cup extra-virgin olive oil
*   1⁄4 cup red wine vinegar
*   1 tbsp. fresh lemon juice
*   3⁄4 tsp. dry mustard
*   1⁄2 tsp. Worcestershire
*   1⁄4 tsp. sugar
*   1 clove garlic, minced
*   Kosher salt and freshly ground black pepper, to taste

_Salad:_

*   1⁄2 head iceberg lettuce, cored and shredded
*   1⁄2 head romaine lettuce, chopped
*   1⁄2 bunch watercress, some of the stems trimmed, chopped
*   2 oz. blue cheese, preferably Roquefort, crumbled
*   6 strips cooked bacon, roughly chopped
*   3 hard-boiled eggs, peeled and cut into 1⁄2" cubes
*   2 medium tomatoes, peeled, seeded, and cut into 1⁄2" cubes
*   1 boneless skinless chicken breast, cooked and cut into 1⁄2" cubes
*   1 avocado, peeled, pitted, and cut into 1⁄2" cubes
*   Kosher salt and freshly ground black pepper, to taste
*   2 tbsp. minced chives

## Preparation

Make the dressing: Combine the canola oil, olive oil, vinegar, lemon juice, mustard, Worcestershire, sugar, and garlic in a blender. Purée the ingredients to make a smooth dressing and season with salt and pepper. Set the dressing aside (or refrigerate, covered, for up to 1 week).

Make the salad: On a large platter, combine the iceberg and romaine lettuces along with the watercress. Arrange the blue cheese, bacon, eggs, tomatoes, chicken, and avocado on top of the greens in neat rows. To serve, drizzle salad with dressing, season with salt and pepper, and top with chives. Alternatively, toss everything together in a bowl.

_Saveur_
