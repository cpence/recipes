---
title: "Alton Brown's Private Cheeseburger"
author: "pencechp"
date: "2015-10-07"
categories:
  - main course
tags:
  - hamburger
  - sandwich
  - untested
---

## Ingredients (per hamburger)

*   3 ounces (86-88 grams) ground beef (<=80% lean)
*   1 1/4 ounces (36-40 grams) grated cheddar cheese (mixture of sharp and mild)
*   1/4 teaspoon smoked paprika
*   1/4 teaspoon garlic powder
*   1 hamburger bun or (soft) Kaiser roll
*   4-5 dill pickle slices or "chips"
*   Mayo and mustard
*   Oil or shortening for frying

## Preparation

Get a nice big dutch oven and add enough oil or shortening to be two inches deep. Install your fry/candy thermometer to the side of the pot and crank the heat to medium high. Your thermal destination is 320 degrees F.

Turn on your broiler and position rack in top position. This is a perfect time to use your toaster oven if you have one.

Meanwhile, weigh out the meat portions for however many burgers you want to make. Roll into balls and set aside. Do not refrigerate.

Grate the cheese. If you want 4 burgers, obviously you need 5 ounces total. Six would be 7 1/2, eight would be 10 ounces. Toss this cheese with the spices until all the powder has stuck to the cheese.

Place a thin layer of mayo on the bottom of the buns. Place half the cheese mixture on top of this. Spread mustard on bun tops and place the rest of the cheese on this. You should have half the cheese on the bottoms (on mayo) and half on the tops (on mustard). Place these under the broiler so that the cheese will melt as you cook the burgers.

When the oil hits 320 degrees F, place one of the meat balls on an upside-down sheet pan. Dip a wide metal grill spatula in the hot fat then use it to smash and spread the meat ball out into a 5 to 6-inch-wide disk. It will be irregular around the circumference and that's good as all those irregularities will become crunchy goodness. The meat will also shrink by a couple of inches. Gently scrape the patty/wafer off the pan with the spatula and gently drop into the fat. Cook one minute, no more, no less. You can cook up to three patties at a time, but watch the oil temp and don't let it drop to under 300 degrees F.

Remove meat to a paper towel to drain briefly then place it on the bun bottom right away. Place the pickles on top, then the bun top. The goal: bread/mayo/cheese/meat/cheese/pickles/mustard/bread.

Consume or wrap in foil and hold for up to half an hour.

_Alton Brown_
