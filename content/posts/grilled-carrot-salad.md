---
title: "Grilled Carrot Salad"
author: "pencechp"
date: "2017-06-20"
categories:
  - side dish
tags:
  - carrot
  - salad
  - untested
---

## Ingredients

*   1/2 cup extra-virgin olive oil
*   2 tablespoons smoked sweet paprika
*   1 tablespoon ground fennel
*   1 tablespoon ground coriander
*   2 garlic cloves, thinly sliced
*   4 thyme sprigs
*   1 pound baby carrots, halved lengthwise and tops trimmed to 1 inch
*   Salt and freshly ground pepper
*   4 tablespoons unsalted butter
*   2 tablespoons sherry vinegar
*   1 tablespoon water
*   2 tablespoons marcona almonds, plus chopped almonds for garnish
*   5 ounces baby arugula

## Preparation

In a bowl, combine the olive oil, paprika, fennel, coriander, garlic and thyme. Add the carrots and let stand for 2 hours.

Preheat a grill pan. Remove the carrots from the marinade and season with salt and pepper. Grill over moderate heat, turning, until crisp-tender, 6 minutes. Transfer to a bowl. Meanwhile, in a small skillet, cook the butter over moderate heat until lightly browned and nutty-smelling, shaking the pan gently, about 5 minutes. Scrape the butter and solids into a blender. Add the vinegar, water and the 2 tablespoons of almonds; puree until smooth. Season the vinaigrette with salt and pepper.

Add the vinaigrette and arugula to the carrots; toss to coat. Transfer the salad to plates, sprinkle with chopped almonds and serve.

_Food & Wine_
