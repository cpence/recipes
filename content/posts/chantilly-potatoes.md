---
title: "Chantilly Potatoes"
author: "pencechp"
date: "2015-12-28"
categories:
  - side dish
tags:
  - potato
---

## Ingredients

*   2 pounds small/fingerling gold potatoes (~2"), or peeled and 2" cubed Yukon Gold
*   Salt
*   1/2 cup cold milk
*   7 tablespoons unsalted butter, softened
*   Freshly ground pepper
*   1 cup heavy cream
*   1/2 cup freshly grated Parmesan cheese

## Preparation

Preheat the oven to 400°. Butter a 9-by-13-inch baking dish. Put the potatoes in a large saucepan and cover with water. Salt the water and bring to a boil, then simmer the potatoes over moderate heat until tender, about 12 minutes. Drain the potatoes, return to the saucepan and shake over high heat for 1 minute to dry. Pass the potatoes through a ricer into a large bowl. Beat in the milk and 6 tablespoons of the butter and season with salt and pepper.

In a large stainless steel bowl, whip the cream to soft peaks. Beat one-third of the cream into the potatoes, then fold in the remaining cream. Scrape the potatoes into the prepared dish. Dot with the remaining 1 tablespoon of butter and sprinkle the Parmesan over the top. Bake the potatoes for 25 minutes. Preheat the broiler and broil the potatoes for 2 minutes, or until browned. Let stand for 10 minutes before serving.

_Food & Wine_
