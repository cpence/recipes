---
title: "Indian-Style Okra with Tomatillos"
author: "juliaphilip"
date: "2010-05-31"
categories:
  - side dish
tags:
  - indian
  - okra
  - tomatillo
  - vegan
  - vegetarian
---

## Ingredients

*   2 tablespoons vegetable oil
*   1/2 cup coarsely chopped onion
*   2 teaspoons ground turmeric
*   1 pound tomatillos, husked, washed, quartered
*   3/4 pound okra, sliced 1/2-inch thick
*   2 large plum tomatoes, chopped
*   1 jalapeno, seeded, minced
*   1 tablespoon grated fresh ginger
*   1/4 cup water
*   1/2 teaspoon salt
*   1/4 cup chopped cilantro

## Preparation

Heat oil in a skillet over medium heat; add onion and turmeric. Cook, stirring, 3 minutes. Add tomatillos and okra; cook over medium-high heat, stirring until browned and vegetables begin to soften, about 5 minutes. Stir in tomatoes, jalapeno, ginger and water; season with salt. Simmer over low heat until okra is tender and most of liquid has evaporated, about 8 minutes. Add cilantro.

_Judy Hevrdejs, Tribune Newspapers_
