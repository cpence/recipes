---
title: "Pink Strawberry Cheesecake"
date: 2020-09-28T12:01:11+02:00
categories:
    - dessert
tags:
    - strawberry
    - cake
    - pie
---

## Ingredients

*   200g cookies (for crust)
*   100g unsalted butter, melted
*   3 sheets leaf gelatin
*   142ml single cream
*   300g full-fat cream cheese
*   100g + 2 tbsp. sugar
*   zest of a lemon (divided)
*   6 tbsp. lemon juice (divided)
*   750g strawberries (divided)
*   142ml double cream
*   1 medium egg white

## Preparation

Seal the cookies in a plastic bag and crush tem to fine crumbs with a rolling
pin. Tip into a bowl and stir in the butter, mixing thoroughly. Put the mix into
a loose-bottomed 24cm (9–10") cake pan at least 5cm deep, and press down with a
spoon to make an even crust. Chill in the fridge while you make the filling.

Soften the gelatin sheets in cold water for 5 minutes. Pour the single cream in
a pan and bring it just to a boil, then take off the heat; wring out the gelatin
sheets and add them to the cream. Leave to cool for a few minutes

Beat the cheese, 100g sugar, half of the lemon zest, and half of the lemon juice
in a bowl until smooth. Mix in the cream and gelatine mixture and 300g of the
strawberries, chopped.

Whip the double cream to soft peaks, then fold into the strawberry mix. Whisk
the egg white in a clean bowl to stiff peaks, then fold it in as well. Pour into
the cake tin and smooth. Chill for at least 2 1/2 hours, or until set.

To make the sauce, blend 250g strawberries and the other half of the zest and
juice, plus 2 tbsp. sugar in a food processor or blender. Add more sugar if
needed, then keep cool until ready to serve.

Serve with the last 200g strawberries halved and piled up in the center, along
with the sauce.

*BBC Good Food*

