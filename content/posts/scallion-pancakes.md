---
title: "Scallion Pancakes"
date: 2020-06-06T14:28:24+02:00
categories:
    - side dish
tags:
    - chinese
    - onion
---

## Ingredients

*   400 g all-purpose flour
*   280 g cold water
*   two large bunches of green onions
*   2x heaping tbsp. (4 g) of salt
*   2x 3 tbsp. vegetable oil

## Preparation

Mix the water into the flour in a bowl. The dough will be soft and extremely
sticky, but try to knead it anyway in the bowl until the bowl starts to come
clean and you feel the dough hardening. Don't add extra flour; it'll come
together and stop sticking eventually. When it does, clean your hands and split
the dough in half.

On a floured surface, smooth each half of the dough into a nice ball. Cover with
a wet cloth and let it rest for 20 minutes.

While you wait, prepare your green onions. Cut them first vertically into halves
or fourths, then mince them as finely as you can.

After 20 minutes pass, gently roll out the dough into rounds on a floured
surface. (Keep re-flouring your surface as you go so you ensure it doesn't
stick.) You should be able to roll it out evenly very thin, 1 or 2mm and maybe a
half-meter wide.

Sprinkle the surface evenly with a heaping tbsp. of salt. Lightly roll the dough
to stick the salt down into it. Spread the oil out over the center of the dough,
leaving a margin around the edge. To get the oil out to the edges of the dough,
fold the sides over into the middle and then unfold them again. Keep folding and
unfolding the dough until the oil is evenly distributed.

Scatter the chopped green onions over the surface of the dough, something like
1/3 to 1/2 cup.

Now, roll the dough tightly into a tube. Seal up the ends. Then, twist the dough
(don't pull it while twisting) so that it looks rather like a rope. Coil the
rope into a single coil, about two layers thick (think soft-serve ice cream
spiral).

(Repeat all of that above again, for the other ball of dough.)

Cover the two dough-coils with a damp cloth again, and rest for another 20
minutes.

On an oiled work surface, take one of the coils and mush it straight down flat,
until it's about 1cm thick.

Heat your non-stick pan over medium heat for quite a while to get its
temperature up. Cook on one side until the bottom is nicely golden-brown in
spots. Flip it, and cover the pan with a lid. Keep checking the bottom. When
it's colored, too, pick the pancake up and throw hell out of it flat into the
skillet. Flip it over and do the same thing again on the other side. This helps
create the interior layers.

Now you should add around 1/4 c. of oil to the pan and turn up the heat, which
will start to deep-fry the outside. Flip it occasionally, cooking until the
outside is fully golden-brown. Remove to a paper towel to drain, quarter, and
eat!

*培仁蔬食MAMA on YouTube, transcribed sort of*

