---
title: "Vegetable Tagine with Harissa"
author: "pencechp"
date: "2014-03-10"
categories:
  - main course
tags:
  - moroccan
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   1 tbsp. olive oil
*   1 red onion, coarsely chopped
*   2 garlic cloves, crushed
*   3 medium carrots, thinly sliced
*   3 celery stalks, chopped into 1/2" pieces
*   2 tsp. harissa paste
*   2 tomatoes, peeled, roughly chopped
*   6 baby eggplants, stalks trimmed, cut in half lengthwise
*   1 c. okra, tops removed
*   2 tbsp. parsley
*   1 c. water
*   a few pinches salt

## Preparation

Heat the oil in a tagine on medium. Fry onion, garlic, carrots, celery until all just beginning to brown. Stir in harissa and cook for one minute. Add tomatoes, 1 c. water, and salt. Cover and cook gently for 30-40 minutes, until vegetables are beginning to soften.

Add eggplants, and cook for 15 minutes. Add okra and cook for another 15-20 minutes.

For serving, the consistency should be quite thick. Remove the lid and boil if needed. Stir in parsley just before serving.

_Le Creuset tagine booklet_
