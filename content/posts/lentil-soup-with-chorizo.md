---
title: "Lentil Soup with Chorizo"
author: "pencechp"
date: "2017-05-02"
categories:
  - main course
tags:
  - chorizo
  - soup
  - spanish
---

## Ingredients

*   1 lb. lentils
*   1 onion
*   5 tbsp. extra-virgin olive oil
*   1 ½ lb. Spanish chorizo
*   3 carrots, peeled and cut into 1/4-inch pieces
*   3 tbsp. parsley
*   7 c. water
*   3 tbsp. sherry vinegar
*   2 bay leaves
*   ⅛ tsp. ground cloves
*   2 tbsp. smoked paprika
*   3 garlic cloves, minced
*   1 tbsp. all-purpose flour

## Preparation

Place lentils and 2 teaspoons salt in pot. Cover with 4 cups boiling water and let soak for 30 minutes. Drain.

Finely chop three-quarters of onion and grate remaining quarter. Heat 2 tablespoons oil in Dutch oven over medium heat until shimmering. Add chorizo and brown, 6-8 minutes. Transfer chorizo to plate. Reduce heat to low and add chopped onion, carrots, 1 tbsp. parsley, and 1 tsp. salt. Cover and cook, stirring occasionally, until vegetables are very soft but not brown, 25 to 30 minutes. If vegetables begin to brown, add water.

Add lentils and sherry vinegar to vegetables; increase heat to medium-high; and cook, stirring frequently, until vinegar starts to evaporate, 3 to 4 minutes. Add 7 cups water, chorizo, bay leaves, and cloves; bring to simmer. Reduce heat to low; cover; and cook until lentils are tender, about 30 minutes.

Heat remaining 3 tbsp. oil in small saucepan over medium heat. Add paprika, grated onion, garlic, and ½ tsp. pepper; cook, stirring constantly, until fragrant, 2 minutes. Add flour and cook, stirring constantly, 1 minute longer. Remove chorizo and bay leaves from lentils. Stir paprika mixture into lentils and continue to cook until flavors have blended and soup has thickened, 10 to 15 minutes. When chorizo is cool enough to handle, cut in half lengthwise, then cut each half into ¼-inch-thick slices. Return chorizo to soup along with remaining 2 tablespoons parsley and heat through, about 1 minute. Season with salt, pepper, and up to 2 teaspoons sherry vinegar to taste, and serve.
