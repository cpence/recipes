---
title: "Pressure Cooker Porcini Risotto"
author: "pencechp"
date: "2017-02-21"
categories:
  - main course
tags:
  - instantpot
  - mushroom
  - rice
---

## Ingredients

*   1 tablespoon olive oil or butter
*   ½ cup finely chopped onions
*   1 ½ cups arborio rice
*   ½ cup dry white wine or dry vermouth
*   3 to 3 ½ cups chicken or vegetable broth
*   1 ounce dried porcini, broken into bits
*   1 cup frozen peas
*   ½ cup grated Parmesan, plus more to pass at the table
*   Salt and freshly ground pepper to taste
*   2 tablespoons chopped parsley, for garnish

## Preparation

Heat the oil in a 2 1/2-quart or larger pressure cooker. Add the onions, and cook over high heat for 1 minute, stirring frequently. Stir in the rice, taking care to coat it with the oil. Cook for 30 seconds, stirring constantly.

Stir in the wine. Cook over high heat until the rice has absorbed the wine, about 30 seconds. Stir in 3 cups of the broth and the porcini, taking care to scrape up any rice that might be sticking to the bottom of the cooker.

Lock the lid in place. Over high heat, bring to high pressure. Reduce the heat just enough to maintain high pressure, and cook for 4 minutes. Turn off the heat. Quick-release the pressure by setting the cooker under cold running water. Remove the lid, tilting it away from you to allow the steam to escape.

Set the cooker over medium-high heat, and stir vigorously. The risotto will look fairly soupy at this point. Boil while stirring every minute or so, until the mixture thickens and the rice is tender but still chewy, 1 to 4 minutes. Stir in the peas when the rice is almost done. (If the mixture becomes dry before the rice is done, stir in the extra ½ cup of broth. The finished risotto should be slightly runny; it will continue to thicken as it sits on the plate.)

Turn off the heat. Stir in the Parmesan, and salt and pepper to taste. Serve immediately, garnished with a little parsley. Pass extra Parmesan at the table.

_New York Times_
