---
title: "Finnish Salmon Soup (Lohikeitto)"
author: "pencechp"
date: "2015-10-07"
categories:
  - main course
tags:
  - finnish
  - fish
  - soup
---

## Ingredients

*   1 tbsp. butter
*   1 onion, finely chopped
*   4-5 potatoes, cubed
*   5 1/2 c. fish stock
*   1 lb. fresh salmon fillet, cubed
*   2 c. whole milk
*   1 c. fresh dill, finely chopped
*   5 bay leaves
*   dash sea salt
*   dash pepper
*   dash allspice

\[CP: To test the next time I make this: juice of 1/4 to 1/2 of a lemon?\]

## Preparation

Place the potato cubes in water to keep them from discoloring. In a soup pot, simmer the chopped onions in the butter over medium heat until soft .Add peeled and diced potatoes and then enough water to just cover the potatoes. Turn up the heat to high, cover, bring to a boil and cook the potatoes until they are just soft, adjusting the heat down as necessary.

Add the cubed salmon to the pot and cook until it is mostly opaque (this will take about 5 minutes, if that). Do not stir the soup so as not to break up the salmon. If you want to keep the Salmon cubes looking like cubes, once the salmon is cooked, remove from the soup and set aside. Add the fish stock, milk, bay leaves, and spices. Cook for 5-10 minutes.

Take off the heat and stir in the fresh dill. If you removed the salmon cubes, transfer the cooked salmon into individual bowls and ladle the soup over them. Serve with rye bread and butter.

_Alternative Finland_
