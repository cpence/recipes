---
title: "Southside"
author: "juliaphilip"
date: "2010-06-23"
categories:
  - cocktails
tags:
  - gin
  - untested
---

## Ingredients

*   3 mint sprigs
*   2 oz gin
*   3/4 oz fresh lime juice
*   3/4 oz simple syrup
*   1 small dash bitters (optional)

## Preparation

Bruise two sprigs of mint gently in a cocktail shaker. Add ingredients. Add ice. Shake. Strain into a cocktail glass. Garnish with remaining mint leaf.

_Toby Maloney_, _Chicago Tribune_
