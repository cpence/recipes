---
title: "Whole-Wheat Spaghetti with Italian Sausage and Fennel"
author: "pencechp"
date: "2016-06-08"
categories:
  - main course
tags:
  - fennel
  - italian
  - pasta
  - sausage
---

## Ingredients

*   ¼ c. olive oil
*   6 garlic cloves, pressed
*   ½ tsp. red pepper flakes (JP: reduce to make less spicy)
*   Salt
*   8 oz. sweet Italian sausage
*   1 fennel bulb, halved, cored, and sliced thin
*   ½ c. pine nuts, toasted and coarsely chopped
*   ½ c. coarsely chopped fresh basil leaves
*   2 tbsp. lemon juice
*   1 lb. whole-wheat spaghetti
*   1 oz. grated parmesan (~½ c.)

## Preparation

1\. Combine oil, garlic, pepper flakes, and 1/2 teaspoon salt in small bowl; set aside.

2\. Heat sausage in 12-inch nonstick skillet over medium-high heat; cook, stirring to break sausage into ½-inch pieces, until browned and crisp, 5 to 7 minutes. Using slotted spoon, transfer sausage to paper towel-lined plate, leaving rendered fat in skillet. Return skillet to medium-high heat, add fennel and ¼ teaspoon salt, and cook, stirring frequently, until fennel is tender, about 5 minutes.

3\. Push fennel to sides of skillet to create 3-inch clearing; add oil-garlic mixture and cook, stirring constantly, until fragrant, about 1 minute. Stir to combine garlic mixture with fennel and cook for 1 minute longer. Remove skillet from heat and stir in sausage, pine nuts, basil, and lemon juice.

4\. Meanwhile, bring 4 quarts water to boil in large Dutch oven. Add pasta and 1 tablespoon salt to boiling water; cook until al dente. Reserve ¾ cup pasta cooking water, drain pasta, and return pasta to Dutch oven.

5\. Add sauce and reserved cooking water to pasta and toss to coat. Season with salt to taste. Transfer to serving bowl, sprinkle with cheese, and serve.

_CI_
