---
title: "Mechoui"
date: 2024-08-05T09:06:09+02:00
categories:
  - main course
tags:
  - lamb
  - moroccan
  - untested
---

## Ingredients

- 3 kg of lamb (shoulder or leg of lamb)
- 1 soup spoonful of sweet red pepper
- 1/4 coffee spoonful of hot red pepper
- 1 coffee spoonful of cumin [GM: + 1 of corriander seed]
- 1 pinch of safran
- salt
- 1 soup spoonful of lemon juice
- 50 g of smelted butter
- 5 soup spoonful of olive oil
- 1 glass of water
- [GM: 4 to 5 cloves garlic]

## Preparation

Clean and drain the meat, mix the ingredients in the previous order, except for
water which will be used to water the meat.

Remove the skin when necessary, tap deeply the fleshy places, mass the meat with
that mixture, leave it impregnate if possible for 2 hours

Pour water in the oven plate, place the fleshy side of the meat in the plate of
the oven, using a little fire for a long cooking of 2 hours, every 1/4 an hour,
water the meat with its own juice, add water if necessary, when almost cooked,
return the meat to cook the other side

Serve hot either the met in complete or in individual plates with salad leaves.

_The website of the government of Morocco, printed out by my grandmother on
November 22, 1999_
