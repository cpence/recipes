---
title: "Whole Roasted Suckling Pig"
author: "pencechp"
date: "2018-12-26"
categories:
  - main course
tags:
  - pork
---

## Ingredients

*   1 whole suckling pig, around 20lb/10kg
*   salt and pepper
*   4 garlic cloves
*   1 tbsp. chopped fresh rosemary
*   2 tbsp. chopped fresh parsley
*   1 tbsp. chopped fresh sage
*   1 tsp. juniper berries, chopped
*   2 bay leaves

## Preparation

Preheat oven to 275 F. Chop together the garlic, rosemary, parsley, sage, and juniper berries, and stir in 2 tbsp. of oil. Rinse the pig inside and out, pat dry. Season inside and out with oil, salt, and pepper. Place the herb mixture inside the cavity of the pig. Cover ears and tail with tin foil to keep them from burning, and put a ball of tin foil in the mouth if you want to add an apple later.

Roast until an instant-read thermometer reads 160 F at the deepest joint (the shoulder near the head), around 4 hours (or less). (If you want to hold the pig, you can tent it with foil at this point until ready to continue.) Increase oven temperature to 500 F and cook for 30 minutes more, until skin is crisp all over.

\[Consider serving this with [Czech potato pancakes.]( {{< relref "bramboracky-czech-potato-pancakes" >}} )\]

_Serious Eats and Recipe 4 Living, edited_
