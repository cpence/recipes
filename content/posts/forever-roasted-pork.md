---
title: "Forever Roasted Pork"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - main course
tags:
  - fennel
  - pork
---

## Ingredients

*   2 medium onions, peeled
*   2 tablespoons extra virgin olive oil
*   salt and freshly ground pepper
*   1 1/2 teaspoons finely chopped fresh sage
*   1/2 cup water
*   4 pounds pork leg or shoulder, at room temperature
*   About 1/4 cup [fennel spice rub]( {{< relref "fennel-spice-rub" >}} )

## Preparation

Thinly slice the onions. Heat the olive oil in a large saute pan over medium heat until hot. Add the onions and a pinch of salt and pepper. Reduce the heat to medium-low and cook for about 1 minute. Add the sage and cook until the onions cease throwing off water, about 3 minutes. Add the water, cover, and cook until the onions are very tender, about 10 minutes. Uncover and saute until the onions are very soft and the pan is dry again, about 2 minutes. Season well with salt and pepper.

Preheat the oven to 275 degrees. Peel back the pork skin and spread the onions directly on the fat layer. Fold the skin back over the onions and tie closed with kitchen string. Season well all over with the fennel spice.

Arrange the meat on a rack in a roasting pan (or using the Cocorico, see below) and cook until the meat is very tender, 6 to 8 hours. It is ready when it pulls away easily if picked at with a pair of tongs. It is often easiest to cook the meat overnight, or put it in the oven in the morning and let it cook all day. It does not need to be attended.

_Michael Chiarello, Napa Style_
