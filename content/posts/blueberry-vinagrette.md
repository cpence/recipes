---
title: "Blueberry Vinagrette"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - miscellaneous
tags:
  - dressing
  - untested
---

## Ingredients

*   1/4 c. champagne vinegar
*   1/4 vanilla bean
*   1/4 c. honey
*   1 c. blueberries
*   2 tbsp. freshly squeezed lemon juice
*   1 medium shallot, diced
*   2 1/2 tsp. kosher salt
*   1 1/2 tsp. Dijon mustard
*   1/2 tsp. lemon zest
*   1/4 tsp. black pepper
*   1 c. olive oil
*   1 c. corn oil
*   1 tbsp. shredded fresh mint

## Preparation

Pour vinegar into a small saucepan over medium heat. Split vanilla bean. Scrape seeds into vinegar and add vanilla bean to vinegar. Bring to a boil.

Reduce heat to low and reduce vinegar/vanilla mixture by half. Remove (and discard) bean, cool mixture and reserve.

In a blender, combine vinegar, honey, blueberries, lemon juice, shallot, salt, mustard, lemon zest and black pepper and blend until smooth. In a small bowl, whisk together olive oil and corn oil.

With blender on low speed, slowly drizzle oils into blender until smooth. Stir in mint. Transfer vinaigrette to an airtight jar and refrigerate at least 2 hours before using. Vinaigrette may be refrigerated for up to 2 weeks; shake before using.

_Mike Grossmann_
