---
title: "Homemade Goop"
author: "pencechp"
date: "2012-07-24"
categories:
  - miscellaneous
tags:
---

## Ingredients

*   7 1/3 tbsp. honey
*   3/4 tbsp. blackstrap molasses
*   1/10 tsp. table salt

## Preparation

Mix, and fill up a 5 oz (or so) Goop squeezy bottle. According to Wolfram Alpha,
this has about 500 calories, so is equivalent to around 5 packs of normal goop.
