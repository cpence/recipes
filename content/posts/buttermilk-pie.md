---
title: "Buttermilk Pie"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - buttermilk
  - pence
  - pie
---

## Ingredients

*   2 c. sugar
*   1/2 c. flour
*   1 stick butter
*   3 eggs
*   scant 1/4 tsp. baking soda
*   1 tbsp. vanilla
*   1 c. buttermilk
*   Two pie crusts

## Preparation

Cream sugar, flour, and butter. Blend in eggs. Add baking soda, vanilla, and buttermilk. Pour into two (yes, two) crusts. Bake at 300–325 degrees, for at least 1 hour or until set.
