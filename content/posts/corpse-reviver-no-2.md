---
title: "Corpse Reviver No. 2"
author: "pencechp"
date: "2015-11-22"
categories:
  - cocktails
tags:
  - gin
  - lemon
---

## Ingredients

*   ¾ oz. dry gin
*   ¾ oz. Cointreau
*   ¾ oz. Cocchi Americano
*   ¾ oz. fresh lemon juice
*   Scant teaspoon absinthe

## Preparation

Chill a coupe glass, and evenly coat the inside with absinthe. Shake remaining ingredients with ice. Strain into coupe.

_The New York Times_
