---
title: "Roasted Kohlrabi"
author: "pencechp"
date: "2017-10-30"
categories:
  - side dish
tags:
---

## Ingredients

*   6 kohlrabi
*   2 tbsp. olive oil
*   3/4 tsp. salt
*   cayenne pepper
*   3 tbsp. parmesan cheese
*   1 tbsp. parsley, chopped

## Preparation

Peel kohlrabi and cut into 1-inch wedges; toss with olive oil, salt and a pinch of cayenne on a rimmed baking sheet. Roast at 450 degrees F, stirring every 10 minutes, until tender and golden, about 30 minutes. Toss with parmesan and parsley.

_Food Network_
