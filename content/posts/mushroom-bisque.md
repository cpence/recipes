---
title: "Mushroom Bisque"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - side dish
tags:
  - mushroom
  - soup
---

## Ingredients

*   1 lb. Fresh mushrooms
*   1 quart chicken or vegetable broth
*   1 medium onion, chopped
*   7 tbs butter
*   6 tbs flour
*   3 cups milk
*   1 cup heavy cream
*   Salt to taste
*   White pepper
*   Tabasco sauce
*   2 tbs sherry (optional)

## Preparation

Wash the mushrooms and cut off the stems.  Slice six caps and save for garnish.  Discard any dried ends from stems.  Grind or chop the remaining caps and stems very fine.  Simmer, covered in the broth with the onion thirty minutes.

Sauté the saved caps in one tablespoon of butter.

Melt the remaining butter in a saucepan, add the flour and stir with a wire whisk until blended.  Meanwhile bring the milk to a boil and add all at once to the butter-flour mixture, stirring vigorously until the sauce is thickened and smooth. Add the cream.

Combine the mushroom-broth mixture with the sauce and season to taste with salt, pepper, and Tabasco sauce.  Reheat and add the sherry before serving.  Garnish with the sautéed sliced mushrooms.

_New York Times Cookbook_
