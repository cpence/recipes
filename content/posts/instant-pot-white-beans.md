---
title: "Instant Pot White Beans"
date: 2021-01-09T22:49:33+01:00
categories:
  - side dish
tags:
  - instantpot
  - bean
---

## Ingredients

- 1 (1-lb.) bag Great Northern Beans, rinsed and sorted
- 1 tablespoon onion powder
- 2 teaspoons garlic powder
- 1-2 teaspoons salt
- 1 bay leaf
- 6 cups unsalted vegetable broth, chicken broth, or water

## Preparation

Add all ingredients to the Instant Pot. Cover, twist to lock the lid, and turn the valve to sealing. Press the Beans/Chili button and set to 30 Minutes.

When the timer beeps, allow the pressure to release naturally for at least 20 minutes. Then, turn the valve to venting.

Use as you would canned beans.

**Notes:** If you want to eat these as a freestanding side dish, you can use full-strength broth; if you're using them as canned beans, think about using half-broth or just water. If the beans are bigger (large white beans, kidney beans) then add 5 minutes or so to the cook time. Small beans deduct 5.

_Camellia Brand Beans_
