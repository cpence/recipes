---
title: "Spaghetti Casserole"
author: "pencechp"
date: "2009-12-10"
categories:
  - main course
tags:
  - beef
  - pasta
  - pence
  - pork
---

## Ingredients

*   1 lg. onion
*   2 sm. bell peppers
*   some ground beef or Italian sausage or ground pork
*   1 pkg. sliced mushrooms
*   2 jars standard spaghetti sauce
*   1 lb. spaghetti
*   1 container ricotta cheese (optional)
*   1 container grated parmesan cheese

## Preparation

Sautee onion and bell pepper until translucent.

Add ground beef (or Italian sausage, or ground pork).

Cook until the meat is about ¾ cooked.

Add mushrooms and cook until mushrooms and meat are done.

Drain, if desired.

Add spaghetti sauce and simmer for 20-30 minutes.

While sauce mixture is simmering, boil pasta.

Add pasta to sauce and mix thoroughly.

Grease a 9x11 casserole dish.

If using ricotta cheese, add half of the sauce/spaghetti mix, then a layer of ricotta, and then the other half of the sauce/spaghetti mix. Otherwise, just add sauce/spaghetti mix to dish.

Bake, covered, at 350 degrees for 30-45 minutes.

Uncover, top with parmesan cheese, and bake for a few more minutes until bubbly.

_Cynthia Pence_
