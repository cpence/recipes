---
title: "Massive Pork Tenderloin"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - pork
---

## Ingredients

*   1/4 c fresh rosemary, chopped
*   1 tbsp sea salt
*   1 tbsp coarse black pepper
*   1 tbsp red chili flakes
*   1/4 c olive oil
*   6 lb pork tenderloin
*   1 c white wine

## Preparation

Mash the first five ingredients into a paste and rub it on the pork tenderloin. Marinate for at least two to three hours.

Sear the tenderloin on all sides (including the ends) and place it in a baking dish. Add about one cup of white wine to the dish. Cook at 325 F for 15-20 minutes per pound, to an internal temperature of 135 F. Remove from oven and allow to rest for at least 10 min.
