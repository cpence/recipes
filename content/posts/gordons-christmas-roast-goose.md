---
title: "Gordon's Christmas Roast Goose"
author: "pencechp"
date: "2012-12-16"
categories:
  - main course
tags:
  - game
  - goose
---

## Ingredients

*   9-12 lb. (4-5.5 kg) fresh goose
*   4 lemons
*   3 limes
*   1 tsp Chinese five-spice powder
*   small handful each of parsley sprigs, thyme and sage, plus extra for garnishing
*   a little olive oil, for browning, optional
*   3 tbsp honey
*   1 tbsp thyme leaves

Serve with [Red Wine & Date Sauce.]( {{< relref "date-red-wine-sauce" >}} )

## Preparation

Calculate the cooking time: Cook for 10 mins at 460 F/240 C/fan 220 C/gas 9, then reduce to 375 F/190 C/fan 170 C/gas 5 and cook for 20 mins per kg for medium-rare, 32 mins per kg for more well-done, plus 30 mins resting.

If the goose is ready-trussed, then loosen the string and pull out the legs and wings a little - this helps the bird cook better. Check the inside of the bird and remove any giblets or pads of fat. Using the tip of a sharp knife, lightly score the breast and leg skin in a criss-cross. This helps the fat to render down more quickly during roasting.

Grate the zest from the lemons and limes. Mix with 2 tsp fine sea salt, the five-spice powder and pepper to taste. Season the cavity of the goose generously with salt, then rub the citrus mix well into the skin and sprinkle some inside the cavity.

Stuff the zested fruit and the herb sprigs inside the bird and set aside for at least 15 mins. Can be done up to a day ahead and kept refrigerated.

Heat oven to 460 F/240 C/fan 220 C/gas 9. If you want to give the bird a nice golden skin, brown in a large frying pan (or a heavy-based roasting tin), using a couple of tbsp of oil. Holding the bird by the legs (you may like to use an oven glove), press it down on the breasts to brown.

Once browned, place the bird in the roasting tin. Drizzle with the honey and sprinkle with thyme leaves. Roast for the calculated time, turning the heat down after 10 mins to 375 F/190 C/fan 170 C/gas 5. Cover the goose with foil if it is starting to brown too much.

Every 30 mins or so, baste the bird with the pan juices, then pour off the fat through a sieve into a large heatproof bowl . You will end up with at least a litre of luscious fat - save this for the potatoes and other veg. At the end of the cooking time, leave to rest for at least 30 mins, covered loosely with foil. The bird will not go cold, but will be moist and much easier to carve.

_Gordon Ramsay, BBC Good Food, December 2005_
