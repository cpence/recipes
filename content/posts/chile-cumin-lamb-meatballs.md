---
title: "Chile-Cumin Lamb Meatballs"
author: "pencechp"
date: "2015-06-21"
categories:
  - main course
tags:
  - lamb
  - mediterranean
  - untested
---

## Ingredients

_Chile sauce:_

*   3 pasilla chiles, seeds removed, chopped
*   1 1/2 teaspoons crushed red pepper flakes
*   1 tablespoon cumin seeds
*   1/4 cup Sherry vinegar or red wine vinegar
*   1 tablespoon sweet smoked paprika
*   1 garlic clove, chopped
*   1/2 cup olive oil Kosher salt

_Meatballs:_

*   1/4 small onion, chopped
*   8 garlic cloves, 7 cloves chopped, 1 clove finely grated
*   1 tablespoon chopped fresh parsley
*   1 teaspoon chopped fresh oregano
*   1 teaspoon chopped fresh sage
*   3/4 teaspoon fennel seeds
*   3/4 teaspoon ground coriander
*   3/4 teaspoon ground cumin
*   1 tablespoon kosher salt
*   1 large egg
*   1 1/4 pounds ground lamb
*   1 tablespoon rice flour or all-purpose flour
*   4–5 tablespoons olive oil, divided
*   1/2 English hothouse cucumber, thinly sliced
*   1/2 teaspoon finely grated lemon zest
*   2 tablespoons fresh lemon juice
*   1 1/2 tablespoons Sherry vinegar or red wine vinegar
*   Freshly ground black pepper
*   1/2 cup plain sheep's-milk or cow's whole-milk yogurt
*   1/4 cup chopped fresh mint
*   Crushed red pepper flakes (for serving)

## Preparation

_Chile sauce:_ Toast chiles, red pepper flakes, and cumin seeds in a dry small skillet over medium heat, tossing occasionally, until fragrant and cumin seeds are golden, about 3 minutes.

Let cool. Working in batches, finely grind in spice mill, then transfer to a blender. Add vinegar, paprika, and garlic to blender and blend until smooth. With motor running, gradually stream in oil and blend until combined. Transfer to a large bowl; season with salt.

_Meatballs and assembly:_ Pulse onion, chopped garlic, parsley, oregano, sage, fennel seeds, coriander, cumin, and 1 tablespoon salt in a food processor, scraping down sides as needed, until finely chopped. Add egg, lamb, and flour and pulse until evenly combined. Form lamb mixture into 1 1/2" balls.

Heat 2 tablespoons oil in a large skillet, preferably cast iron, over medium-high. Working in 2 batches and adding another 1 tablespoon oil to skillet if needed, cook meatballs, turning occasionally, until browned on all sides and cooked through, 5–8 minutes. Transfer meatballs to paper towels to drain.

Transfer all meatballs to bowl with chile sauce and toss to coat. Toss cucumber in a medium bowl with lemon zest, lemon juice, vinegar, and remaining 2 tablespoons oil; season with salt and pepper.

Spoon yogurt into bowls. Evenly divide meatballs among bowls; top with dressed cucumber and mint and sprinkle with red pepper flakes.

_Bon Appetit, April 2015_
