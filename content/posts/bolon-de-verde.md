---
title: "Bolon De Verde (Ecuadoran Plantain Dumplings)"
date: 2022-11-28T10:12:32+01:00
categories:
  - side dish
tags:
  - untested
  - ecuadoran
---

## Ingredients

- 4 green plantains peeled and cut in medium sized chunks
- 4-5 tbs butter or lard
- 2 tbs oil canola or sunflower
- 1 tbs hot pepper or chili powder
- 1 tsp cumin
- 1 cup grated cheese and/or 1 cup cooked chorizo or chicharrones or bacon
- Salt to taste
- Ground peanuts (optional – add when mashing the green plantains)

## Preparation

1. Melt the butter or lard over medium heat in large sauté pan
2. Add the plantain chunks and cook for about 40 minutes or until they are very
   soft, turn them about every 10 minutes, they should be slightly golden but
   not too crispy.
3. Sprinkle the cooked plantains with the chili powder, cumin and salt.
4. Transfer the plantain pieces to a bowl, do this while they are still hot (but
   be careful not to burn yourself).
5. Mash the plantains using a wood masher – or just a regular potato masher –
   until you obtain chunky dough like consistency.
6. Form balls slightly smaller than the size of a tennis ball with the dough.
7. Make a hole in the middle of each ball and fill it with the cheese or chorizo
   or chicharrones (mixed with ground peanuts), gently press the filling into
   the hole, cover the filling and reshape it back into a ball shape.
8. Heat the oil over high heat, add the stuffed plantain dumplings and fry them
   until they are golden and crispy on each side.
9. Transfer to plate lined with paper towels to drain the grease and serve
   immediately.

_Laylita's Recipes_
