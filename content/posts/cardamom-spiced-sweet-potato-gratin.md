---
title: "Cardamom-Spiced Sweet Potato Gratin"
author: "pencechp"
date: "2014-11-30"
categories:
  - side dish
tags:
  - sweetpotato
---

## Ingredients

*   6 pounds sweet potatoes, peeled and coarsely grated
*   12 cloves garlic, grated or minced
*   3 tablespoons all-purpose flour
*   6 tablespoons extra-virgin olive oil
*   1¾ teaspoons fine sea salt
*   1 teaspoon freshly ground pepper
*   1 tablespoon ground cardamom
*   6 tablespoons duck fat or butter 
*    cups white breadcrumbs
*    4 tablespoons butter, melted
*    ¼ cup chopped parsley

## Preparation

1\. Toss potatoes, garlic, flour, oil, salt, pepper and cardamom together in a very large gratin dish (or 2 medium dishes). Place dollops of duck fat, evenly spaced, on top of potato mixture. Cover with foil and refrigerate until ready to use.

2\. Remove gratin from refrigerator and allow to come to room temperature, 1-2 hours. When ready to cook, preheat oven to 375 degrees. Place in oven and bake 25 minutes. Remove from oven, toss mixture, cover again with foil and bake until tender, 30 minutes more.

3\. Meanwhile, make breadcrumb topping: In a bowl, toss white breadcrumbs, melted butter and parsley until combined.

4\. Remove gratin from oven and remove foil. Increase oven temperature to 500 degrees. Place oven rack in top position. Sprinkle breadcrumb topping over potatoes. Place dish, uncovered, on top rack and broil until golden, about 5 minutes. Let rest 10-15 minutes before serving.

_Gail Monaghan, Wall Street Journal_
