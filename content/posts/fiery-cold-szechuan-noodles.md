---
title: "Fiery Cold Szechuan Noodles"
author: "pencechp"
date: "2015-07-31"
categories:
  - main course
tags:
  - chile
  - chinese
  - pasta
  - untested
---

## Ingredients

_For the chili paste/oil:_

*   2 tsp of red Szechuan peppercorn
*   1 tsp of green Szechuan peppercorn
*   3 + 2 tbsp of chili flakes (divided)
*   4 cloves of garlic, finely minced
*   1 small piece of scallion
*   2 star anise
*   1 dry bay leaf
*   1/2 tsp of ground cumin
*   1/2 tsp of ground coriander
*   1/8 tsp of curry powder
*   1 cup of vegetable oil
*   1 grated garlic
*   3 tsp of soy sauce

_For the sesame sauce:_

*   90 grams (1/4 cup + 1/8 cup) of toasted sesame paste
*   35 grams (1/8 cup) of creamy peanut butter
*   20 grams (1/8 cup) of chili oil (without the “bits”)
*   4-5 tbsp of soy sauce
*   2 tsp of dark Asian vinegar
*   2 tsp of sesame oil
*   1 tsp of sugar
*   1/2 cup of water (more or less depending on the thickness of sesame paste)

_For serving:_

*   2 servings of ramen noodles, plus more
*   sesame oil for rubbing the noodles
*   cucumber, cut into thin strips
*   radish, cut into thin strips
*   1 egg, beated with a tiny splash of milk
*   Crushed salted peanuts and toasted sesame to top

## Preparation

To make the chili paste/oil:  Finely grind the red/green Szechuan peppercorn in a spice-grinder or stone mortar (but you’ll need to work harder to break them down).  Set aside.  In a deep sauce pot, add 3 tbsp of chili flakes (reserve the other 2 tbsp), minced garlic, scallion, star anise, bay leaf, ground cumin, ground coriander, curry powder and vegetable oil.  Set the pot over medium heat and bring to a sizzle.  Stir constantly and let the mixture cook for 1 ~ 2 min until the minced garlic turn JUST LIGHTLY BROWNED.  Turn off the heat completely, then add the ground sichuan peppercorns and 2 tbsp of chili flakes (ground sichuan peppercorns will turn bitter if overcooked which is why it’s added at the end).  Keep stirring until the oil stops sizzling/bubbling.  Then add the grated garlic and soy sauce, and stir to combine.  Let the mixture sit at room temperature for AT LEAST 2 hours.  After which you can remove the star anise, bay leaf and scallions, then keep the oil and paste in a jar.  It will keep in the fridge for… a long long time.

To make the sesame sauce:  Add the toasted sesame paste, peanut butter, chili oil from above, soy sauce, dark vinegar, sesame oil and sugar in a blender.  Blend on high while slowly adding in 1/2 cup of water.  Scrape the sides down a few times to ensure even blending.  You can add more soy sauce or water to adjust seasoning and consistency if needed.  The sauce should be the consistency of yogurt.

To make the noodle:  Heat a flat and wide non-stick skillet over medium-high heat and brush the surface with oil.  Pour in 1 beated egg and swirl the pan to evenly distribute it so the egg thinly coats the bottom of the skillet.  Cook until both side of the egg-sheet is evenly browned, then thinly cut it into short strips.  Set aside.

Cook the ramen noodle according to the package instructions.  Once done (DON’T overcook it!  It should be still slightly chewy), rinse under cold water until cools down completely.  Toss with 2 tsp of sesame oil to prevent sticking, then you can keep it in the fridge until needed.  Before serving, toss the ramen noodles, cucumber strips, radish strips, egg strips and enough sesame sauce to generously coat every strand of noodles.  Evenly stir the chili paste/oil then add a couple tsp to the noodle (or more…), plus crushed salted peanuts and sesame on top.

_Lady and Pups_
