---
title: "Original KFC Recipe"
author: "pencechp"
date: "2014-09-02"
categories:
  - main course
tags:
  - chicken
  - untested
---

##Brine

*   4 cups of water
*   1 tbsp white Vinegar
*   1 tbsp table salt, non iodized
*   1 tbsp Sugar, white
*   1 tbsp MSG

After 4 hours, remove the chicken from brine and rinse in fresh water. Add the brined chicken to egg wash.

## Egg Wash

*   1 cup milk (The Col. used skim or reconstituted)
*   1 large egg

Combine well, dip chicken in egg wash dredge in 1- 2 cups seasoned flour depending on the size of your bird.

## Seasoned Flour

*   4 cups cake flour (low protein flour is very important here)
*   4 tsp freshly ground white pepper
*   3 tsp freshly ground black pepper
*   4 tsp freshly ground sage
*   1 1/2 tsp freshly ground coriander
*   1 1/4 tsp high quality ground ginger
*   1 tsp freshly ground Ancho chile
*   3/4 tsp freshly ground whole vanilla bean
*   3/4 tsp freshly ground bay leaf
*   3/4 tsp freshly ground savory
*   1/2 tsp freshly ground cloves
*   1/2 tsp freshly ground green cardamom seeds
*   1 tsp MSG

The salt is added after the chicken has been cooked, for each 4lb chicken use about 4ts popcorn salt. Some people find that too salty so you might want to reduce it some. I use finely ground sea salt.

You need about 2 to 2 1/2 cups of this breading for a 3 1/2 to 4 lb bird plus extra for gravy. The original chicken was cut into 9 pcs and cooked in corn oil. The Colonel would use the flour mix left over after breading the chicken to make his famous milk gravy.

This method is for people who don't want to pressure cook the chicken. It produces a tasty result but it is no where as moist as the pressure fried chicken, it is much less complicated though. If you want crunchier chicken mix a little milk with the breading, about an ounce or so, enough to make the flour shaggy. Press the flour firmly on the egg/milk dipped chicken.

Preheat your oven to 400 degrees.

Heat corn or vegetable oil in 11-inch straight-sided sauté pan over medium-high heat to 375 degrees. Carefully place chicken pieces in pan, skin side down, and cook until golden brown, 3 to 5 minutes. Carefully flip and continue to cook until golden brown on second side, 2 to 4 minutes longer. Transfer chicken to wire rack set in rimmed baking sheet. Bake chicken until instant-read thermometer inserted into thickest part of chicken registers 160 degrees for breasts and 175 for legs and thighs, 15 to 20 minutes. (Smaller pieces may cook faster than larger pieces. Remove pieces from oven as they reach correct temperature.) Let chicken rest 5 minutes before serving.

## Colonel Sanders' Original "Cracklin' Gravy" (1952-1964)

*   1 TBSP butter
*   1/4 cup Original Recipe breading flour
*   1/2 cup cracklings strained from the pressure cooker or frying pan
*   1 cup whole milk, cold
*   1 cup boiling water
*   1/4 cup whole cream, cold

Put the butter, flour and cracklings into a frying pan over low to medium heat. Stir continuously until the ingredients are fully blended and the raw flour is lightly browned. This takes a few minutes. The roux should not be darker than peanut butter brown. If you burn it, dump it, clean the pan start again. (If you are a novice gravy maker you might want to save your cracklins until the end so you won't waste them on a burned batch.)

Gently pour the milk, hot water, and cream, continue stirring until it begins to thicken. Taste the gravy and add extra salt or pepper if needed. Allow the mixture to come to a light boil, when it is the thickness you want turn off the heat and serve.

_A bunch of crazy people on an entire forum dedicated to KFC recipes, via Reddit_
