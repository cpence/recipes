---
title: "Irv's Mandelbrot (Biscotti)"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - cookie
  - untested
---

## Ingredients

*   3 c. flour
*   1 1/4 c. sugar
*   1 c. each, chopped: almonds, dried cranberries
*   2 tsp. baking powder
*   1/8 tsp. salt
*   3 eggs
*   1 c. canola oil
*   1 tsp. vanilla
*   zest of 1 orange
*   1 tsp. cinnamon

## Preparation

1\. Heat oven to 325 degrees.  Mix together the flour, 1 c. of the sugar, almonds, cranberries, baking powder, and salt in a large bowl with electric mixer on low speed; slowly beat in the eggs, oil, vanilla and orange zest.

2\. Scrape dough onto a lightly floured wooden board; knead until well mixed.  Roll into ball; divide into four pieces.  Roll each piece into a log about 10 inches long and 3 inches wide.  Bake logs, 3 inches apart, on lightly oiled foil-lined cookie sheets until lightly brown and firm, 30-40 minutes.

3\. Meanwhile, mix together the remaining 1/4 c. of the sugar and cinnamon in a small bowl.  Remove logs from oven; slide onto cutting board.  Let logs cool 5 minutes.  Gently cut each log into 12 pieces using a serrated knife.  Return cookies, cut side up, to cookie sheet.  Sprinkle with sugar-cinnamon mix.  Return to oven, bake 5 minutes.  Turn off oven; leave cookies overnight to dry out.

_Chicago Tribune_
