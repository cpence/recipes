---
title: "Soupe Au Potiron"
date: 2021-01-07T22:42:28+01:00
categories:
  - main course
tags:
  - soup
  - pumpkin
  - untested
---

## Ingredients

- 200g de lardons
- 800g de cubes de potiron (ou courge butternut)
- 1 oignon
- 400g de tomates pelées en cubes
- 1 l d'eau
- 2 cubes de bouillon de volaille
- 2 c à s d'huile d'olive
- 1 gousse d'ail
- 1 bouquet garni
- poivre et sel

## Preparation

Faites revenir à bon feu les cubes de potiron, l'oignon haché, et l'ail émincé dans une casserole contenant l'huile d'olive. Après 2 min, ajoutez les tomates, l'eau, les cubes de bouillon émiettés et le bouquet garni. Portez à ébullition et laissez mijoter 25 min à feu doux. Laissez reposer 5 min.

Pendant ce temps, faites rissoler les lardons dans une poêle anti-adhésive, sans matière grasse, en débutant la cuisson à feu doux et en l'augmentant quand le gras commence à fondre. Égouttez-les sur du papier absorbant.

Retirez le bouquet garni de la soupe et mixez-la finement. Ajoutez les lardons et rectifiez l'assaisonnement.

Servez bien chaud, avec du pain.

_Delhaize_
