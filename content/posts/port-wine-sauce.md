---
title: "Port Wine Sauce"
date: 2021-08-09T18:27:44+02:00
categories:
  - miscellaneous
tags:
  - french
  - sauce
---

## Ingredients

- 1 oz. butter
- ¼ cup shallots finely chopped
- ¾ cup Port
- ¼ cup red wine
- 1 cup [demi-glace]({{<relref demi-glace.md>}})
- salt and pepper to taste
- 1 bay leaf
- 1 sprig fresh thyme
- ¼ cup heavy cream (optional)

## Preparation

Melt the butter in a saucepan and sauté the shallots briefly until translucent
(3 min). Deglaze with the Port and red wines and reduce until most of the wine
has cooked off (5-6 min). Add the demi glace, salt, pepper, bay leaf, and thyme
and simmer for approximately 5-6 minutes (or until the sauce has thickened).
Strain the sauce, add the heavy cream (if desired), and return to a simmer for a
few minutes and serve. The optional heavy cream gives the sauce a richer color
and additional flavor.

_Reluctant Gourmet_
