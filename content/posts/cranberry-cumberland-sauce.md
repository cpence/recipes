---
title: "Cranberry Cumberland Sauce"
date: 2021-01-09T21:32:59+01:00
categories:
  - side dish
tags:
  - cranberry
---

## Ingredients

- 2 cups cranberries (about 8 ounces)
- 3/4 cup Tawny Port
- 1/2 cup sugar
- 2 1/2 tablespoons orange juice
- 3/4 teaspoons corn starch
- 1/2 teaspoon dry mustard
- 1/2 teaspoon fresh lemon juice
- pinch ground cloves
- pinch ground ginger
- 3/4 cup golden raisins
- 1 tablespoon grated orange peel
- 1/2 teaspoon grated lemon peel

## Preparation

Combine berries and port in heavy saucepan over medium heat. Cook until berries burst stirring occasionally, about 10 minutes. Add 1/2 cup sugar and salt. Stir 1 minute. Combine orange juice, corn starch, dry mustard, lemon juice and spices in small bowl. Whisk to combine. Stir into berry mixture. Add raisins orange peel, lemon peel. Simmer until thickened, stiring occasionally, about 5 minutes. Season with more sugar if desired.

_Bon Appetit, Nov. 1994_
