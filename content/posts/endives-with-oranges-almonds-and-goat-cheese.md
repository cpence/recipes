---
title: "Endives with Oranges, Almonds, and Goat Cheese"
author: "pencechp"
date: "2010-02-27"
categories:
  - side dish
tags:
  - chevre
  - salad
  - spanish
---

## Ingredients

*   1 bunch endive leaves, cored and cleaned [CP: I have no idea what unit a
    "bunch" is, but it's a lot]
*   8 tablespoons orange segments
*   4 tablespoons sliced toasted almonds
*   8 ounces Spanish goat cheese [or 4 oz. if you're making for two]
*   2 tablespoons chopped chives
*   Salt
*   4 tablespoons [Vinagreta de Ajo Tostado]( {{< relref "vinagreta-de-ajo-tostado" >}} )

## Preparation

Set leaves on a plate. Add oranges, almonds, cheese, chives and salt. Cover with
Vinagreta de Ajo Tostado. Serve.
