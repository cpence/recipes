---
title: "Brussels Sprouts with Pancetta"
author: "pencechp"
date: "2012-12-16"
categories:
  - side dish
tags:
  - brussels sprouts
---

## Ingredients

*   1kg (2.2 lb) Brussels sprouts
*   100g (3.5 oz) thinly sliced pancetta, cut into 2cm strips
*   about 4 tbsp hot goose fat
*   a handful of shredded sage

## Preparation

Blanch the Brussels sprouts in a pan of boiling salted water for 3 mins, then drain and tip into a bowl of iced water to cool quickly. Drain well again and set aside until nearly ready to serve. Sauté the pancetta in the hot goose fat until crisp. Toss in the sprouts and stir-fry for 2-3 mins, adding the sage to serve.

_Gordon Ramsay, BBC Good Food, December 2005_
