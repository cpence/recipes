---
title: "Turkey Gravy from Scratch"
author: "pencechp"
date: "2010-05-08"
categories:
  - miscellaneous
tags:
  - family
  - sauce
  - turkey
---

## Ingredients

*For the turkey stock:*

*   4 tablespoons butter, more if necessary for gravy, and for seasoning (optional)
*   6 turkey legs or other dark meat turkey parts (thighs, wings, etc.), to make about 6 pounds
*   Salt and black pepper
*   1 medium onion, peeled and stuck with 3 cloves
*   3 large carrots, peeled and cut into large chunks
*   3 stalks celery with leaves, trimmed and cut into large chunks
*   2 bay leaves
*   12 black peppercorns
*   1 cup white wine, Madeira, vermouth, dry sherry or water

*For the gravy:*

*   12 tablespoons (3/4 cup) all-purpose flour
*   Salt and black pepper

## Preparation

1\. For the stock: Heat oven to 375 degrees. Melt 4 tablespoons butter. Sprinkle turkey parts with salt and pepper, place in roasting pan and brush with melted butter. Roast 2 hours, basting with butter every 20 minutes or so.

2\. Transfer roasted turkey to a stockpot and set roasting pan aside. Add onion, carrots, celery, bay leaves and peppercorns to stockpot. Add cold water just to cover, bring to a simmer and cook, slightly uncovered, about 6 hours.

3\. Meanwhile, place roasting pan on top of stove and bring juices to a simmer over low heat. Pour in wine (or water), stirring and scraping to bring up browned bits. (If using wine, simmer at least 5 minutes.) Pour all liquid into a bowl and refrigerate. When deglazing liquid is cool, lift off top layer of fat; reserve fat. Add deglazing liquid to stockpot.

4\. When stock is golden and flavorful, strain into a large container and refrigerate. When cool, lift off fat and mix it with reserved fat from deglazing liquid. Reserve 3 quarts stock for gravy and refrigerate or freeze the rest for another use.

5\. For the gravy: In a deep skillet or large heavy pot, melt 12 tablespoons ( 3/4 cup) reserved turkey fat over medium heat. If you do not have enough turkey fat, use additional butter to make 3/4 cup. Gradually whisk in the flour. Cook, whisking, until golden brown and toasty-smelling, 3 to 5 minutes or longer for darker gravy.

6\. Whisk in a small amount of stock (this prevents lumps), then add remainder more quickly and whisk until smooth. Simmer, continually whisking, until thickened. If too thick, thin with more stock or a little wine and simmer briefly. Season with salt and pepper. If desired, whisk in a few tablespoons cold butter to smooth and enrich gravy.

Yield: 3 quarts, about 20 servings.

_The New York Times_
