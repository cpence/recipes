---
title: "Watermelon and Goat Cheese Salad"
author: "pencechp"
date: "2011-09-03"
categories:
  - side dish
tags:
  - chevre
  - salad
  - watermelon
---

## Ingredients

*   1 pound seedless watermelon chunks, cut into small slabs
*   6 ounces fresh goat cheese, cut into 12 rounds
*   1/4 teaspoon finely grated orange zest
*   1 teaspoon nigella seeds
*   2 tablespoons fresh orange juice
*   1 tablespoon fresh lemon juice
*   3 tablespoons extra-virgin olive oil
*   Salt and freshly ground pepper

## Preparation

Arrange the watermelon slabs on 6 plates and top with the fresh goat cheese
rounds. Sprinkle all over with the orange zest and nigella seeds.

In a small bowl, whisk the orange juice and lemon juice with the olive oil and
season with salt and pepper. Drizzle the dressing over the watermelon and goat
cheese and serve right away.

This doesn't keep well, so you want to eat it all on the first day!

_Food & Wine_
