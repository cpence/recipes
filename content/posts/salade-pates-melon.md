---
title: "Salade de pâtes au melon, mozzarella et jambon croustillant"
date: 2021-08-09T10:23:45+02:00
categories:
  - side dish
tags:
  - salad
  - pasta
  - melon
  - untested
---

## Ingredients

- 160 g de jambon de Parme
- 400 g de tomates cerise
- 1/2 melon jaune (450 g)
- 1 oignon rouge
- 1 citron
- 50 g d’amandes de Californie
- 1 botte de basilic
- 250 g de petites pâtes grecques
- 250 g de bocconcini di mozzarella di bufala (Garofalo)
- 4 c. à s. d’huile d’olive
- Poivre et sel

## Preparation

Préchauffez le four à 180 °C. Chiffonnez un peu les tranches de jambon sur la
plaque du four recouverte de papier cuisson. Faites-les rôtir ± 10 min, jusqu’à
ce qu’elles soient croustillantes (à surveiller !).

Faites cuire les pâtes « al dente », à l’eau bouillante salée, selon les
instructions sur l’emballage.

Faites griller les amandes à feu modéré dans une poêle antiadhésive, sans
matière grasse.

Pelez et épépinez le melon, puis taillez-le en petits morceaux. Coupez les
tomates cerises en 2. Émincez l’oignon rouge. Hachez le basilic. Égouttez les
bocconcini. Hachez grossièrement les amandes. Mettez le tout dans un grand
saladier.

Les pâtes étant cuites, égouttez-les et rincez-les longuement à l’eau froide.
Ajoutez-les dans le saladier, joignez le jus de citron et l’huile d’olive,
poivrez et salez ; mélangez bien.

Servez frais en parsemant de jambon à la dernière minute pour qu’il reste
croustillant.

_Delhaize_
