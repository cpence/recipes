---
title: "Crostata di Limone"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - dessert
tags:
  - italian
  - lemon
  - untested
---

## Ingredients

*   1 3/4 cup all-purpose flour
*   1/2 cup ground toasted almonds
*   1/2 cup sugar
*   4 ounces (1 stick) unsalted butter
*   1 egg, plus 4 eggs, separated
*   Pinch salt
*   3/4 cup sugar
*   2 lemons, zested, plus 3 lemons, juiced

## Preparation

Grease the bottom and sides of a 9-inch tart pan with butter and set aside.

Toss together the flour, almonds, and sugar. Cut the butter into the dry mixture until it is the consistency of fine bread crumbs. Add 1 egg and salt and mix well, kneading slightly. Form the pastry into a ball, wrap in plastic wrap, and refrigerate while you make the filling.

Beat the 4 egg yolks with the sugar until very thick, then beat in the lemon zest and juice. Transfer the mixture to the top of a double boiler and set over barely simmering water. Cook, stirring constantly, until the mixture becomes thick, about 15 to 20 minutes. Remove from the heat and set aside to cool slightly.

Preheat the oven to 450 degrees F.

Roll the dough, if you can, between 2 sheets of waxed paper. Place in a pan and prick the dough with a fork all over the bottom, line with waxed paper, and fill with dried beans or rice to keep the bottom from puffing. Place in the oven and bake 10 minutes, then remove the paper and beans, and cook an additional 5 minutes.

Meanwhile, beat the 4 egg whites until very stiff and fold them into the cooled lemon mixture.

Remove the pie casing from the oven and reduce the temperature to 325 degrees F. Spread the lemon filling over the pie shell and bake for 10 to 15 minutes, or until the filling is thoroughly set.

_Mario Batali_
