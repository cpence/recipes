---
title: "Lentil Stew"
author: "pencechp"
date: "2014-10-03"
categories:
  - main course
tags:
  - lentil
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   3/4 c. lentils, rinsed
*   4 tsp. olive oil, divided
*   1/3 c. diced carrot
*   1/3 c. diced celery
*   1 tbsp. tomato paste
*   2 cloves garlic, crushed and minced
*   1 1/2 c. dry red wine
*   1 1/2 c. water
*   1 tsp. dijon mustard
*   1-2 tsp. spice blend (optional)
*   1/4-1 tsp. salt
*   1/4-1/2 tsp. pepper
*   12 red pearl onions
*   1 lg. bunch kale, washed and chopped coarsely
*   1 tbsp. walnut or almond oil
*   loaf rustic bread

## Preparation

Parboil the lentils for 5 minutes, drain, and rinse. Heat 1 tbsp. olive oil in a saucepan over medium-high heat. Add the carrots and celery. When slightly browned, add garlic and tomato paste and mash into the vegetables. Pour in the wine, water, and mustard. Add the drained lentils, herbs, and a bit of salt. Simmer, covered, until lentils are tender, 30-40 minutes.

While the lentils are cooking, blanch the onions in boiling water for 1 minute, drain, and peel. Put them in a heavy frying pan with the other tbsp. of olive oil and cook over medium heat until tender and beginning to color, about 5 minutes. Add a splash of water or wine near the end to deglaze the pan. Season with salt and pepper and remove from skillet.

Wilt the kale in the skillet, season with salt and pepper. Stir into the lentils. Serve with a swirl of walnut oil, a garnish of pearl onions, and with a thick, toasted slice of rustic bread.

_Penzey's Spices_
