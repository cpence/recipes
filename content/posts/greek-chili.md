---
title: "Greek Chili"
author: "pencechp"
date: "2014-03-23"
categories:
  - main course
tags:
  - greek
  - stew
  - untested
---

## Ingredients

*   2 poblano peppers
*   1 head garlic
*   1 large onion olive oil
*   1 c. pistachios
*   1 lb. ground lamb
*   1 lb. ground pork
*   2 cans diced tomatoes
*   2 cans white beans
*   oregano
*   feta cheese

## Preparation

Roast the poblanos, garlic, and onion in the oven (with some olive oil) until soft and a bit charred. Skin the poblanos. Roughly chop them up, add more olive oil, and the pistachios, and toss everything into a blender or food processor. Turn it into a paste.

Brown the ground lamb and pork in a dutch oven, and add the paste. Cook for a while. Add two cans of tomatoes, two cans of white beans, and some oregano. Simmer everything together for a while. Add feta and serve.

_Friends from church_
