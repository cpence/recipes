---
title: "Pain d'Épices Belge"
date: 2020-03-17T18:37:16+01:00
categories:
    - dessert
    - bread
---

## Ingredients

*   400 g of flour; preferably 50/50 white and rye
*   1 packet of chemical leavening (FIXME: how much is this?)
*   200 g honey (preferably creamy, if available)
*   50 g brown sugar
*   20 cl milk
*   1 tsp. ground anise
*   1 tsp. cinnamon
*   1 tsp. ground ginger
*   1 pinch ground nutmeg
*   1 pinch ground clove
*   1 pinch of grated orange zest
*   100 g of candied citrus zest or ginger (optional)
*   1 tbsp. sliced almonds

## Preparation

Gently heat the milk in a pot and add the honey and sugar to melt. Whisk to ensure it's completely combined. Let cool around 10 minutes.

In a bowl, add the flours, leavening, and powdered spices and grated orange zest. Mix well. Add the milk mixture in batches, mixing well. If desired, add candied fruit/ginger.

Butter a large cake pan and add the dough. Let rest 1 hour before baking. Top with sliced almonds, if desired.

Bake at 285F/140C for around 90 minutes. The cake is cooked when the tip of a knife comes out clean. If it browns too quickly, cover the top with aluminum foil.

Remove from the pan and let cool on a rack. It tastes better if you let it rest 48h before eating.

*two random recipes I found online, translated*

