---
title: "Marinated Olives with Tangerine and Rosemary"
author: "pencechp"
date: "2013-11-26"
categories:
  - side dish
tags:
  - olive
---

## Ingredients

*   1 pound assorted olives (such as Kalamata, Gaeta, and picholine)
*   1 small tangerine, cut into 4 wedges, each wedge thinly sliced crosswise
*   1 tablespoon coarsely chopped fresh rosemary
*   1 teaspoon fennel seeds, lightly crushed
*   1 teaspoon coriander seeds, lightly crushed
*   1/8 teaspoon dried crushed red pepper

## Preparation

Drain olives if in brine. Combine all ingredients; mix well. Cover and refrigerate for 2 days, turning and shaking several times.

_Bon Appetit, November 2006_
