---
title: "Chai Spiced Buttermilk Pancakes"
author: "juliaphilip"
date: "2011-01-08"
categories:
  - breakfast
tags:
  - pancake
---

## Ingredients

*   2 eggs
*   1 1/2 c buttermilk
*   1/2 c brewed chai tea
*   4 tbl butter, melted and cooled
*   1/2 tsp vanilla extract
*   2 c flour
*   3 tbl sugar
*   2 tsp baking powder
*   1 tsp baking soda
*   1 tsp salt
*   3/4 tsp cinnamon
*   1/4 tsp ground cardamom
*   healthy pinch ground cloves
*   1/8 tsp ground ginger
*   1/8 tsp crushed black peppercorns

## Preparation

In a large bowl beat eggs. Add buttermilk, chai tea, butter and vanilla and mix well. Add flour, sugar, baking powder, baking soda, spices and salt. Mix well until mostly smooth. Let batter set for a few minutes.

Heat griddle or pan over medium heat. Add a teaspoon of oil (I used grapeseed oil) to the pan or spray with cooking spray. You can test to see if the pan is hot enough by adding a few drops of water, when the drops start to dance its hot enough.

Pour 2 Tablespoons of batter onto the griddle. Cook on the first side until bubbles that form start to pop. You can also gently lift up the pancake to make sure the bottom is not overcooking, if it is the pan may be too hot and you will need to adjust the heat. Flip the pancake over with a spatula and cook until golden brown. Repeat until all the batter is gone. Let cooked pancakes rest on a heat proof plate in a 200 degree F oven until ready to serve. Maple syrup… that’s nice!
