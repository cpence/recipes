---
title: "Braised Pine Nuts with Butternut Squash"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
tags:
  - italian
  - squash
  - untested
  - vegetarian
---

## Ingredients

*   1 small butternut squash (2 1/4 pounds)
*   1 tablespoon extra-virgin olive oil, plus more for drizzling
*   Salt and freshly ground pepper
*   1 cup Italian pine nuts (3 1/2 ounces)
*   1 large shallot, minced
*   1 teaspoon tomato paste
*   1/2 cup dry white wine
*   1 cup low-sodium chicken broth
*   Pinch of saffron threads
*   Finely grated orange zest
*   1/2 cup Parmigiano-Reggiano shavings

## Preparation

Preheat the oven to 350°. Peel and slice the butternut squash 1/2 inch thick; spread on a baking sheet, drizzle with olive oil and season with salt and pepper. Cover with foil and roast until tender but not browned, 45 minutes. Meanwhile, spread the pine nuts on a baking sheet and toast until golden, 4 minutes.

Transfer the squash to a food processor and puree until smooth. Set aside 1 cup of the puree and reserve the rest for another use.

Heat the 1 tablespoon of olive oil in a pressure cooker. Add the shallot and cook over moderate heat, stirring, until softened, 4 minutes. Add the pine nuts and tomato paste and cook, stirring, for 2 minutes. Add the wine and boil until reduced to 2 tablespoons, 5 minutes. Add the broth. Cover and cook at 15 PSI (see Note) for 7 minutes. Place the pressure cooker in the sink and run cold water over the lid to depressurize it rapidly; remove the lid once it can be released without force.

Return the pressure cooker to medium heat, uncovered, and bring the contents to a boil. Allow the liquid to reduce until the pine nuts are suspended in a thick sauce, about 4 minutes. Stir in the reserved squash puree and saffron and cook until heated through. Stir in a tablespoon of water if the puree is too thick. Season with salt and pepper. Spoon the mixture into bowls, garnish with orange zest and the cheese shavings and serve.

_Food & Wine_
