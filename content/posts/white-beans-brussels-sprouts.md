---
title: "Smoky White Beans and Brussels Sprouts"
date: 2021-01-09T22:40:18+01:00
categories:
  - main course
  - side dish
tags:
  - bean
  - brussels-sprouts
---

## Ingredients

- 2 tablespoons olive oil
- 1 pound Brussels sprouts, trimmed and halved
- 2 garlic cloves, chopped
- 3/4 teaspoon Spanish smoked paprika (pimenton)
- 1/2 teaspoon fine sea salt, plus more to taste
- 1/4 to 1/2 teaspoon crushed red pepper flakes
- 1/4 teaspoon ground black pepper, plus more to taste
- 1 1/2 cups cooked or canned no-salt-added white beans (from one 15-ounce can), drained and rinsed
- 3/4 cup drained bean cooking liquid or water
- 2 tablespoons fresh lemon juice
- 2 tablespoons chopped fresh chives

## Preparation

In a large skillet over medium-high heat, heat the oil until shimmering. Add the Brussels sprouts, cut-side down and in one layer. Cook, without disturbing, until deep golden brown on the bottom, 5 minutes. Turn them over, add the garlic and cook until the sprouts are just barely tender, 2 to 3 minutes.

Stir in the smoked paprika, salt, red pepper flakes and pepper and cook until fragrant, 30 seconds. Add the beans and bean liquid or water and cook, stirring often, until the beans are heated through, the liquid has reduced and turned creamy, and the sprouts are tender, about 5 minutes.

Stir in the lemon juice and chives, taste, and add more salt and pepper if needed. Serve hot.

_WaPo_
