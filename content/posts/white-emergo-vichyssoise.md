---
title: "White Emergo Vichyssoise"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - side dish
tags:
  - bean
  - soup
---

## Ingredients

*   1 cup white emergo beans
*   4 leeks, white part only, split, washed well and sliced
*   1 tbsp butter
*   1 medium onion, diced
*   4 cups cooked chicken broth
*   1 cup half and half
*   sea salt and fresh black pepper to taste

## Preparation

Rinse and pick over beans. Pour boiling water over beans to cover by 2 inches and leave for 1 hour. Drain, rinse, and add water to cover by 2 inches. Bring to a boil, let cook at slow boil until tender.

In a skillet, melt butter and lightly saute onions and leeks. Do not let them brown.

Drain beans, add chicken broth and vegetables. Heat over medium heat for 10 minutes. Remove from heat, add half and half. Puree in small batches in blender. Chill.

Serve chilled and garnished with fresh minced chives, salt and pepper to taste.
