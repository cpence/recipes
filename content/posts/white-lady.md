---
title: "White Lady"
author: "pencechp"
date: "2011-04-28"
categories:
  - cocktails
tags:
  - gin
---

## Ingredients

*   2 parts gin
*   1 part Cointreau
*   1 part lemon juice

## Preparation

Shake with ice, strain, serve in martini glass.
