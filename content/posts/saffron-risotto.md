---
title: "Saffron Risotto"
date: 2020-04-11T13:46:53+02:00
categories:
  - side dish
tags:
  - italian
  - rice
---

## Ingredients

- 1.5 L chicken stock
- 80 g butter
- 1 onion
- 350g risotto rice
- 1 glass dry white wine
- 1/2 tsp. saffron threads
- 80g Parmesan cheese, grated

## Preparation

Bring the stock to the boil. Melt 2/3rds of the butter in a saucepan and add the finely chopped onion, stirring occasionally on a low heat for 5 minutes. Stir in the risotto rice so it’s evenly coated in butter and starts to turn translucent. Pour in the wine and cook until it has evaporated.

Then start to add the broth, one ladle at a time, so the rice slowly absorbs the liquid. Add more stock as the saucepan goes dry. This will take about 20-25 minutes. The rice should be tender with a pleasant bit of bite in the middle.

Before you add the last ladle of stock, stir in the saffron threads and a pinch of salt. After all the stock has been absorbed take the pan off the heat and stir in the remaining butter and the grated parmesan. Serve.

_Nudo_
