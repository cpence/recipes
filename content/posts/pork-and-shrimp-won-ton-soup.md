---
title: "Pork and Shrimp Won Ton Soup"
author: "juliaphilip"
date: "2017-01-03"
categories:
  - appetizer
  - main course
tags:
  - chinese
  - pork
  - shrimp
  - soup
---

## Ingredients

*   ½ pound ground pork, not too lean
*   ½ pound fresh shrimp, peeled, deveined and roughly chopped in 1/4-inch pieces
*   Salt and pepper
*   1 tablespoon sweet rice wine, such as Shaoxing rice wine (or use sherry)
*   1 tablespoon soy sauce
*   1 tablespoon sugar
*   1 tablespoon finely grated ginger
*   2 cloves garlic, minced
*   1 teaspoon spicy Chinese bean paste, also called chili bean sauce (or use chile paste)
*   2 serrano chiles, finely chopped
*   1 ½ cups chopped Chinese garlic chives (or use 3/4 cup chopped scallions, green and white parts)
*   36 wonton skins, about 3 by 3 inches, available at Asian markets and many grocery stores
*   1 small egg, beaten
*   Cornstarch for dusting
*   8 ounces baby spinach leaves
*   ½ cup chopped cilantro
*   8 cups good chicken broth, hot, salted to taste
*   Red pepper oil (optional)

## Preparation

Put pork and shrimp in a chilled mixing bowl. Season with salt and pepper and mix briefly with chopsticks, wet hands or wooden spoons. Add rice wine, soy sauce, sugar, ginger, garlic, bean paste, serrano chiles and garlic chives. Mix well to incorporate. Pan-fry a small flat patty in a small amount of oil to check seasoning; taste and adjust. Transfer mixture to a small container, cover and chill at least 30 minutes, or longer if you have time, up to 24 hours.

To prepare wontons, remove a few wonton skins from package and lay them on dry work surface. Put 1 teaspoon filling in the center of each square skin. Paint edges of square lightly with egg. Gently fold one side over the other, pinching edges together. You should a have a folded rectangle. Now pull the lower corners in toward each other and pinch together to make the traditional curved wonton shape. Place wontons 1 inch apart on a baking sheet or platter. Dust lightly with cornstarch and refrigerate, uncovered, until ready to cook.

Bring a large pot of well-salted water to a boil. Meanwhile, put a small handful of spinach leaves and about 2 tablespoons cilantro in each person’s deep wide soup bowl. When water is boiling, drop about 10 wontons into pot and cook for 2 minutes. Remove with wire bamboo spider (or a large fine-meshed sieve with a handle) and divide among bowls. Repeat with remaining wontons. Pour about 1 1/2 cups hot broth over each serving. Drizzle with red pepper oil if desired.

_New York Times_
