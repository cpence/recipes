---
title: "Lemon Poppy Seed Muffins"
author: "pencechp"
date: "2010-05-08"
categories:
  - breakfast
tags:
  - muffin
---

## Ingredients

*   1 1/2 cups sugar
*   1/2 cup butter, softened
*   1 tbsp lemon extract
*   1 tsp vanilla extract
*   4 eggs
*   1 tsp minced lemon peel, rehydrated in 1tbsp water
*   1/2 cup sour cream
*   1/4 cup blue poppy seeds
*   2 cups AP flour
*   2 tsp baking powder
*   1/2 tsp salt
*   3/4 cup whole milk

## Preparation

In a small bowl, combine flour, baking powder, and salt. In a larger bowl, beat together sugar and butter until well blended, then add the lemon extract, vanilla extract, and eggs and beat until fluffy. Mix in lemon peel, sour cream and poppy seeds. Add the flour mixture and milk in alternating increments, beating and scraping after each addition. Batter should be thick and fluffy.

Fill cupcake liners approx. 2/3rds full. Bake for 20-24 minutes, rotating pans half-way.

_Penzey's Spices_
