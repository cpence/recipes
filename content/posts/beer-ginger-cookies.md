---
title: "Beer Ginger Cookies"
date: 2020-06-06T14:10:11+02:00
categories:
    - dessert
tags:
    - cookie
    - beer
---

## Ingredients

*for the beer molasses:*

*   1 1/2 cup dark or light brown sugar
*   1/2 cup amber/brown beer
*   1/8 teaspoon cream of tartar
*   1 teaspoon lemon juice

*for the cookies:*

*   1 cup sugar
*   3/4 cup butter
*   1/4 cup beer molasses
*   1 teaspoon kosher salt
*   1/2 teaspoon ground cinnamon
*   1 tablespoon ground ginger
*   2 teaspoon baking soda
*   1 egg
*   2 1/4 cup all-purpose flour

## Preparation

*For the beer molasses:*

In a medium saucepan over medium-high heat, combine sugar, beer, cream of tartar
and lemon juice. Stir gently until the mixture reaches a boil, and once it
boils, do not stir again.

Let the mixture boil and thicken for 1 minute, and then remove from heat. Let
mixture cool completely before using it in the recipe. You will have more
molasses than you’ll use in this recipe; store the rest in the fridge for
another use or more cookies.

*To make the cookies:*

Heat oven to 350F/175C.

Bring all of the remaining ingredients to room temperature before starting to
mix the cookies.

In a medium-size bowl, sift flour and set aside. In a large mixing bowl, cream
butter and sugar until light and fluffy using a spatula or the paddle attachment
if using a stand mixer.

Add beer molasses, salt, cinnamon, ginger and baking soda with butter mixture
and mix until combined. Add eggs and beat until mixture until combined, making
sure you scrap the bottom of the bowl between adding additional ingredients.

Add sifted flour and mix gently until combined. Place cookie dough in a bowl and
cover with plastic wrap. Let dough rest in the refrigerator for at least 1 hour,
up to 24 hours.

Once the dough has rested, use a cookie scoop to evenly portion out
cookies. Right before baking, roll cookies in granulated sugar and place evenly
spaced on the baking sheet. Bake for 6 minutes. Rotate the baking sheet and bake
cookies for another 6 minutes.

*AAS*
