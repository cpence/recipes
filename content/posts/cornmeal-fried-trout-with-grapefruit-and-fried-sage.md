---
title: "Cornmeal-fried Trout with Grapefruit and Fried Sage"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - fish
---

## Ingredients

*   1 large red grapefruit
*   Four 6-ounce skinless trout fillets
*   Salt and freshly ground pepper
*   All-purpose flour, for dusting
*   1 large egg beaten with 1 tablespoon water
*   Yellow cornmeal, for dredging
*   3 tablespoons unsalted butter, chilled
*   8 large sage leaves
*   Vegetable oil, for frying
*   1/4 cup water

## Preparation

Using a sharp knife, remove the peel and bitter white pith from the
grapefruit. Working over a medium bowl, carefully cut in between the membranes
to release the sections into the bowl. Squeeze the remaining grapefruit juice
into a separate bowl. [CP: If you do this, the grapefruit will fall apart when
it's cooking in the butter sauce. I haven't figured out how to stop that yet.]

Put the flour, egg and cornmeal in 3 shallow bowls. Season the trout fillets
with salt and pepper and lightly dust them with flour, tapping off the
excess. Dip the fillets in the beaten egg, then dredge them in the cornmeal.

In a large skillet, melt 1 tablespoon of the butter. Add the sage leaves and
cook over moderate heat, turning once, until crisp, about 2 minutes per
side. Transfer the sage leaves to a plate lined with paper towels to drain. Add
1/4 inch of the vegetable oil to the skillet and heat until
shimmering. Carefully add the trout fillets and fry over moderately high heat
until golden brown and just cooked through, about 3 minutes per side. Divide the
fillets among 4 plates.

Discard the oil and wipe out the skillet with paper towels. Add the fresh
grapefruit juice and water to the skillet and bring to a boil; cook for 1
minute. Reduce the heat to moderate, add the grapefruit sections and simmer the
sauce for 1 minute. Remove the skillet from the heat and swirl in the remaining
2 tablespoons of butter, 1 tablespoon at a time. Season with salt and pepper and
spoon the sauce and grapefruit sections over the trout. Garnish each fillet with
2 sage leaves and serve immediately.

**Pumpkin Seed Crust:** In a food processor pluse 1/2 c of raw, unsalted pumpkin
seeds until coarsely ground. After dusting the fish filets with flour and
dipping them in the beaten egg, dredge them in the ground pumpkin seeds instead
of cornmeal and fry as directed.

_Marcia Kiesel, Food and Wine April 2008_
