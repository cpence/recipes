---
title: "Microwave Strawberry Pie"
author: "juliaphilip"
date: "2010-07-04"
categories:
  - dessert
tags:
  - philip
  - strawberry
---

## Ingredients

*   5 cups fresh strawberries
*   1 cup water
*   3 tablespoons corn starch
*   3/4 c sugar
*   1 9inch graham cracker pie crust
*   cool whip for serving

## Preparation

Place 4 cups strawberries pointing up in pie shell. Mash remaining strawberries. Cook water and mashed strawberries (1 cup). Cook in microwave on high 4 minutes, stirring once. Combine sugar and cornstarch. Stir into strawberry juice. Cook 2 - 3 minutes in microwave on high until transparent, stirring often. Pour glaze over berries, chill. Serve with cool whip.

_Emily Weiburg_
