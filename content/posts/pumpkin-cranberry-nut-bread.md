---
title: "Pumpkin Cranberry Nut Bread"
author: "pencechp"
date: "2014-10-03"
categories:
  - bread
  - dessert
tags:
  - cranberry
  - pumpkin
  - untested
---

## Ingredients

*   3 1/2 c. flour
*   2 tsp. cinnamon
*   1 tsp. salt
*   1 tsp. baking soda
*   1/2 tsp. baking powder
*   2 tsp. grated orange peel
*   3/4 c. butter, softened
*   1 c. sugar
*   1 c. brown sugar
*   3 eggs
*   1/2 tsp. vanilla
*   1 15 oz. can pumpkin
*   1 c. fresh, chopped cranberries
*   1 c. chopped walnuts or pecans

## Preparation

Preheat oven to 350. Combine dry ingredients and set aside. Cream butter and sugar. Add eggs, one at a time, mixing after each one. Add vanilla, pumpkin, and dry mixture about 1/2 c. at a time, alternating between pumpkin and dry mix until all is added. Stir in cranberries and nuts. Divide batter between two 8x4 1/2" greased and floured loaf pans and bake for 55-65 minutes.

_Penzey's Spices_
