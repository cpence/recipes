---
title: "Peas in a Roux"
author: "pencechp"
date: "2014-12-02"
categories:
  - side dish
tags:
  - cajun
  - peas
---

## Ingredients

*   1 can of petit pois peas
*   4 tablespoons bacon grease
*   4 tablespoons flour
*   1 large onion, chopped
*   1 1/2 tablespoons sugar
*   1/2 teaspoon cayenne pepper (or less, to taste)
*   Salt and pepper to taste
*   2 tablespoons butter

## Preparation

Make a dark roux with the bacon grease and flour: Melt the bacon grease in a saucepan. Stir in the flour and continue stirring over medium-low heat for 10 to 12 minutes, or until the roux turns a chocolate color.

Add the onions and saute 5 minutes. Sprinkle sugar on the onion and cook 2 minutes. Add the peas (including the packing liquid in the can), cayenne pepper, salt and pepper. Reduce heat to low and cover. Simmer peas for 10 minutes. Stir in butter and serve.

_NPR_
