---
title: "Bacon Quinoa with Almonds and Herbs"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
  - side dish
tags:
  - bacon
  - quinoa
---

## Ingredients

*   1/3 cup slivered almonds
*   1 teaspoon vegetable oil
*   2 thick slices of applewood-smoked bacon, cut into 1/4-inch dice
*   1 small shallot, minced
*   1 cup quinoa, rinsed
*   2 cups chicken stock
*   1 sage sprig
*   1 tablespoon minced chives
*   1 tablespoon chopped parsley
*   Salt and freshly ground white pepper

## Preparation

Preheat the oven to 350°. Spread the almonds in a pie plate and toast in the oven until golden brown, 4 minutes; let cool.

In a medium saucepan, heat the oil. Add the bacon and cook over moderately high heat until the fat has rendered, about 2 minutes. Add the shallot and cook, stirring a few times, until softened but not browned, about 1 minute. Add the quinoa, stock and sage and bring to a boil. Cover and cook over low heat until the stock has been absorbed, about 17 minutes. Remove the quinoa from the heat and let stand, covered, for 5 minutes. Discard the sage and fluff the quinoa with a fork. Stir in the chives, parsley and toasted almonds. Season the quinoa with salt and pepper and serve.

_Food & Wine_
