---
title: "Double Blueberry Whole-Wheat Pancakes"
author: "pencechp"
date: "2010-05-09"
categories:
  - breakfast
tags:
  - blueberry
  - pancake
  - untested
---

## Ingredients

*Sauce:*

*   1/4 c. water
*   1 tsp. corn starch
*   1 1/2 c. blueberries
*   1/4 c. sugar
*   1/8 tsp. salt
*   2 tsp. lemon juice

*Pancakes:*

*   1 c. milk
*   1 tbsp. butter, melted
*   1 egg, beaten
*   1/2 c. whole-wheat flour
*   1/2 c. all-purpose flour
*   3 tbsp. sugar
*   1/4 tsp. salt
*   1/2 c. blueberries

## Preparation

To make sauce, stir together water and cornstarch in saucepan.  Stir in blueberries, sugar, and salt.  Simmer 5 minutes or until blueberries cook down and liquid is thickened, stirring occasionally.  Remove from heat; stir in lemon juice.  Cover; set aside.

To make pancakes, stir together milk, butter, and egg in small bowl.  In separate large mixing bowl, stir together dry ingredients.  Pour milk mixture into dry ingredients; stir until just blended.  Fold in blueberries.

Grease griddle or heavy-bottomed skillet.  Heat over medium heat until water sizzles when dropped on griddle.  Drop batter by 1/4 cup measures onto griddle.  Cook pancakes about 3 minutes on each side, or until golden.  Serve pancakes with sauce.

_Meijer_
