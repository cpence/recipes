---
title: "Lemon Bars"
author: "pencechp"
date: "2015-12-28"
categories:
  - dessert
tags:
  - lemon
  - pence
---

## Ingredients

_Crust:_

*   1 1/2 cups flour
*   1/2 cup powdered sugar
*   6 oz. cold unsalted butter, cut in 1/2-inch pieces

_Filling:_

*   6 large eggs
*   3 cups sugar
*   1 cup plus 2 tbsp. fresh lemon juice
*   1/2 cup flour

## Preparation

_Crust:_ Preheat oven to 325. Mix flour and powdered sugar. Cut in butter until pea-sized. Mixture will be very dry. Gently press in bottom of 13-by-9 inch pan. Bake until golden brown, 20 to 25 minutes. Let cool to room temperature and reduce oven to 300.

_Filling:_ Whisk together eggs and sugar until smooth. Stir in lemon juice and then flour. Pour filling on crust and bake until set, about 40 minutes. Cool completely. Cut into 21/4 inch squares. Dust with powdered sugar. Makes 24.
