---
title: "Kashmiri Chicken"
author: "pencechp"
date: "2013-06-13"
categories:
  - main course
tags:
  - chicken
  - curry
  - indian
  - untested
---

## Ingredients

*   3 lb. chicken pieces, skin removed (pref. dark meat)
*   4 tbsp. vegetable oil or ghee
*   3 lg. onions, finely sliced
*   10 black peppercorns, crushed
*   10 cardamom seeds, crushed (or 1/4 tsp. ground cardamom)
*   1 3-in cinnamon stick (or 1/4 tsp. ground cinnamon)
*   1 2-in piece fresh ginger, chopped
*   2-4 garlic cloves, minced
*   1 heaping tsp. chili powder
*   1 heaping tsp. turmeric
*   2 heaping tsp. paprika
*   1/2 tsp. salt (or to taste)
*   1 c. plain yogurt
*   1/2 c. white wine

## Preparation

Heat the ghee or oil in a large skillet with a lid.  Add the onions, pepper, cardamom, and cinnamon and cook until the onions are golden, about 10 minutes.  Add the ginger, garlic, chili powder, turmeric, paprika, and salt, and cook for 2 minutes, stirring occasionally.  Add the chicken pieces and cook until nicely browned, stirring often.  In a medium bowl, mix yogurt with wine.  Reduce heat to low and gradually add yogurt mixture, stirring constantly until fully incorporated.  Reduce heat, cover, and simmer for about 30 minutes.  Serve with rice.

_Serves about 6._

_Penzey's Spices_
