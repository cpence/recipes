---
title: "Scrambled Tofu With Tomatoes, Scallions and Soy Sauce"
author: "pencechp"
date: "2014-09-02"
categories:
  - main course
tags:
  - tofu
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   2 tablespoons neutral oil
*   1 1/2 cups chopped tomatoes
*   Salt and ground black pepper
*   1 pound firm tofu, drained
*   1/3 cup sliced scallions
*   Soy sauce

## Preparation

Put the oil in a deep skillet over medium heat. When hot, add the tomatoes, sprinkle with salt and pepper and cook, stirring occasionally, until their juices release and they begin to dry out slightly, 5 to 10 minutes.

Crumble the tofu with your fingers and add it to the pan along with the scallions. Cook, stirring occasionally, until the tofu is heated through and dried out a bit, 5 to 10 minutes. Serve, drizzling with soy sauce at the table. Note: To make Scrambled Tofu With Corn, Tomatoes and Basil, use olive oil instead of neutral oil and reduce the amount of tomatoes to 1 cup. Substitute 3/4-cup corn kernels for the scallions and stir in some chopped fresh basil before serving. Skip the soy sauce and drizzle with a little more olive oil at the table if you like.

_Mark Bittman, New York Times_
