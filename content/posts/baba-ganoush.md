---
title: "Baba Ganoush"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - side dish
tags:
  - eggplant
  - mediterranean
  - vegan
  - vegetarian
---

## Ingredients

*   1 large eggplant
*   1 clove garlic
*   1/4 - 1/2 cup lemon juice (depending on taste)
*   3 tablespoons tahini
*   1 teaspoon salt
*   3 teaspoons olive oil

*Garnish:*

*   2 tablespoons lemon juice
*   2 teaspoons olive oil

### Preparation:

Preheat oven to 375 degrees and bake eggplant for 30 minutes, or until outside is crisp and inside is soft.

Allow to cool for 20 minutes.

Cut open eggplant and scoop out the flesh into colander and allow to drain for 10 minutes. Removing the excess liquid helps to eliminate a bitter flavor.

Place eggplant flesh in a medium bowl. Add remaining ingredients and mash together. You can also use a food processor instead of by hand. Pulse for about 2 minutes.

Place in serving bowl and top with lemon juice and olive oil. Add other garnishments according to taste.

Serve with warm or toasted pita or flatbread. Enjoy!

_Saad Fayed, About.com_
