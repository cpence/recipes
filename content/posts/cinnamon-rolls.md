---
title: "Cinnamon Rolls"
date: 2020-11-10T14:46:23+01:00
categories:
    - breakfast
tags:
    - pastry
---

## Ingredients

*   1 package yeast (7g)
*   1/2 c. warm water
*   1 c. + more for the pan sugar (divided)
*   1/2 c. scalded milk
*   1/3 c. + 1/2 c. + 4 tbsp. + more for the pan butter, melted
*   1 tsp. salt
*   1 egg
*   3 1/2 to 4 c. flour
*   2 tbsp. ground cinnamon
*   3/4 c. raisins, walnuts, or pecans (optional)
*   2 c. powdered sugar
*   1 tsp. vanilla
*   3 to 6 tbsp. hot water

## Preparation

In a small bowl, dissolve yeast in warm water and set aside.

In a large bowl, mix milk, 1/4 c. of the sugar, 1/3 c. of the melted butter,
salt, and egg. Add two cups of flour and mix until smooth. Add yeast mixture.
Mix in remaining flour until dough is easy to handle. Knead dough on a lightly
floured surface for 5 to 10 minutes. Place in a well-greased bowl, and let rise
until doubled (usually 1 to 1 1/2 hours).

Punch down dough. Roll out on a floured surface into a 9x15" rectangle. Spread
melted butter all over dough. Mix the remaining 3/4 c. of sugar and the
cinnamon, and sprinkle over the dough. Sprinkle with the raisins, walnuts, or
pecans, if using. Starting on the long side, roll up dough. Pinch the edge to
the roll to seal. Cut into 12–15 slices. Coat the bottom of the pan with butter,
and sprinkle it with sugar. Place cinnamon rolls closely together in the pan and
let rise until doubled, about 45 minutes. (If making the night before, let rise and then refrigerate.)

Bake at 350F for about 25–30 minutes, or until nicely browned.

Meanwhile, mix the 4 tbsp. of melted butter, powdered sugar, and vanilla. Add
hot water, 1 tbsp. at a time, until the glaze reaches glaze consistency. Spread
over the rolls after they've slightly cooled.

*Paula Deen*

