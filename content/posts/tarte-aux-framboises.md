---
title: "Tarte aux framboises"
date: 2021-08-09T10:03:12+02:00
categories:
  - dessert
tags:
  - pie
  - raspberry
  - untested
---

## Ingredients

- 1 rouleau de pâte brisée
- 500 g de framboises
- le jus de 2 citrons
- 1 blanc d'œuf
- 1 càs de semoule de maïs
- 1 jaune d'œuf
- 50 g de sucre
- 150 ml d'eau

## Preparation

Déroulez la pâte et déposez-la avec le papier de cuisson dans un moule à tarte.
Découpez le papier de cuisson superflu.

Piquez le fond, recouvrez de papier de cuisson et parsemez de haricots secs.
Faites cuire pendant 15 min au four préchauffé à 200 C. Enlevez les haricots et
laissez le moule à tarte refroidir.

Entre-temps, portez l'eau, le jus de citron et le sucre à ébullition. Battez le
jaune d'œuf avec 2 càs d'eau et la semoule de maïs. Sans cesser de mélanger,
ajoutez ce mélange à l'eau citronnée bouillante. Faites bouillir le tout.
Retirez du feu.

Battez le blanc d'œuf en neige et incorporez-le délicatement à la crème de
citron encore chaude. Laissez refroidir, versez la crème dans le moule à tarte
et recouvrez de framboises. Saupoudrez de sucre glace.

_Pâte brisée Delhaize_
