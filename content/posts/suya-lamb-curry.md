---
title: "Suya Lamb Curry"
date: 2021-07-10T11:39:16+02:00
categories:
  - main course
tags:
  - lamb
  - curry
  - african
  - untested
---

## Ingredients

- 1 kg lamb leg shank, diced
- 2 red onions, sliced
- 4 cloves garlic, sliced
- 4cm piece fresh ginger, peeled and grated
- 2 habañero peppers
- 2 tbsp. coconut oil
- 400ml coconut milk
- salt and pepper
- handful parsley

_for the spice mix:_

- 3 uda pods, deseeded and bark discarded
- 100g cashews, roasted and coarsely ground
- 1/2 tsp. chili powder
- 1/2 tsp. smoked paprika
- 1 tsp. ground ginger
- 1 tsp. garlic powder
- 1 tsp. onion powder

## Preparation

Preheat the oven to 180C fan/gas mark 6.

Using a pestle and mortar, grind all the ingredients for the suya spice mix
until you have a fine powder. In a large bowl, season the diced lamb with the
suya mix, salt and pepper, and set aside to marinate.

Put the onions, garlic, ginger and habanero peppers in a food processor and
blitz to create a paste. Over a medium heat, melt the coconut oil in a large
casserole dish and add the onion paste. Fry for 5 minutes before adding the
marinated lamb. Cook for a few minutes until the meat has browned, then pour in
the coconut milk and heat until it begins to bubble, stirring every so often.
Cover the dish with a lid and transfer to the preheated oven. Cook for 1 hour
and 30 minutes before removing the lid and cooking for a further 30 minutes –
this will allow the sauce to reduce. Taste for seasoning and adjust as you see
fit.

Serve the curry scattered with parsley leaves.

The longer you wait to eat this curry, the better the flavour will be. To reheat
it, place in the oven at 180C fan/gas mark 6 for about 15 minutes, then serve
immediately. If you have a slow cooker, you could just put all the ingredients
in at once and cook for 2 hours on a high setting.

_Lopè Ariyo / BBC_
