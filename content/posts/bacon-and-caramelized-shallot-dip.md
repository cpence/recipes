---
title: "Bacon and Caramelized Shallot Dip"
date: 2022-09-03T11:37:54+02:00
categories:
  - side dish
  - appetizer
tags:
  - dip
  - bacon
---

## Ingredients

- 1 tablespoon unsalted butter
- 1 tablespoon extra-virgin olive oil
- 1 large shallot or 2 smaller shallots, cut into thin slices (enough to yield 1/2 packed cup)
- 4 slices bacon, cooked, drained and cut into small pieces
- 1/2 cup shaved or shredded Parmigiano-Reggiano cheese
- 1/2 cup mayonnaise

## Preparation

Position an oven rack 4 to 6 inches from the broiler element; preheat the broiler. Line a plate with paper towels.

Heat the butter and oil in a nonstick saute pan or skillet over medium heat. Stir in the shallot slices and reduce the heat to medium-low; cook for 20 to 25 minutes, stirring occasionally, until they become golden brown, molten soft and shimmering. Transfer to the lined plate to drain and cool.

Combine the bacon pieces, cheese and mayonnaise in a mixing bowl. Add the cooled shallot and stir until well incorporated. Transfer to a small gratin or baking dish (8-ounce capacity); broil for 2 to 4 minutes, until bubbling and a thin crust forms on top.

Serve right away.

_WaPo_
