---
title: "Tuna with Farro"
author: "pencechp"
date: "2010-03-28"
categories:
  - main course
tags:
  - farro
  - fish
  - italian
---

## Ingredients

*   1 pound farro
*   2 bay leaves
*   salt
*   olive oil
*   2 cloves garlic, chopped
*   1 lg. can crushed tomatoes
*   capers
*   red pepper flakes
*   1 large can tuna or cooked tuna
*   1 c. fresh basil fresh parsley

## Preparation

Cook farro with bay leaves, salt and olive oil according to package directions.

Sauté garlic in olive oil.

Add tomatoes, lots of capers, red pepper flakes and salt to taste.

Stir in canned or baked tuna, flaked, and lots of fresh basil.

Stir in farro and simmer a few minutes.

Sprinkle with parsley and serve.

_Lidia Bastianich_
