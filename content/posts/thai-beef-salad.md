---
title: "Thai Beef Salad"
author: "pencechp"
date: "2013-06-13"
categories:
  - main course
tags:
  - beef
  - salad
  - thai
---

## Ingredients

*   1 teaspoon sweet paprika
*   1 teaspoon cayenne pepper
*   1 tablespoon white rice
*   8 ounces skirt or flank steak, leftover or raw
*   6 cups torn salad greens (mixed is nice)
*   1 cup torn fresh herb leaves (mint, cilantro, Thai basil or a combination)
*   1/4 cup minced red onion
*   1 medium cucumber, peeled if skin is tough, cut in half lengthwise, seeded and diced
*   1 small fresh hot red chili, like Thai, or to taste, minced
*   Juice of 2 limes
*   1 tablespoon sesame oil
*   2 tablespoons fish sauce
*   1/2 teaspoon sugar

## Preparation

Heat paprika and cayenne in 8-inch skillet over medium heat; cook, shaking pan, until fragrant, about 1 minute. Transfer to small bowl. Return now-empty skillet to medium-high heat, add rice, and toast, stirring frequently, until deep golden brown, about 5 minutes. Transfer to second small bowl and cool for 5 minutes. Grind rice with spice grinder, mini food processor, or mortar and pestle until it resembles fine meal, 10 to 30 seconds (you should have about 1 tablespoon rice powder).

If you are starting with raw meat, start a gas or charcoal grill or heat a broiler; rack should be about 4 inches from heat source. Grill or broil beef until medium rare, turning once or twice, 5 to 10 minutes, depending on thickness; set it aside to cool.

Toss greens with herbs, onion and cucumber. In a bowl, combine all remaining ingredients with 1 tablespoon water and 1/4 tsp. paprika mixture; dressing will be thin. Use half of this mixture to toss with greens. Remove greens to a platter.

Slice beef thinly, reserving its juice; combine juice with remaining dressing and half of the rice powder. Lay slices of beef over salad, drizzle remaining dressing over all, and serve with the rest of the rice powder and paprika.

_Serves 4_

_The New York Times_
