---
title: "Vietnamese Pork Sandwiches"
author: "juliaphilip"
date: "2010-02-07"
categories:
  - main course
tags:
  - pork
  - sandwich
  - untested
  - vietnamese
---

## Ingredients

*   1/3 c mayonnaise
*   1/4 tsp five-spice powder
*   2 tbl Asian fish sauce
*   2 tbs soy sauce
*   dash of sugar
*   4 mini french bread loaves or hoagie buns
*   8 strips cucumber--6"x1/4"
*   8 oz thin strips deli or leftover roast pork or Asian barbecue pork
*   1/2 c julienned carrots
*   8 strips seeded deveined jalapeno chili
*   12 sprigs cilantro (leaves and thin stems)

## Preparation

Combine mayonnaise and five-spice powder in a small bowl. Combine fish sauce, soy sauce, 1 tsp water and sugar in another bowl.

Slice bread horizontally without cutting all the way through. Spread about 1 tbs mayonnaise mixture on the inside of each loaf. Top each side with 1 cucumber slice, fill with pork. Top with carrots, chile and cilantro; drizzle lightly with fish sauce mixture.
