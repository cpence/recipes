---
title: "Corpse Reviver No. 1"
author: "pencechp"
date: "2015-11-22"
categories:
  - cocktails
tags:
  - brandy
  - untested
---

## Ingredients

*   1 ½ oz. brandy
*   ¾ oz. Calvados
*   ¾ oz. sweet Italian vermouth

## Preparation

Chill a coupe glass. Stir all ingredients with ice in a mixing glass. Strain into coupe.

_The New York Times_
