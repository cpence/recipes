---
title: "Carrot Ginger Pork Buns"
author: "pencechp"
date: "2015-07-31"
categories:
  - main course
tags:
  - chinese
  - dumpling
  - pork
  - untested
---

## Ingredients

_For the dough:_

*   5 grams instant dry yeast
*   1 teaspoon sugar
*   1 ½ cups lukewarm water
*   4 cups all-purpose flour, plus extra for rolling

_For the filling:_

*   1 cup ground pork
*   1 teaspoon sesame oil
*   1 tablespoon light soy sauce
*   3 tablespoons cooking oil
*   ¼ teaspoon white pepper
*   ¼ teaspoon salt
*   3 large carrots, finely grated (about 4 cups)
*   3 tablespoons oil
*   1 cup finely chopped scallion
*   2 teaspoons finely minced or grated ginger
*   1 teaspoon salt
*   1 teaspoon sesame oil
*   1 teaspoon shaoxing wine

_For fried buns:_

*   2 tablespoons oil
*   ¼ cup water
*   a small handful of finely chopped scallion (optional)
*   2 teaspoons of toasted sesame seeds (optional)

## Preparation

In a large mixing bowl or mixer with a dough hook attachment, completely dissolve yeast and sugar in the lukewarm water. Add the flour and knead for about 15 minutes. The dough should be pretty soft and not too firm. If it seems dry, add a little more water. Cover the mixing bowl with a damp kitchen towel and let it proof one hour.

Combine the first six filling ingredients and mix for a few minutes, until the meat mixture resembles a fine paste, then set aside. Heat a couple tablespoons of oil in a pan over medium heat and cook the grated carrots for a few minutes until they turn color (they shouldn’t be mushy. Cook just until they’re not raw anymore). Let cool completely. Combine the pork mixture, the cooked carrots, and the rest of the filling ingredients. Mix for a couple minutes, until the entire mixture resembles a paste.

After the dough has finished proofing, turn it out on a clean surface dusted with flour. Knead for 2 minutes to get rid of any air pockets. Roll the dough into a long tube and rip off chunks of dough to make individual dough balls. They should be about the size of a golf ball for larger buns, and about half that size for smaller buns.

Take each dough ball, and with a rolling pin, roll from the edge towards the center, without actually rolling the center of the dough too much. The goal is to flatten the dough into a round circle with a thinner edge and thicker center. The difference in thickness should be about 1:2.

Add some filling to the center of each disk (about 1 ½ tablespoons for the larger buns and 2 teaspoons for the smaller ones).

You can start with a smaller amount of filling until you get the hang of the folding. The buns are folded with one hand holding the skin and filling, and the other hand pleating the edges of the dough disk like an accordion. As you fold, the goal is to make it all the way around the circle, until you’ve sealed it at the top. You’ll be making about 10-15 folds. That’s it! Once the top is closed, a bun is born. Lay the buns on a floured surface while you finish assembling them.

Once assembled, let the buns proof under a clean kitchen towel for another 15 minutes before cooking or freezing.

To freeze, lay the buns on a baking sheet lined with parchment paper and put the baking sheet in the freezer. Once the buns are frozen, transfer them to a Ziploc bag, press out as much air as you can from the bag, and freeze for up to two months. To cook, just follow the directions below as if you were cooking them fresh. The cooking times will just be a little longer!

_To make steamed pork buns:_

Boiling water should not directly touch the buns during steaming. Avoid sticking by brushing oil onto the surface the bun sits on or by laying down some kind of natural nonstick surface. In our case, we used corn husks, cut into little squares. Thin napa cabbage leaves will work too. If using a bamboo steamer, brush the sides of the steamer with oil, as the buns expand and might stick to the sides. The surface that the buns sit on should not be solid, like a plate for example. This will trap moisture and make the buns soggy. There should be some cross-ventilation. Make sure the lid is tight so you don’t lose any steam.

Start with cold water, and put your pork buns on the steamer. Turn on the heat to medium. Set the timer for 12-15 minutes for smaller buns and 15-20 minutes for the larger ones. To see if the buns are cooked, press the buns lightly with your finger. If the dough immediately bounces back, they’re done. Keep a close eye on them. Over-steaming will cause the buns to collapse, so cooking time is important.

Once they’re done, turn off the heat, keep the lid on, and let the buns “rest” for about 2 minutes before taking them out. Then eat!

_To make pan-fried pork buns:_

Pre-heat a flat-bottomed cast-iron or other seasoned pan over medium heat. Add the oil and swirl it around the pan to coat it evenly. Add the buns to the pan. Let them cook for a few minutes until the bottoms turn golden brown.

Once golden, add the water to the pan and immediately cover with a lid. Turn the heat down to medium low and let the buns steam for 7-10 minutes until all the water is evaporated.

Uncover the lid, and toss the buns around with scallion and sesame seeds. Done!

_The Woks of Life_
