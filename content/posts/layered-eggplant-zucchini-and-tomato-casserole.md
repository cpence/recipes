---
title: "Layered Eggplant, Zucchini and Tomato Casserole"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
  - side dish
tags:
  - casserole
  - eggplant
  - tomato
  - untested
  - zucchini
---

## Ingredients

*   3 tablespoons extra-virgin olive oil, plus more for greasing and brushing
*   3 medium zucchini (1 1/2 pounds), sliced lengthwise 1/4 inch thick
*   2 long, narrow eggplants (1 1/2 pounds), peeled and sliced lengthwise 1/3 inch thick
*   Salt and freshly ground pepper
*   1 large shallot, minced
*   1 pound plum tomatoes, cut into 1/2-inch dice
*   3 ounces feta cheese, crumbled (3/4 cup)
*   1/4 cup chopped basil
*   1/3 cup panko or coarse dry bread crumbs

## Preparation

Preheat the oven to 425°. Oil 2 large rimmed baking sheets. Put the zucchini slices on one sheet and the eggplant on the other. Brush the slices all over with oil and season with salt and pepper. Arrange the slices on each sheet in a slightly overlapping layer. Bake for 15 minutes, until tender.

Meanwhile, in a large skillet, heat 2 tablespoons of the oil. Add the shallot and cook over moderate heat until softened, 3 minutes. Add the tomatoes and cook over high heat until slightly softened and bubbling, 1 minute. Season with salt and pepper.

Oil a large, shallow baking dish (about 10 by 15 inches). Lay half of the eggplant in the dish and spread one-fourth of the tomatoes on top. Scatter with half of the feta and basil. Layer half of the zucchini on top, followed by another one-fourth of the tomato and the remaining basil, eggplant and zucchini. Top with the remaining tomato and feta. Mix the panko with the remaining 1 tablespoon of oil and sprinkle over the casserole. Bake in the upper third of the oven for 20 minutes, until bubbling and crisp. Let stand for 5 minutes, then serve hot or warm.

_Food & Wine_
