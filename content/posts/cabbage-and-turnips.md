---
title: "Cabbage and Turnips"
author: "pencechp"
date: "2016-12-04"
categories:
  - side dish
tags:
  - cabbage
  - carrot
  - turnip
---

## Ingredients

*   2 Tbs. unsalted butter
*   1 turnip, peeled and cut into 1/4-inch cubes
*   1 carrot, peeled and cut into 1/4-inch cubes
*   1 head savoy cabbage, about 2 lb., quartered, cored and sliced crosswise into strips 1/4 inch wide
*   3/4 cup low-sodium chicken stock
*   Coarse salt and freshly ground pepper, to taste
*   1 Tbs. chopped fresh flat-leaf parsley

## Preparation

In a large saucepan over medium-high heat, melt the butter. Add the turnip and carrot and sauté until lightly browned, about 7 minutes. Add the cabbage and sauté until wilted, about 2 minutes. Stir in the stock, reduce the heat to medium-low and cook, uncovered, until the cabbage is tender and most of the cooking liquid has evaporated, about 10 minutes.

Season with salt and pepper and stir in the parsley. Serve immediately. Serves 6.

_Williams Sonoma_
