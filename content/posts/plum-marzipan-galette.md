---
title: "Plum-Marzipan Galette"
author: "pencechp"
date: "2016-06-08"
categories:
  - dessert
tags:
  - pie
  - plum
  - untested
---

## Ingredients

*   Vegetable oil cooking spray
*   1 1/2 pounds Italian plums, pitted and sliced
*   1/3 cup plus 1 teaspoon sugar, divided
*   1/4 teaspoon ground cardamom
*   2 tablespoons all-purpose flour
*   1 refrigerated ready-to-bake pie crust dough (7.5 ounces)
*   1/4 cup marzipan, crumbled into 1/4-inch pieces
*   1 large egg, lightly beaten

## Preparation

Heat oven to 375°. Coat a large rimmed baking sheet with cooking spray. In a bowl, toss plums with 1/3 cup sugar and cardamom. Dust flour on work surface; roll out dough to a 14-inch diameter and transfer to baking sheet. Leaving a 2-inch border, sprinkle marzipan on dough. Arrange plums in circular pattern over marzipan. Fold and pleat edges toward center. Brush dough with egg and sprinkle with remaining 1 teaspoon sugar. Bake until crust is golden, 22 to 26 minutes. Cool; serve.

_Epicurious_
