---
title: "Smoked Salmon Chowder"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - soup
---

## Ingredients

*   1 tablespoon olive oil
*   3 medium leeks, white and light green parts only, rinsed and sliced (about 3 cups)
*   1 garlic clove, minced
*   1 large russet potato, peeled and cubed
*   1 large stalk celery, chopped
*   1/2 teaspoon kosher salt
*   1/2 teaspoon freshly ground black pepper
*   2 cups vegetable broth
*   2 tablespoons tomato paste
*   2 cups milk (any fat content)
*   8 ounces smoked salmon, flaked
*   1/2 cup heavy cream
*   2 tablespoons chives, chopped

## Preparation

Heat the olive oil in a large, heavy-bottomed pot over low heat. Add the leeks and garlic and sautéthem for 2 minutes.

Add the potato, celery, salt, and pepper and cook over medium heat for about 1 minute, stirring constantly.

Add the broth and simmer until the potato is tender, about 15 minutes.

Add the tomato paste and milk, then the salmon, and bring the mixture back to a simmer for a few minutes (but don't let it boil, or the milk will separate).

As it simmers, stir in the cream.

Remove from heat, garnish with the chives, and serve.

_Cookie, November 2007_
