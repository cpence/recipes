---
title: "Spicy Peanut Sesame Noodles"
author: "juliaphilip"
date: "2010-02-07"
categories:
  - main course
tags:
  - chicken
  - thai
  - vegan
  - vegetarian
---

## Ingredients

*   1 c unsalted peanut butter
*   1/4 c rice vinegar or white vinegar
*   2 tbl light soy sauce \[plus more if necessary--JP\]
*   1 tsp dark soy sauce
*   1 garlic clove, chopped
*   1 to 3 serrano or other chili peppers, seeded and chopped
*   1 1/2 tbl sugar or honey
*   1 tsp salt
*   1/4 c + 2 tsp toasted sesame oil
*   1 tbl chili oil \[this makes it a bit spicy, reduce to ~2.5 tsp--JP\]
*   1/2 c freshly brewed black tea
*   1 lb chinese egg noodles or spaghetti
*   4 c shredded cooked chicken
*   thin strips of peeled seeded cucumber
*   cilantro leaves
*   chopped peanuts

## Preparation

Combine peanut butter, vinegar, both soy sauces, garlic, chili peppers, sugar or honey, salt, 1/4 c sesame oil, chili oil and black tea in a food processor or blender and blend throughly. The sauce can be covered and refrigerated for 1 to 2 days. Allow to return to room temperature and stir well before using.

Cook the egg noodles in boiling water, drain in a colander and rinse under cold water until cool. Toss with 2 tsp sesame oil. Add chicken and sauce and stir together. \[Chicken can be omitted to make the dish vegan--JP\] Garnish with cucumbers, cilantro, and peanuts.

_Joy of Cooking Calendar_
