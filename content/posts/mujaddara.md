---
title: "Mujaddara"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - lentil
  - mediterranean
  - onion
  - rice
---

## Ingredients

*   2 pounds onions, halved and sliced crosswise into 1/4-inch-thick pieces
*   2 teaspoons salt
*   1 1/2 cups vegetable oil
*   1 cup plain whole-milk yogurt
*   2 tablespoons lemon juice
*   1/2 teaspoon minced garlic
*   1/2 teaspoon salt
*   8 1/2 ounces (1 1/4 cups) green or brown lentils, picked over and rinsed
*   Salt and pepper
*   1 1/4 cups rice
*   3 garlic cloves, minced
*   1 teaspoon ground coriander
*   1 teaspoon ground cumin
*   1/2 teaspoon ground cinnamon
*   1/2 teaspoon ground allspice
*   1/8 teaspoon cayenne pepper
*   1 teaspoon sugar
*   3 tablespoons minced fresh cilantro

## Preparation

Toss onions and salt together in large bowl. Microwave for 5 minutes. Rinse thoroughly, transfer to paper towel–lined baking sheet, and dry well. Heat onions and oil in Dutch oven over high heat, stirring frequently, until onions are golden brown, 25 to 30 minutes. Reserve 3 tbsp. of the oil from cooking the onions. Drain onions in colander set in large bowl. Transfer onions to paper towel–lined baking sheet to drain.

Whisk the next four ingredients together in bowl. Refrigerate while preparing rice and lentils.

Bring lentils, 4 cups water, and 1 teaspoon salt to boil in medium saucepan over high heat. Reduce heat to low and cook until lentils are tender, 15 to 17 minutes. Drain and set aside. While lentils cook, place rice in medium bowl and cover by 2 inches with hot tap water; let stand for 15 minutes.

Using your hands, gently swish rice grains to release excess starch. Carefully pour off water, leaving rice in bowl. Add cold tap water to rice and pour off water. Repeat adding and pouring off cold tap water 4 to 5 times, until water runs almost clear. Drain rice in fine-mesh strainer.

Heat reserved onion oil, garlic, coriander, cumin, cinnamon, allspice, 1/4 teaspoon pepper, and cayenne in Dutch oven over medium heat until fragrant, about 2 minutes. Add rice and cook, stirring occasionally, until edges of rice begin to turn translucent, about 3 minutes. Add 2 1/4 cups water, sugar, and 1 teaspoon salt and bring to boil. Stir in lentils, reduce heat to low, cover, and cook until all liquid is absorbed, about 12 minutes.

Off heat, remove lid, fold dish towel in half, and place over pot; replace lid. Let stand for 10 minutes. Fluff rice and lentils with fork and stir in cilantro and half of crispy onions. Transfer to serving platter, top with remaining crispy onions, and serve, passing yogurt sauce separately.

_CI_
