---
title: "Brennan's Shrimp Remoulade"
author: "pencechp"
date: "2016-06-08"
categories:
  - main course
  - side dish
tags:
  - cajun
  - dressing
  - salad
  - shrimp
  - untested
---

## Ingredients

*   1⁄4 cup ketchup
*   1⁄4 cup prepared horseradish
*   1⁄4 cup creole mustard
*   2 tablespoons prepared yellow mustard
*   2 tablespoons white vinegar
*   1⁄2 lemon, juice of
*   1⁄2 cup finely chopped green onion
*   1⁄4 cup finely chopped celery
*   1⁄4 cup finely chopped parsley
*   1 clove garlic, minced
*   1 tablespoon paprika
*   1⁄8 teaspoon cayenne peppe
*   1⁄4 teaspoon salt
*   1 dash Tabasco sauce
*   1 fresh egg (or pasteurized equivalent)
*   3⁄4 cup vegetable oil

## Preparation

Put ketchup, horseradish, Creole and prepared mustards, vinegar, lemon juice, onion, celery, parsley, garlic, paprika, pepper, salt, Tabasco and egg into blender container or food processor. Cover and mix at high speed until well blended. Remove cover and gradually add oil in a slow steady stream. Sauce will thicken to a pourable, creamy consistency.

Store in covered container in refrigerator up to 3 days. Serving suggestion: toss boiled shrimp in remoulade sauce. Spoon onto a bed of thinly sliced cucumbers, radishes and frisee which were drizzled with a simple vinaigrette dressing.

_Food.com_
