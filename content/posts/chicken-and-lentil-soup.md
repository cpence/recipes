---
title: "Chicken and Lentil Soup"
author: "pencechp"
date: "2018-01-01"
categories:
  - main course
tags:
  - chicken
  - instantpot
  - lentil
  - soup
---

## Ingredients

*   1 pound dried brown or green lentils
*   12 to 15 ounces boneless, skinless chicken thighs (fat trimmed)
*   6 c. chicken stock
*   1 c. water
*   1 small yellow onion, coarsely chopped
*   2 scallions, white and light-green parts, chopped
*   1/4 cup chopped cilantro leaves
*   3 cloves garlic, minced
*   1 medium-size ripe tomato, cut into 1/2-inch dice
*   1 teaspoon granulated garlic (powder)
*   1 teaspoon ground sumac (may substitute cumin)
*   1/4 teaspoon dried oregano
*   1/2 teaspoon sweet paprika
*   1/2 teaspoon kosher salt, or more as needed

## Preparation

Combine the lentils, chicken (to taste), water, stock, onion, scallions, garlic,
tomato, cilantro, granulated garlic, ground sumac, oregano, paprika and salt in
the pressure cooker or Instant Pot.

Lock the pressure-cooker lid in place. Turn the heat to high; once the pot
reaches HIGH pressure, cook for 30 minutes, adjusting the heat to maintain
pressure, as needed. For the Instant Pot, lock the lid and press the SOUP button
and cook for 30 minutes. Use the pressure releases, then unlock and open.

Transfer the chicken to a cutting board; use two forks to shred it, then return
it to the pot, stirring to incorporate. Taste, and adjust the salt, as needed.

_Washington Post_
