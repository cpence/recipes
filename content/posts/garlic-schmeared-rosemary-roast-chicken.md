---
title: "Garlic-Schmeared Rosemary Roast Chicken"
author: "juliaphilip"
date: "2009-12-08"
categories:
  - main course
tags:
  - chicken
---

## Ingredients

*   1 (3 1/2 pound) free range chicken
*   2 tablespoons olive oil
*   1 tablespoon salt
*   1/4 teaspoon freshly ground black pepper
*   2 tablespoons finely chopped rosemary (reserve stems)
*   1/3 cup roast garlic puree

## Preparation

Preheat oven to 350 degrees F.

Rinse the chicken well inside and out and pat dry with paper towels. Rub chicken all over with olive oil, and season, inside and out, with salt and pepper. Sprinkle rosemary all over chicken (place stems inside cavity) and roast in the oven for 1 to 1 1/4 hours, until chicken is almost cooked through. Remove chicken from the oven and schmear roast garlic puree all over outside of chicken and return to the oven for 15 minutes, until chicken is cooked through.

_Emeril Lagasse_
