---
title: "Spicy Szechuan Eggplant"
author: "pencechp"
date: "2010-05-28"
categories:
  - main course
tags:
  - chinese
  - eggplant
  - pork
---

## Ingredients

*   1/4 pound ground pork

_Marinade:_

*   1/4 teaspoon salt
*   1/4 teaspoon black pepper
*   a Pinch (less than 1/2 teaspoon) cornstarch

*Sauce:*

*   1/4 cup chicken broth
*   2 tablespoons dark soy sauce
*   2 tablespoons light soy sauce
*   1 teaspoon red rice vinegar, red wine vinegar, or balsamic vinegar
*   1 teaspoon granulated sugar, or to taste
*   2 teaspoons cornstarch

_Other:_

*   4 Asian eggplants
*   1 cloves garlic
*   4 tablespoons oil for stir-frying, or as needed
*   1 tablespoon bean sauce (hot bean sauce if possible)
*   1/2 teaspoon chile paste with garlic, or to taste
*   1 teaspoon sesame oil

## Preparation

Combine the ground pork with the salt, pepper, and cornstarch. Marinate the pork for 15 minutes.

While the pork is marinating, prepare the remaining ingredients. In a small bowl, combine the chicken broth, dark and light soy sauce, vinegar, and sugar. Whisk in the cornstarch.

Cut the eggplant into 1-inch squares. Mince the garlic.

Heat the wok over medium-high to high heat. Add 2 tablespoons of oil. When the oil is hot, add the eggplant. Stir-fry the eggplant until it is softened (about 5 minutes). Remove and drain on paper towels.

Add 2 tablespoons oil to the wok. When the oil is hot, add the garlic and bean sauce. Stir-fry until aromatic. Add the ground pork. Stir-fry until it changes color, using cooking chopsticks or a wooden spoon to separate the individual pieces.

Push the pork up to the sides of the wok. Add the sauce in the middle and bring to a boil, stirring quickly to thicken. Add the eggplant slices and the chile paste. Cook for a few more minutes and stir in the sesame oil. Serve hot.

_Rhonda Parkinson, About.com_
