---
title: "Crème Brûlée"
author: "pencechp"
date: "2011-11-04"
categories:
  - dessert
tags:
  - french
---

## Ingredients

*   1/2 vanilla bean
*   2 c. heavy cream
*   3 egg yolks
*   pinch salt
*   1/4 c. plus 8-12 tsp. sugar

## Preparation

Preheat an oven to 300F. Have a saucepan of boiling water ready. Line a shallow baking pan with a small kitchen towel.

Using a paring knife, split the vanilla bean lengthwise down the middle and scrape the seeds into a 2-quart saucepan. Add the cream and the split vanilla bean, stir to mix and set the pan over medium-low heat. Warm the cream until bubbles form around the edges and steam begins to rise from the surface. Remove from the heat and set aside to steep, about 15 minutes.

In a large bowl, whisk together the egg yolks, salt, and the 1/4 c. sugar until the mixture is pale yellow and thick ribbons fall from the whisk, about 5 minutes. Gradually pour the cream into the egg mixture, stirring until well blended.  Pour the custard through a fine-mesh sieve set over a bowl.

Divide the custard among four 6-oz. ramekins and place the ramekins in the prepared baking pan. Add boiling water to fill the pan halfway up the sides of the ramekins. Cover the pan loosely with aluminum foil and bake until the custards are just set around the edges, 30 to 35 minutes. Transfer the ramekins to a wire rack and let cool to room temperature. Cover with plastic wrap and refrigerate until chilled, at least 4 hours or up to 3 days.

Just before serving, sprinkle 2 to 3 tsp. sugar over each custard. Set the crème brûlée flat on a work surface and ignite the torch. Adjust the intensity of the flame. Hold the flame close to the surface until the sugar begins to melt quickly. Move the flame gradually in small circles over the surface of the custard, heating until it is evenly melted and golden. The sugar will harden in a few seconds. Serve immediately.

_Williams-Sonoma Blowtorch Instructions_
