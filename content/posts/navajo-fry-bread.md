---
title: "Navajo Fry Bread"
author: "pencechp"
date: "2010-04-03"
categories:
  - bread
tags:
---

## Ingredients

*   4 c. white flour
*   1 tbsp. baking powder
*   1 tsp. salt
*   1 1/2 c. warm water
*   1 c. oil, preferably lard

## Preparation

Mix dry ingredients.  Add water.  Knead until soft and elastic and doesn't stick to bowl (add more water or flour if necessary).  Shape into balls about the size of a small peach.  Let sit for 15 minutes.  Flatten a little bit, pinch the dough all around the edge, and then pat back and forth between your hands until the dough is round and about 1/2 to 3/4" thick.  Make a hole in the center.  Melt lard in heavy frying pan.  Cook rounds one at a time - they will expand to almost the circumference of the pan!  Drain on paper towel and serve hot topped with refried beans, hamburger, salsa or green chili stew, and assorted taco toppings.
