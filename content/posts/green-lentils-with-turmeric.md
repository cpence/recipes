---
title: "Green Lentils with Turmeric"
date: 2020-01-27T18:36:07+01:00
categories:
    - side dish
tags:
    - ethiopian
    - lentil
---

## Ingredients

*   200 gm (1 cup) green lentils, washed
*   1 tbsp vegetable oil
*   1 onion, finely chopped
*   2 garlic cloves, chopped
*   1 tbsp ginger, finely chopped
*   1 tsp turmeric
*   750 ml (3 cups) water
*   1/2 tsp salt, to taste
*   1/2 tsp cracked black pepper, to taste

## Preparation

Add enough water to the lentils to cover, and bring to a boil. Boil for 5 minutes, then drain. Heat the oil in a saucepan over medium-high heat. Cook the onions for 5 minutes, stirring regularly. Add garlic, ginger, and turmeric, and cook for 2 minutes. Add the lentils and the water. Bring to a boil, then reduce heat to a simmer. Cook for approximately 30 minutes, until the lentils have softened, adding more water if needed. (You can adjust the consistency of the final dish by adding more water and cooking longer.) Adjust seasoning and serve.

*Browsers Grazers*

