---
title: "Quinua Amarilla [Yellow Quinoa]"
author: "pencechp"
date: "2012-07-18"
categories:
  - main course
  - side dish
tags:
  - peruvian
  - quinoa
---

## Ingredients

*   1 1/2 c. quinoa
*   2 1/2 c. water
*   1 small onion, chopped
*   2 cloves of garlic, minced
*   3/4 tbsp. aji amarillo paste
*   2 tbsp. "Delicioso adobo" (follows)
*   3 tbsp. flat leaf parsley, chopped
*   1 green pepper, chopped
*   2 tbsp. of olive oil
*   2 tbsp. butter
*   salt to taste

## Delicioso adobo

*   1 tablespoon lemon pepper seasoning
*   1 tablespoon garlic powder
*   1 tablespoon onion powder or flakes
*   1 tablespoon dried oregano
*   1 tablespoon parsley flakes
*   1 tablespoon achiote powder (ground annato)
*   1/2 tablespoon ground cumin
*   1 tablespoon salt

## Preparation

Rinse the quinoa well and dry it in a colander.  Put it in a medium saucepan, add the water and cook for 20 minutes or until the quinoa starts to pop.  Drain it.  Put the oil and butter in a large pan.  Add the onion, garlic, and bell pepper and cook until the onion is transparent, 5-7 minutes.  dd the aji amarillo paste, the "Delicioso adobo," the parsley (save a bit for garnish), and salt to taste.  Add the drained quinoa and mix completely.  Serve hot with the rest of the parsley sprinkled on top.

_Ingrid Hoffman, Delta Sky Magazine (trans. from Spanish)_
