---
title: "Tangy Mustard Cauliflower"
author: "pencechp"
date: "2016-12-04"
categories:
  - side dish
tags:
  - cauliflower
---

## Ingredients

*   1 medium head cauliflower
*   1/4 cup water
*   1/2 cup mayonnaise
*   1 tsp. finely chopped onion
*   1 tsp. prepared or Dijon mustard
*   1/4 tsp. salt
*   1/2 cup shredded cheddar cheese

## Preparation

1\. Place cauliflower and water in 1 1/2 quart glass casserole and cover with glass lid.

2\. Microwave 9 minutes on high. Drain. Combine mayonnaise, onion, mustard, and salt in small mixing bowl. Spoon mustard sauce on top of cauliflower. Sprinkle with cheese.

3\. Microwave 11/2 to 3 minutes on medium-high to heat topping and melt cheese. Let stand 2 minutes before serving.

Serves 6 to 8
