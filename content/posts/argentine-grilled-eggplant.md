---
title: "Argentine Grilled Eggplant"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - eggplant
  - vegan
  - vegetarian
---

## Ingredients

*   3 small (4 to 6 ounces each) Italian eggplants
*   2 cloves garlic, minced
*   3 tablespoons olive oil
*   1 teaspoon dried oregano
*   1 teaspoon dried basil
*   1/2 teaspoon dried thyme
*   1 teaspoon sweet or hot paprika
*   1/2 teaspoon hot red pepper flakes (optional)
*   Salt and freshly ground black pepper, to taste

## Preparation

Preheat the grill to high.

Cut the eggplants in half lengthwise; do not trim off the stem ends. (If using larger eggplants, cut crosswise into 1/2-inch-thick slices and grill 3 to 5 minutes per side.) Mix the garlic and oil in a small bowl. Brush the mixture over the cut sides of the eggplants. Combine the herbs and spices in a small bowl and set aside.

When ready to cook, arrange the eggplants, cut sides down, on the hot grate and grill until nicely browned, 3 to 4 minutes. Lightly brush the skin sides of the eggplant with the oil mixture. Turn the eggplants with tongs and brush the tops with the remaining oil. Sprinkle with the dried herb mixture and salt and black pepper to taste. Continue cooking the eggplants, cut sides up, until the flesh is soft, 6 to 8 minutes more. Serve immediately. Serves 6.

_"The Barbecue! Bible" by Steven Raichlen_
