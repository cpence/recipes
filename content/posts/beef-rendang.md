---
title: "Beef Rendang"
author: "pencechp"
date: "2013-03-13"
categories:
  - main course
tags:
  - beef
  - malaysian
  - untested
---

## Ingredients

_For the flavor base:_

*   15 dried japones chiles or 10 dried chiles de árbol or 3 Tbs. sambal oelek
*   1-1/2 cups sliced shallots (from 4 large shallots)
*   2 Tbs. sliced garlic
*   1 Tbs. sliced peeled fresh ginger
*   1 Tbs. chopped fresh or frozen and thawed galangal (optional)

_For the whole spice blend:_

*   4 whole cloves
*   4 whole green cardamom pods
*   2 whole star anise
*   1 3-inch-long cinnamon stick, snapped in half

_For the ground spice blend:_

*   2 tsp. ground coriander
*   2 tsp. ground cumin
*   2 tsp. ground fennel seeds
*   1 tsp. ground turmeric
*   1/2 tsp. freshly ground black pepper

_For the rendang:_

*   3/4 cup canola or vegetable oil; more as needed
*   2 lb. boneless top blade beef chuck (or bottom or top round, flank, or sirloin steak), cut into 1/2-inch-thick slices, then cut into 1-1/2- to 2-inch pieces
*   1 13.5-oz. can unsweetened coconut milk
*   1/4 cup tamarind concentrate
*   3 wild lime leaves, thinly sliced
*   2 medium lemongrass stalks, bruised with back of knife and tied in a knot
*   4 tsp. palm sugar or dark brown sugar
*   2 1/2 tsp. table salt
*   1/2 cup tightly packed grated fresh coconut or unsweetened frozen coconut, thawed
*   1/4 cup coarsely chopped fresh cilantro, for garnish (optional)
*   Lime wedges, for garnish (optional)

## Preparation

_Make the flavor base:_

If using dried chiles, steep them in hot water until pliable, 5 to 8 minutes; then slit and seed them (use gloves). Put the chiles, shallots, garlic, ginger, galangal (if using), and 1/4 cup water in a food processor and process to a coarse purée, about 3 minutes (if using whole dried chiles, you’ll still see little pieces of the skins).

_Make the spice blends:_

In a small bowl, combine the cloves, cardamom pods, star anise, and cinnamon pieces. In a second small bowl, combine the coriander, cumin, fennel, turmeric, and pepper.

_Make the rendang:_

Heat 2 Tbs. of the oil in a 12-inch skillet or wok over medium-low heat until shimmering hot. Add the whole spice blend and cook, stirring constantly, until the cinnamon sticks unfold (the cardamom may also crack open), 1 to 2 minutes; don’t let the spices burn. Add another 2 Tbs. of the oil and the ground spice blend and cook, stirring constantly, until the mixture sizzles and becomes fragrant, 30 to 60 seconds more (if the spices stick to the pan, add a little more oil to prevent burning).

Add the remaining 1/2 cup oil and the flavor base and cook, stirring, until the purée is an intense reddish-brown, about 10 minutes. Raise the heat to medium, add the beef and cook, stirring, to coat it with the spices, about 2 minutes. Add the coconut milk, tamarind concentrate, lime leaves, and lemongrass and cook, stirring constantly, until the mixture comes to a boil, about 5 minutes.

Reduce the heat to low, add the sugar and salt, and simmer, stirring occasionally for the first hour and then more frequently as the stew thickens, until the liquid is very thick and oil appears on its surface, about 1-3/4 hours. The meat will not be fork-tender at this point.

Meanwhile, squeeze any excess liquid from the coconut with your hands. In a 10-inch skillet, toast the coconut over low heat, stirring constantly, until golden-brown, about 10 minutes. Transfer to a small bowl.

Stir the toasted coconut into the stew and then continue stirring until it's incorporated and much of the liquid is gone, about 15 minutes. Add 1 cup water if you prefer a saucy consistency. Continue to cook, stirring frequently, until the meat is fork-tender, 20 to 30 minutes more (the oil will start frothing after 15 to 20 minutes).

Remove the lemongrass, cinnamon pieces, star anise, and as many cardamom pods and cloves as you can find. Transfer the meat to a serving platter and garnish with the cilantro and lime wedges (if using).

_Serves 6 to 8_

_Susheela Raghavan, Fine Cooking_
