---
title: "Noodle Salad with Chicken and Chile-Scallion Oil"
author: "pencechp"
date: "2018-10-01"
categories:
  - main course
  - side dish
tags:
  - chinese
---

## Ingredients

_Chile-Scallion Oil_

*   2 scallions, thinly sliced
*   2 garlic cloves, thinly sliced
*   2 star anise pods
*   2 tablespoons crushed red pepper flakes
*   1 tablespoon chopped fresh ginger
*   1 teaspoon Sichuan peppercorns
*   ½ cup vegetable oil

_Noodles and Assembly_

*   6 oz. Japanese wheat noodles (such as ramen, somen, or udon)
*   2 tablespoons reduced-sodium soy sauce
*   2 tablespoons unseasoned rice vinegar
*   2 teaspoons sugar
*   1 teaspoon toasted sesame oil
*   2 cups shredded cooked chicken
*   2 scallions, thinly sliced
*   ½ large English hothouse cucumber, halved lengthwise, thinly sliced
*   4 radishes, trimmed, thinly sliced
*   1 cup cilantro leaves or any sprout

## Preparation

_Chile-scallion oil_ Cook all ingredients in a small saucepan over medium heat, swirling pan occasionally, until scallions and garlic are just golden brown, about 3 minutes. Let cool; transfer oil to a jar.

_Noodles and assembly_ Cook noodles in a large pot of boiling water according to package directions; drain. Rinse noodles under cold water, then shake off as much water as possible. Whisk soy sauce, vinegar, sugar, and oil in a medium bowl until sugar dissolves. Add noodles, chicken, and scallions; toss to coat. Toss with cucumber, radishes, and cilantro and drizzle with chile oil just before serving.

_Bon Appetit_
