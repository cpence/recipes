---
title: "Fajita Marinade"
author: "juliaphilip"
date: "2009-12-08"
categories:
  - miscellaneous
tags:
  - beef
  - chicken
  - mexican
  - seasoning
---

## Ingredients

*   1/4 cup lime juice
*   1/3 cup water
*   2 tablespoons olive oil
*   4 cloves garlic, crushed
*   2 teaspoons soy sauce
*   1 teaspoon salt
*   1/2 teaspoon liquid smoke flavoring
*   1/2 teaspoon cayenne pepper
*   1/2 teaspoon ground black pepper

## Preparation

In a large resealable plastic bag, mix together the lime juice, water, olive oil, garlic, soy sauce, salt, and liquid smoke flavoring. Stir in cayenne and black pepper.

Place desired meat in the marinade, and refrigerate at least 2 hours, or overnight. Cook as desired.

_Robbie Rice, Allrecipes.com_
