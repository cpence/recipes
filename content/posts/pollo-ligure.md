---
title: "Pollo Ligure"
date: 2020-10-18T16:21:20+02:00
categories:
    - main course
tags:
    - chicken
    - lemon
    - italian
---

## Ingredients

* olive oil, 80 ml/ 2.7 fl oz
* onion, 1 large chopped
* fennel seeds, a good pinch
* chicken thighs, 8
* white wine, 200ml/ 6.8 fl oz (Vermentino is the local grape)
* fresh herbs, 2 tbsp roughly chopped – sage, thyme, marjoram, rosemary and a
  few whole sprigs
* pine nuts, 25g/1oz
* zest of 1 lemon
* olives, 60g/ 2.5oz  preferably Taggiasca, the local Ligurian cultivar
* sea salt and freshly milled pepper

## Preparation

1. Pour a good glug of olive oil into a good-sized pan. Sauté the onion in the
   oil until it is translucent and then add the garlic and fennel seeds being
   careful not to let them catch.
2. Next, in a separate pan, heat a little olive oil and sear the chicken all
   over until it is golden brown. Then add the chicken to the onion pan.
3. Next add the wine and cook letting the wine evaporate a little.
4. Add the herbs, zest of the lemon, olives, pine nuts and salt and pepper and
   cover and cook on a low heat for 40 minutes.
5. When the chicken is cooked add the fresh juice of the lemon and serve with a
   small jug of freshly squeezed lemon juice on the side for those who like it
   extra tangy.

*Nudo*

