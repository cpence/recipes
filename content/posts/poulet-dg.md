---
title: "Poulet DG"
date: 2022-09-03T11:41:17+02:00
categories:
  - main course
tags:
  - african
  - chicken
---

## Ingredients

- 5 cuisses de poulet
- 200 gr de haricots verts (surgelés)
- 3 carottes
- 1 poivron rouge
- 1 poivron vert
- 3 piments doux verts
- 2 oignons
- 10 gousses d’ail
- 2 boites de pulpes de tomates (400gr chacune)
- 2 feuilles de laurier
- 1 c. à café bombé de gingembre
- 1 c. à café de piment de cayenne
- 2 bouillons cubes de volaille
- Huile
- Sel et poivre

_Pour les plantains:_

- 2 bananes plantain
- Huile pour friture

## Preparation

Couper les cuisses de poulet en deux. Eplucher et couper en rondelles les carottes. Laver les poivrons et les piments, les épépiner et couper en gros morceaux les 2 poivrons. Les piments, les laisser entier ou coupé en deux.

Eplucher l’ail et l’oignon. Emincer les oignons. Ecraser sous la paume de la main 5 gousses d’ail et couper les autres en deux, les presser à l’aide d’un appareil pour en faire de la pulpe.

Dans une grande marmite mettre un peu d’huile et faire revenir les morceaux de poulet.

Quand ils sont bien dorés sur les deux faces, les retirer de la marmite et les réserver.

Mettre dans la marmite les oignons et les gousses d’ail entières. Faire revenir à feu doux.

Bien mélanger et mettre les boites de pulpes de tomates. Mélanger et laisser mijoter 5 mn.

Ajouter les légumes (haricots verts, carottes, piments et poivrons) et l’ail pressé. Saupoudrer le tout avec le gingembre, le piment de cayenne. Emietter les bouillons cube et bien mélanger. Mettre les feuilles de laurier, saler et poivrer.

Ajouter enfin le poulet et 30 cl d’eau chaude. Bien mélanger, couvrir et laisser mijoter pendant 30 mn en remuant de temps en temps.

Si la sauce est trop épaisse, rajouter un peu d’eau.

_Pour les plantains:_ (ou allez les acheter au marché)

Pendant ce temps, éplucher les bananes plantain et les couper en rondelles.

Faire chauffer un bain de friture et mettre les bananes à frire. Les retirer quand elles sont dorées.

A 5 mn de la fin de la cuisson du poulet, les glisser dans la marmite, mélanger.

Servir bien chaud accompagné de riz nature.

_Voozenoo_
