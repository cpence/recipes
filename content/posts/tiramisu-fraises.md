---
title: "Tiramisu aux fraises, aux pistaches et à l'orange"
date: 2021-08-09T10:07:39+02:00
categories:
  - dessert
tags:
  - strawberry
  - pistachio
  - orange
---

## Ingredients

- 2 œufs
- 300g de fraises
- 2 oranges
- 250 g de mascarpone
- 25 cl de crème fraîche à 35%
- 200 g de biscuits « Cuillers »
- 80 g de sucre ultrafin

Finition:

- 200 g de fraises
- 1 orange
- 50 g de pistaches décortiquées
- 2 brins de menthe

## Preparation

Lavez et équeutez les fraises, puis coupez-les en morceaux. Pressez le jus des
oranges.

Séparez les blancs des jaunes d'oeufs. Fouettez ces derniers avec le sucre,
jusqu'à ce que le mélange blanchisse et devienne mousseux. Ajoutez le
mascarpone, en mélangeant délicatement au fouet, à la main. Joignez ensuite la
crème fraîche fouettée en chantilly; mélangez bien. Montez les blancs neige bien
ferme et incorporez-les délicatement à la préparation.

Trempez la moitié des biscuits, un par un, dans le jus d'orange. Rangez-les au
fur et à mesure dans le fond d'un plat rectangulaire. Recouvrez-les avec la
moitié de la crème et disposez les fraises coupées par-dessus. Surmontez-les
d'un deuxième couche de biscuits imbibés de jus d'orange. Nappez avec le restant
de la crème, égalisez la surface et couvrez de film fraicheur. Laissez reposer
24h au frigo.

Hachez grossièrement les pistaches au couteau. Pelez l'orange à vif et
taillez-la en fines tranches. Lavez, équeutez et coupez les fraises en morceaux.
Décorez le dessus du tiramisu avec ces éléments, ajoutez quelques feuilles de
menthe et servez frais.

_Delhaize_
