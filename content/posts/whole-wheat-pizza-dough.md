---
title: "Whole Wheat Pizza Dough"
author: "juliaphilip"
date: "2010-08-07"
categories:
  - bread
tags:
  - pizza
---

## Ingredients

*   1 1/4 cups Water
*   3 tablespoons Olive oil
*   320 g whole wheat flour
*   1/3 cup Gluten flour
*   1 1/2 teaspoons Sugar
*   1 1/2 teaspoons Salt
*   4 teaspoons Active dry yeast
*   cornmeal for sprinkling bottom of pan

## Preparation

Add the ingredients to the bread machine in the order listed. Run on the dough cycle and 1.5 loaf size.

At the end of the rising cycle, turn the dough out onto a lightly floured surface. Cover and let rest for 10 minutes. Lightly oil a 12-inch (or 15-inch) pizza pan and sprinkle with the cornmeal.

With floured hands, gently stretch the dough into a 12-inch (or 15-inch) circle and place in the prepared pan. Continue stretching until the dough covers the entire surface if the pan. Push the dough against the rim of the pan to make an edge, then top the dough with your choice of pizza fillings. Bake the pizza on the bottom rack of a preheated 400?F oven for 20 to 25 minutes, until the crust is golden and the filling is melted and bubbly.

Serve hot.
