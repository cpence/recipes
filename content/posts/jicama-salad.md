---
title: "Jicama Salad"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - jicama
  - mexican
  - orange
  - salad
  - vegan
  - vegetarian
---

## Ingredients

*   2 oranges, chopped
*   1 small jicama
*   1/4 c. [bitter orange juice]( {{< relref "bitter-orange-juice" >}} )
*   2 tsp. good chile powder (pref. New Mexico)
*   2 tbsp. cilantro

## Preparation

Chop all ingredients but the cilantro and mix.  Let stand for an hour, then add cilantro just before serving.
