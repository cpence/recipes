---
title: "Tiramisu"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - italian
  - philip
---

## Ingredients

*   1 big container of mascarpone cheese
*   3 eggs
*   24 Lady Fingers
*   2 c strong coffee
*   120 g sugar

## Preparation

Beat the yolks of the eggs with the sugar

Fold in the mascarpone

Beat the whites to firm peaks

Add the whites to the yolks

In the serving dish add by alternating layers the lady fingers dunked in coffee and the egg mixture

Sprinkle coco powder on top

_Translated from French by Jenny_
