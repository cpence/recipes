---
title: "Tofu Chorizo"
author: "pencechp"
date: "2014-09-02"
categories:
  - main course
tags:
  - mexican
  - tofu
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   2 tablespoons olive oil
*   1 small onion, chopped
*   1 tablespoon garlic, chopped
*   Salt and ground black pepper
*   2 blocks firm tofu
*   1 tablespoon chili powder
*   1 teaspoon cumin
*   1/8 teaspoon cinnamon
*   1 teaspoon cider vinegar
*   Chopped fresh cilantro for garnish
*   Chopped scallions for garnish

## Preparation

Put oil in a large skillet over medium-high heat. Add onion and garlic; sprinkle with salt and pepper. Cook, stirring occasionally, until the vegetables soften, 3 to 5 minutes.

With your hands, crumble tofu into the pan. Cook, stirring and scraping the bottom of the skillet occasionally and adjusting heat as necessary, until tofu browns and crisps as much or as little as you like, anywhere from 10 to 30 minutes.

Sprinkle with the chili powder, cumin and cinnamon; stir and cook, continuing to scrape any browned bits from the bottom of the pan until the mixture is fragrant, a minute or two. Stir in vinegar and adjust the seasoning taste. Garnish with cilantro and scallions and serve with warm corn tortillas or over rice.

_Mark Bittman, New York Times_
