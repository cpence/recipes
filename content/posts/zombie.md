---
title: "Zombie"
author: "pencechp"
date: "2013-12-10"
categories:
  - cocktails
tags:
  - gin
  - lillet
---

The Corpse Reviver #2, tweaked by yours truly.

## Ingredients

*   2 oz. gin (pref. citrusy, like Plymouth or Bluecoat)
*   1 oz. Lillet blanc
*   3/4 oz. lemon juice
*   dash bitters

## Preparation

Shake with ice, strain into a martini glass, serve with a lemon wheel.

_Charles Pence_
