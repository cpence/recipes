---
title: "Okra with Tomatoes"
author: "pencechp"
date: "2010-10-20"
categories:
  - side dish
tags:
  - okra
  - pence
  - tomato
---

## Ingredients

*   1 box okra
*   1 onion, sliced
*   1 lg. can tomatoes _or_ 1-2 lg. fresh tomatoes
*   dried basil or oregano (optional)

## Preparation

Cut the stem tips off the okra (but don't cut off the top of the pod)

Saute sliced onion in olive oil or butter until translucent.

Add chopped fresh or canned tomatoes.

Add okra, salt and pepper.

Add enough water to keep it from sticking.

Simmer until it looks like it's supposed to. (Taste before done and if the tomatoes are too tart, add a pinch of sugar.)

If you want, you can and an herb of your choice. Oregano is good. Basil, too.
