---
title: "Chicken waterzooi"
author: "pencechp"
date: "2018-12-26"
categories:
  - main course
tags:
  - belgian
  - chicken
  - soup
---

## Ingredients

*   1 nice chicken
*   20g butter
*   1 egg
*   1 slice of bread per person
*   Lemon
*   20cl cream
*   5 parsley roots/parsnips
*   5 potatoes
*   2 onions, cut in half
*   5 carrots
*   6 celery stalks
*   1 clove
*   Thyme
*   Bay leaf
*   Pepper and salt
*   Parsley

## Preparation

The day before, prepare a nice chicken stock using the chicken. Discard the
skin, but reserve the meat and the bones.

Place the bones in a large soup pot and cover them with half water and half
chicken stock. Add the onions and the rest of the vegetables, either chopped
medium-size or in large julienne. Add the clove, thyme, and bay leaf, and
lightly season with salt and pepper to taste. Simmer on low heat for three
hours.

Remove the bones and spices, and reserve the vegetables. Add the cream and
butter to the broth, and adjust the seasoning. Add all the vegetables and the
chicken meat.

Just before serving, temper the egg yolk in broth and add it to the soup, then
heat the soup slowly, making sure the egg yolk doesn't cook.

Serve with chopped parsley and a squeeze of fresh lemon juice, and a slice of
bread and butter on the side.

_visit.brussels_
