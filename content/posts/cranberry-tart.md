---
title: "Cranberry Tart"
date: 2022-11-28T10:18:43+01:00
categories:
  - dessert
tags:
  - cranberry
  - untested
---

## Ingredients

_For the cranberry curd:_

- 1 pound (455 grams/4 cups) fresh or frozen cranberries
- 1 1/4 cups (250 grams) granulated sugar
- Finely grated zest and juice of 1 orange (about 1 tablespoon zest and 1/2 cup juice)
- One (2-inch) piece fresh ginger, peeled and thinly sliced
- Pinch fine sea salt or table salt
- 3 large egg yolks
- 2 teaspoons cornstarch
- 4 tablespoons (55 grams) unsalted butter, cut into 4 pieces and at room temperature

_For the gingersnap cookie crust:_

- 8 tablespoons (113 grams/1 stick) unsalted butter, at room temperature
- 1/4 cup packed (55 grams) dark brown sugar
- 1/4 cup (50 grams) granulated sugar
- 1 large egg yolk
- 2 tablespoons molasses
- 1 3/4 cups (220 grams) all-purpose flour
- 2 teaspoons ground ginger
- 1 teaspoon ground cinnamon
- 1/2 teaspoon ground allspice
- 1/4 teaspoon fine sea salt or table salt

## Preparation

1. Make the cranberry curd: In a medium saucepan over medium-high heat, combine the cranberries, sugar, orange zest and juice, ginger and salt and bring to a boil, stirring occasionally. Adjust the heat to maintain a very gentle simmer, cover and cook until all the cranberries have burst and started to shrivel, about 10 minutes.

2. While the cranberries cook, in a small bowl, whisk together the egg yolks and cornstarch until smooth. Transfer the hot cranberry mixture to a heatproof blender. Immediately add the yolk mixture, cover loosely, gradually increase the speed to high and blend until smooth, about 1 minute. Let the mixture cool, uncovered, in the blender until a skin forms and it registers 120 to 125 degrees on an instant-read thermometer, about 1 hour 30 minutes.

3. Make the gingersnap cookie crust: In the bowl of a stand mixer fitted with the paddle attachment, beat the butter and brown and granulated sugars on medium-low speed until smooth, 2 to 3 minutes.

4. Increase the mixer speed to medium, add the egg yolk and molasses and mix to combine. Stop the mixer and scrape down the sides and bottom of the bowl thoroughly. Add the flour, ginger, cinnamon, allspice and salt and mix on low speed until fully incorporated and the mixture looks like crumbly cookie dough, about 1 minute.

5. Turn out the dough into a 9-inch tart pan with a removable bottom. Use your fingers to press it evenly across the bottom and up the sides. Loosely cover with a kitchen towel or plastic wrap and refrigerate for at least 30 minutes and up to overnight.

6. When the crust is ready to bake, position a rack in the middle of the oven and preheat to 350 degrees. Dock the crust across the bottom with a fork and blind-bake (without pie weights), until evenly golden brown, 20 to 22 minutes. Transfer to a wire rack and let cool completely before adding the filling.

7. When the cranberry puree has cooled, add the softened butter and blend until fully combined, about 30 seconds. Pour the puree into the cooled crust and smooth with an offset spatula into an even layer. Let the tart sit at room temperature for at least 2 hours.

_WaPo_
