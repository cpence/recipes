---
title: "Sweet Potato Croquettes with Black-Eyed Pea Curry"
author: "pencechp"
date: "2017-03-30"
categories:
  - main course
tags:
  - curry
  - greens
  - indian
  - peas
  - sweetpotato
---

## Ingredients

_Croquettes_

*   3 medium sweet potatoes, boiled, peeled and quartered
*   1 tablespoon oil, plus extra for panfrying 
*  1 teaspoon cumin seeds
*  1 large onion, finely chopped
*  1/3 cup grated raw cauliflower
*  1 1/2 teaspoons ginger-garlic paste
*  1/4 teaspoon turmeric
*  1 1/2 teaspoons coriander
*  1/2 teaspoon ground cumin
*  1/2 to 1 teaspoon red chili powder
*  1/2 to 1 teaspoon garam masala
*  1/2 teaspoon amchur (dry mango powder)
*  3/4 cup breadcrumbs
*  2 tablespoons fresh coriander (cilantro), finely chopped
*  1/2 cup semolina flour or breadcrumbs
*  salt to taste

_Black-Eyed Pea Curry_

*   1/2 cup black-eye peas
*   2 tablespoons chopped onion
*   3 tablespoons chopped tomatoes
*   1 tablespoons fresh ginger garlic paste
*   2 tablespoons grated coconut
*   1 cup chopped collard greens
*   1 teaspoon chili powder
*   2 teaspoons ground coriander
*   3/4 teaspoon ground cumin
*   1 teaspoons garam masala
*   1 teaspoon kastoori methi
*   1/4 teaspoon turmeric powder
*   2 tablespoons oil
*   1 teaspoon cumin seeds

## Preparation

_Curry: _Puree tomato, onion, garlic, ginger and coconut to a smooth paste and keep it ready. Heat oil, add cumin seeds, when it splutters, add the ground paste, all the spice powder, salt needed and cook until oil separates. Stir frequently in between. Add the cooked black eyed beans, 1 cup of water and collard greens and cook on low heat for about an hour.

_Croquettes: _Mash sweet potato and set aside to cool. Heat ½ tbsp oil in a pan and add cumin seeds to it. When cumin seeds start to sizzles and color deepens, add finely chopped onions and sauté 5 minutes.  Add cauliflower and sauté 3 minutes. Add ginger-garlic paste and saute 1½ mins. Add turmeric, coriander, cumin, and chili powder and sauté them for 30 seconds. Add garam masala and dry mango powder and set aside to cool.

Combine onion mixture with the mashed sweet potatoes. To this add the bread crumbs, chopped cilantro and salt to taste and mix them well.

Form flattened croquettes.  Dust each patty generously in semolina.Place 2-3 patties on a hot griddle or skillet with extra oil. Let them cook until the side turns golden brown and crisp. Gently flip and add more oil and cook until crisp and browned, 4-5 more minutes.

_Maneet Chauhan, Edible Nashville_
