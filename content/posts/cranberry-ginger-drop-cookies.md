---
title: "Cranberry Ginger Drop Cookies"
author: "juliaphilip"
date: "2010-12-04"
categories:
  - dessert
tags:
  - cookie
  - cranberry
---

## Ingredients

*   3/4 cup sugar
*   1/2 cup brown sugar
*   1/2 cup butter or margarine, softened
*   1/3 cup milk
*   1 large egg
*   2 cups all-purpose flour
*   1/2 teaspoon baking powder
*   1/2 teaspoon ground ginger
*   1/4 teaspoon baking soda
*   1 1/2 cups Fresh or Frozen Cranberries, coarsely chopped
*   1/2 cup chopped pecans
*   1/4 cup finely chopped crystallized ginger

## Preparation

Heat oven to 375°F. Lightly grease cookie sheets.

Combine sugar, brown sugar and butter in large mixing bowl; beat on medium speed until well mixed. Add milk and egg; beat until smooth. Add flour, baking powder, ground ginger and baking soda; beat on low speed until well mixed. Gently stir in cranberries, pecans and crystallized ginger.

Drop dough by rounded teaspoonfuls 2 inches apart onto greased cookie sheet. Bake for 9 to 12 minutes or until edges are light golden brown. Immediately remove from cookie sheet; cool on wire rack.

Makes 3 1/2 dozen cookies.

_Ocean Spray_
