---
title: "Instant Pot Butter Chicken"
date: 2020-06-06T13:39:56+02:00
categories:
    - main course
tags:
    - indian
    - chicken
    - instantpot
---

## Ingredients

*   14 oz. can tomatoes
*   5-6 cloves garlic
*   1-2 tsp. minced ginger
*   1 tsp. turmeric
*   1/2 tsp. cayenne pepper
*   1 tsp. smoked paprika
*   1 tsp. salt
*   1 tsp. garam masala
*   1 tsp. ground cumin
*   1 lb. chicken (pref. boneless thighs)
*   4 oz. butter, cubed
*   4 oz. heavy cream
*   1 tsp. garam masala
*   1/4–1/2 c. cilantro

## Preparation

Place the first ten ingredients into the instant pot. Cook for 10 minutes on
high. Allow the pressure to drop for 10 minutes naturally, then quick release.

Take the chicken out and set aside. Blend the sauce with an immersion blender,
and add the butter, cream, cilantro, and further 1 tsp. garam masala.

Set aside half of the sauce for future use (mix with paneer, or peas, or shrimp,
or leftover cooked chicken).

Chop the chicken, return it to the sauce, and heat gently. Serve with rice or
naan.

*Two Sleevers*

