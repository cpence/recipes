---
title: "Olive Oil Poached Shrimp with Soba Noodles"
author: "pencechp"
date: "2013-12-04"
categories:
  - main course
tags:
  - japanese
  - shrimp
  - untested
---

## Ingredients

*   12 jumbo (11-15 count) shrimp
*   2 garlic cloves, minced
*   2 tablespoons minced peeled fresh ginger
*   1 teaspoon sriracha
*   1 pound asparagus, tough ends trimmed
*   1/3 cup, plus 5 teaspoons olive oil
*   salt
*   freshly ground black pepper
*   8 ounces fresh shiitake mushrooms
*   8 ounces soba noodles
*   1/4 cup soy sauce
*   1 tablespoon honey
*   1 1/2 teaspoons Dijon mustard
*   2 green onions, whites and greens thinly sliced
*   2 tablespoons sesame seeds, toasted

## Preparation

Rub the shrimp with half of the garlic and ginger, and the sriracha. Cover and let the shrimp marinate in the refrigerator for 1 hour.

Preheat the oven to 400°F.

Spread the asparagus out on a baking sheet and brush it with 2 1/2 teaspoons of the oil. Roast just until the asparagus is tender, about 15 minutes. Remove, season with salt and pepper, and set aside to cool.

Reduce the oven temperature to 300°F. Toss the shiitakes with another 2 1/2 teaspoons of oil, spread them out on a baking sheet, and transfer to the oven. Roast just until the mushrooms begin to shrivel, about 30 minutes. Season with salt and pepper and set aside to cool.

Put the shrimp in a small baking dish or ovenproof sauté pan, cover with the remaining 1/3 cup oil and season lightly with salt. Cover the dish with foil and poach the shrimp in the oven just until the exteriors are bright orange and if you slice into one, the interior is still opaque, about 15 minutes (don’t worry that it doesn’t look completely done as it will carry-over cook a bit). Remove the shrimp from the oil and reserve the oil, allowing it to cool.

Meanwhile, bring a large pot of water to a boil. Cook the noodles according to the package directions. Rinse with cold water and set aside to cool.

Cut the asparagus into 1-inch pieces and the shiitakes into thin strips. Toss the vegetables with the cooled noodles.

Whisk together the remaining ginger and garlic with the soy sauce, honey, and mustard. Slowly whisk in the reserved poaching oil. Pour the dressing over the noodles and toss well to combine. Top with the shrimp, green onions, and sesame seeds before serving.

_Girl in the Kitchen, via HuffPo_
