---
title: "Fried Chickpeas with Chorizo and Spinach"
author: "pencechp"
date: "2013-06-13"
categories:
  - main course
tags:
  - sausage
  - spanish
  - tapas
---

## Ingredients

*   1/4 cup olive oil, plus more for drizzling
*   2 cups cooked or canned chickpeas, as dry as possible
*   Salt and black pepper
*   4 ounces chorizo, diced
*   1/2 pound spinach, roughly chopped
*   1/4 cup sherry
*   1 to 2 cups bread crumbs

## Preparation

Heat the broiler.

Put three tablespoons of the oil in a skillet large enough to hold chickpeas in one layer over medium-high heat. When it’s hot, add chickpeas and sprinkle with salt and pepper.

Reduce heat to medium-low and cook, shaking the pan occasionally, until chickpeas begin to brown, about 10 minutes, then add chorizo. Continue cooking for another 5 to 8 minutes or until chickpeas are crisp; use a slotted spoon to remove chickpeas and chorizo from pan and set aside.

Add the remainder of the 1/4 cup of oil to the pan; when it’s hot, add spinach and sherry, sprinkle with salt and pepper, and cook spinach over medium-low heat until very soft and the liquid has evaporated. Add chickpeas and chorizo back to the pan and toss quickly to combine; top with bread crumbs, drizzle with a bit more oil and run pan under the broiler to lightly brown the top. Serve hot or at room temperature.

_Serves 4_

_The New York Times_
