---
title: "Cacio e pepe"
author: "pencechp"
date: "2016-12-29"
categories:
  - main course
  - side dish
tags:
  - italian
  - pasta
---

## Ingredients

*   Salt
*   1 ½ cups finely grated pecorino Romano, plus more for dusting completed dish
*   1 cup finely grated Parmigiano-Reggiano
*   1 tablespoon ground black pepper, plus more for finishing the dish
*   ¾ pound tonnarelli or other long pasta like linguine or spaghetti
*   Good olive oil

## Preparation

Put a pot of salted water on to boil. In a large bowl, combine the cheeses and black pepper; mash with just enough cold water to make a thick paste. Spread the paste evenly in the bowl.

Once the water is boiling, add the pasta. The second before it is perfectly cooked (taste it frequently once it begins to soften), use tongs to quickly transfer it to the bowl, reserving a cup or so of the cooking water.

Stir vigorously to coat the pasta, adding a teaspoon or two of olive oil and a bit of the pasta cooking water to thin the sauce if necessary. The sauce should cling to the pasta and be creamy but not watery.

Plate and dust each dish with additional pecorino and pepper. Serve immediately.

_Mark Bittman, NYT_
