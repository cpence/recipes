---
title: "Baked Acorn Squash with Chestnuts, Apples and Leeks"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
tags:
  - squash
  - untested
---

## Ingredients

*   4 acorn squash (about 1 pound each), halved lengthwise and seeded
*   3 tablespoons extra-virgin olive oil, plus more for brushing
*   Kosher salt and freshly ground pepper
*   3 tablespoons unsalted butter
*   1 1/2 cups diced celery
*   2 leeks, halved lengthwise and sliced crosswise 1/4 inch thick
*   2 Granny Smith apples, peeled and diced
*   2 teaspoons finely chopped thyme
*   10 ounces day-old rustic rye bread—crusts removed, bread cut into 1/2-inch dice (about 6 cups)
*   7 ounces vacuum-packed cooked chestnuts
*   1/2 cup chopped parsley
*   1/3 cup heavy cream
*   1/3 cup vegetable stock or low-sodium broth

## Preparation

Preheat the oven to 350°. Brush the cut sides of the squash with olive oil and season the cavities with salt and pepper. Place the squash cut side down on two baking sheets and roast for about 25 minutes, until just tender.

Meanwhile, in a large skillet, melt the butter in the 3 tablespoons of olive oil. Add the celery, leeks and a generous pinch each of salt and pepper and cook over moderate heat, stirring occasionally, until softened, about 8 minutes. Add the apples and thyme and cook over moderately high heat until the apples just start to soften, about 5 minutes. Scrape the mixture into a large bowl. Add the bread, chestnuts, parsley, cream and stock and toss well. Season with salt and pepper.

Turn the squash cut side up. Spoon the stuffing into the cavities and bake until the squash are tender and the stuffing is golden brown, about 20 minutes. Transfer to plates and serve.

_Food & Wine_
