---
title: "Penne with Zucchini and Sweet Onion"
author: "juliaphilip"
date: "2010-04-05"
categories:
  - main course
tags:
  - italian
  - pasta
  - zucchini
---

## Ingredients

*   Salt (for the cooking water, plus teaspoon or more to taste)
*   8 ounces dried penne pasta
*   1 tablespoon olive oil
*   4 ounces (12 thin slices) pancetta, cut into ¼-inch dice
*   1 medium onion, preferably sweet, such as Vidalia or Mayan, cut into ¼- to
    ½-inch dice (1 cup)
*   1 pound zucchini, preferably 4 small ones, cut into ½-inch dice
*   Leaves from 6 stems basil leaves (3 tablespoons), chopped
*   Freshly ground black pepper
*   Freshly grated Parmesan cheese, for serving

You can use this recipe as a template, replacing the vegetable and the fresh
herbs.

## Preparation

Bring a large pot of salted water to a boil over medium-high heat. Add the penne
and cook according to the package directions. Drain and hold in a colander.

Meanwhile, heat the oil in a large (at least 12-inch) skillet or shallow
braising pan over medium heat. Add the pancetta and cook for 2 to 3 minutes,
stirring, until the pieces start to turn translucent but not brown. Add the
onion and cook for 3 to 4 minutes, stirring occasionally, until the pieces
soften.

Add the zucchini and the teaspoon salt (or to taste) and stir to incorporate;
cook for 10 to 15 minutes, stirring occasionally, until the zucchini is
tender. Add 2 tablespoons of the basil and the pepper to taste.

Combine the cooked penne with the vegetable mixture in a large serving bowl,
preferably warmed. Garnish with the remaining tablespoon of basil. Serve warm,
with Parmesan for passing at the table.

_Stephanie Witt Sedgwick_
