---
title: "Creamy Pasta with Asparagus, Parmesan, and Crispy Prosciutto"
author: "juliaphilip"
date: "2009-12-08"
categories:
  - main course
tags:
  - asparagus
  - pasta
  - prosciutto
---

## Ingredients

*   1 tablespoon olive oil
*   4 ounces thin sliced prosciutto, cut into 1/4 in slices
*   1 garlic clove, sliced thin
*   1 cup heavy cream
*   2 tablespoons lemon juice
*   salt and pepper
*   1 lb pasta or penne or fusilli, any with texture
*   1 lb asparagus, trimmed and cut into 1 in pieces
*   1 1/2 cups grated parmesan cheese
*   3/4 cup chopped fresh basil

## Preparation

Bring 4 qts water to boil in largepot.(in the meantime) Heat oil in large nonstick skillet over med heat until just smoking. Cook prosciutto until lightly browned and crisp, (5 min) Transfer to paper towel lined plate to reduce fat. Add garlic to pan and cook 30 sec. Stir in cream and lemon juice and simmer until thickened, 3-5 minute.

Add 1 tbsp salt and pasta to boiling water until just eginning to soften, 8 minute Add asparagus to pot and cook until tender and pasta become al dente (4 min).

Reserve 1 c cooking water and drain pasta and asparagus, return to pot.

Add sauce, 1/2 c reserved water, cheese, and basil to pot, toss to combine, add remaining water as needed.

Season with salt and pepper to taste.

Sprinkle with crunchy prosciutto.

Serve. Delicious with crusty warm bread.
