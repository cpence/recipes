---
title: "Grandma Philip's Corn Casserole"
author: "juliaphilip"
date: "2017-01-03"
categories:
  - side dish
tags:
  - corn
  - philip
---

## Ingredients

*   1 box Jiffy corn bread mix
*   1 can cream corn
*   1 can corn (drain liquid and save)
*   1 stick butter (or less)
*   1 egg
*   ½ cup liquid – from the corn plus milk to make the ½ cup

## Preparation

Mix all ingredients, pour into loaf pan and bake at 350 for 25 minutes or until set in the middle.
