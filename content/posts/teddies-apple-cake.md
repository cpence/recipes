---
title: "Teddie's Apple Cake"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - apple
  - cake
  - untested
---

## Ingredients

*   Butter for greasing pan
*   3 cups flour, plus more for dusting pan
*   1 1/2 cups vegetable oil
*   2 cups sugar
*   3 eggs
*   1 teaspoon salt
*   1 teaspoon cinnamon
*   1 teaspoon baking soda
*   1 teaspoon vanilla
*   3 cups peeled, cored and thickly sliced tart apples, like Honeycrisp or Granny Smith
*   1 cup chopped walnuts
*   1 cup raisins
*   Vanilla ice cream (optional)

## Preparation

1\. Preheat oven to 350 degrees. Butter and flour a 9-inch tube pan. Beat the oil and sugar together in a mixer (fitted with a paddle attachment) while assembling the remaining ingredients. After about 5 minutes, add the eggs and beat until the mixture is creamy.

2\. Sift together 3 cups of flour, the salt, cinnamon and baking soda. Stir into the batter. Add the vanilla, apples, walnuts and raisins and stir until combined.

3\. Transfer the mixture to the prepared pan. Bake for 1 hour and 15 minutes, or until a toothpick inserted in the center comes out clean. Cool in the pan before turning out. Serve at room temperature with vanilla ice cream, if desired. Serves 8.

_The New York Times_
