---
title: "Asparagus with Asian Citrus Dressing"
author: "juliaphilip"
date: "2010-04-05"
categories:
  - side dish
tags:
  - asparagus
  - untested
---

## Ingredients

*   1 lb asparagus, trimmed
*   3 tbl orange juice
*   2 tsp lemon juice
*   2 tsp rice vinegar
*   1 tsp honey
*   1/2 tsp orange rind
*   1/2 tsp fish sauce

## Preparation

Place about 1 in of water in a large 2-in or more deep skillet and bring to a boil. Add the asparagus and cook until bright green and crisp-tender, 4 to 7 min, depending on the thickness of the asparagus.

Meanwhile, whisk together the orange juice, lemon juice, rice vinegar, honey, orange rind, and fish sauce until the honey is completely dissolved. Pour over the warm asparagus and serve hot or cold.

_Marge Perry_
