---
title: "Whole Wheat Biscuits"
author: "juliaphilip"
date: "2012-04-22"
categories:
  - bread
  - breakfast
tags:
---

## Ingredients

*   3/4 c buttermilk or milk + 2 T vinegar
*   2 T canola oil
*   1 T honey or brown sugar
*   1 c whole wheat flour
*   1 c whole wheat pastry flour
*   1/2 tsp salt
*   4 tsp baking powder
*   1/4 t baking soda

## Preparation

Preheat oven to 400.

Mix milk, honey, and oil. Add dry ingredients. Drop biscuits onto greased cookie sheet or roll biscuits and place on greased cookie sheet. Bake at 400 for 10-12 minutes.

_KASEYK, SparkPeople_
