---
title: "Onions Toluca"
author: "pencechp"
date: "2010-05-16"
categories:
  - main course
tags:
  - chorizo
  - mexican
  - pence
---

## Ingredients

*   4 large sweet onions
*   4 oz. Mexican chorizo
*   4 oz. ground beef
*   1/2 tsp. black pepper
*   4 thick slices Mexican goat cheese or Muenster (4"x4")
*   chopped cilantro
*   4 flour tortillas

*for serving:* additional flour tortillas, picante sauce or pico de gallo, guacamole

## Preparation

Peel the onions. Cut an area off the top on which to set the onion. Hollow out the onions from the root end until about half the center has been removed. \[I use the parings for my salsa and/or guac.\] Place the onions in a steamer for about 20 minutes or until translucent but not too soft. \[I use my microwave steamer, which only accommodates one onion at a time, for about 51/2 minutes per onion.\] Remove and let cool on paper towel.

Saute meats and salt and pepper over medium heat for about 12 minutes, stirring constantly until well blended. Drain grease and fill onion cavities with stuffing. Place onions in a casserole and top with a slice of cheese. Cook uncovered in a 350 degree over for about 10 minutes, then broil until cheese is melted and lightly browned.

Sprinkle with cilantro and serve immediately on the center of a warm flour tortilla with additional tortillas, picante and/or guacamole.
