---
title: "Rhubarb-Strawberry Sorbet"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - icecream
  - rhubarb
  - strawberry
  - untested
---

## Ingredients

*   3/4 pound rhubarb (5 or 6 thin stalks), trimmed
*   3/4 cup sugar
*   10 ounces fresh strawberries (about 1 1/2 cups)
*   1/2 teaspoon freshly squeezed lemon juice

## Preparation

1\. Cut the rhubarb into half-inch pieces. In a medium, nonreactive saucepan, bring the rhubarb, two-thirds cup water and the sugar to a boil. Reduce the heat, cover and simmer until the rhubarb is tender and cooked through, about 5 minutes. Cool to room temperature.

2\. Slice the strawberries and purée them in a blender or food processor with the cooked rhubarb mixture and lemon juice until smooth.

3\. Chill the mixture thoroughly, then freeze in an ice cream maker according to the manufacturer's instructions.

_Chicago Tribune_
