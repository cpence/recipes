---
title: "Pommes Anna"
author: "pencechp"
date: "2011-11-25"
categories:
  - side dish
tags:
  - french
  - potato
---

## Ingredients

*   3/4 c. clarified butter
*   3 lb. potatoes
*   salt
*   pepper

## Preparation

Layer thinly sliced potatoes in a circle pattern, brushing with butter and seasoning with salt and pepper as you go. Drizzle the extra butter over the top when you're done.  Cover w/ foil and bake at 425 for 30 minutes.  Remove the foil and press with a spatula.  Bake uncovered for 30 more minutes.  Drain off the extra butter, invert out of the pan, and serve.
