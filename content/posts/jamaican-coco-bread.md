---
title: "Jamaican Coco Bread"
date: 2020-09-28T12:17:06+02:00
categories:
    - bread
tags:
    - jamaican
    - untested
---

## Ingredients

*   6 tablespoons unrefined coconut oil, plus more melted oil for greasing and
    brushing
*   1 cup full-fat well-shaken and stirred coconut milk (from a 13.5-ounce can)
*   1/4 cup granulated sugar
*   1 teaspoon kosher salt
*   1 (1/4-ounce) envelope active dry yeast (2 1/4 teaspoons)
*   1 large egg, lightly beaten
*   3 1/2 cups (about 14 7/8 ounces) all-purpose flour, plus more for dusting
    and rolling

## Preparation

Lightly grease a large bowl with melted coconut oil, and line a large rimmed
baking sheet with parchment paper. Add coconut oil, coconut milk, sugar, and
salt to a medium-size microwaveable bowl; microwave on HIGH until sugar is
dissolved and oil is melted, about 1 minute. (Or heat mixture in a small
saucepan over low heat for 1 minute.) Stir to combine. Stir in yeast and egg.
Add flour to coconut milk mixture, and stir together to form a soft dough.

Transfer dough to a lightly floured surface, and gently knead until dough is
smooth and well combined, about 2 minutes. Resist the urge to add more flour—the
softer the dough, the lighter and more tender the coco bread. Place dough in
greased bowl; cover lightly with a clean kitchen towel. Let dough rise in a warm
place (75F), until doubled in size, about 1 hour.

Preheat oven to 350°F, placing rack in middle position. Punch down dough, and
transfer to a lightly floured surface. Using a bread knife or bench scraper, cut
dough in half; cut each half into 4 equal portions. Shape each portion into a
ball, and roll each into a 6- to 7-inch oval about the size of your hand. Oval
should be about 1/4-inch thick. Brush surface of each oval with melted coconut
oil, and fold in half crosswise to form a semicircle. Brush tops with more
melted coconut oil. Place folded dough 1 to 2 inches apart on prepared baking
sheet; let rest at room temperature 15 minutes. Bake in preheated oven until
light brown, about 17 minutes. Let cool 5 minutes before serving. Serve warm.

*Food & Wine*

