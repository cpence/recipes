---
title: "Samfaina"
author: "pencechp"
date: "2010-12-07"
categories:
  - main course
  - side dish
tags:
  - eggplant
  - spanish
---

## Ingredients

*   1 medium eggplant, peeled and diced very small
*   Salt
*   1/4 cup extra virgin olive oil
*   2 medium onions, very finely chopped
*   4 large garlic cloves, minced
*   2 red bell peppers (or one red and one green), peeled, seeded and sliced in thin strips or diced small
*   1 medium zucchini, peeled and finely chopped
*   Black pepper
*   1 pound ripe tomatoes, peeled, seeded and chopped, or a 14-ounce can, drained

## Preparation

1\. Lay eggplant pieces on two layers of paper towels. Sprinkle with salt. After 30 minutes, squeeze out liquid and pat dry.

2\. Heat 2 tablespoons oil over medium heat in an earthenware casserole over a flame tamer or in a Dutch oven. Add onions and cook, stirring often, until they soften, about 8 minutes. Add a generous pinch of salt and the garlic, and stir for about 30 seconds. Add the remaining olive oil along with the eggplant, bell peppers, zucchini and black pepper. Turn the heat to low, stir, cover and cook until the vegetables are soft, about 1 hour, stirring occasionally. Stir in the tomatoes, season with salt, cover again and cook until the mixture has reduced to a thick relish, 2 to 3 more hours, stirring occasionally. Taste and adjust seasonings. Before serving, allow to sit for at least 1 hour, or better yet refrigerate overnight.

Yield: 6 servings.

_New York Times_
