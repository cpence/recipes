---
title: "Crawfish Bisque with Boulettes"
author: "pencechp"
date: "2014-10-03"
categories:
  - main course
tags:
  - cajun
  - crawfish
  - soup
---

## Ingredients

_Boulettes_

*   1 c. chopped onion
*   1 c. chopped celery
*   1 c. chopped green onion
*   1 clove garlic, minced
*   2 tbsp. butter
*   1 lb. peeled crawfish tails
*   2 c. plain breadcrumbs
*   1 tsp. baking powder
*   1/2 tsp. Cajun seasoning
*   1 tsp. hot sauce
*   1 egg, slightly beaten
*   2 tbsp. melted butter

_Bisque_

*   1 c. flour
*   1 c. vegetable oil
*   1 c. chopped onion
*   1 c. chopped celery
*   1/2 c. chopped bell pepper
*   2 cloves garlic, minced
*   8 c. chicken broth
*   2 tbsp. Kitchen Bouquet
*   2 tbsp. tomato paste
*   1 lb. peeled crawfish tails
*   1 tsp. salt
*   1 tsp. pepper
*   1 tsp. Cajun seasoning
*   1 tbsp. hot sauce
*   rice

## Preparation

Sautee the onion and celery in the butter for 3-4 minutes until they become soft. Add garlic and green onions and sautee for another minute. Fold in crawfish tails and sautee for 2-3 minutes more. Cool. Pour into a food processor and pulse until chopped. Place in a bowl and add eggs, breadcrumbs, seasoning, and hot sauce. Mix well. Preheat the oven to 350 and line a baking sheet with parchment paper. Roll the stuffing to ping-pong ball size, then mold into the shape of a small football. This should make 18-20 boulettes. Place on the baking sheet. Brush the tops with melted butter and bake for 15-20 minutes until golden. Set aside.

In a stockpot, heat oil and add flour. Cook until the roux is the color of peanut butter. Add chopped onion, celery, and bell pepper. Reduce heat to medium and cook for 4-5 minutes. Add garlic, cook for 15-20 seconds, then slowly add the chicken broth and stir until smooth. Whisk in tomato paste, Kitchen Bouquet, salt, seasoning, pepper, and hot sauce. Bring to a boil. Reduce the heat to low and simmer for 10 minutes. Add crawfish tails and boulettes. Simmer for another 5-10 minutes. Serve over rice.

_225 Magazine_
