---
title: "Peruvian Shrimp and Corn Chowder"
date: 2020-06-06T14:01:53+02:00
categories:
    - main course
tags:
    - peruvian
    - stew
    - soup
    - shrimp
    - corn
    - untested
---

## Ingredients

*   3 tablespoons cooking oil
*   1 pound unpeeled medium shrimp
*   3 teaspoons salt
*   1 onion, chopped
*   2 cloves garlic, chopped
*   1/4 teaspoon cayenne
*   1/2 teaspoon paprika (or 1 tsp. aji amarillo paste, if available)
*   1/2 teaspoon ground cumin
*   1/2 teaspoon oregano
*   Dash Tabasco sauce
*   1 small butternut squash (about 1 1/2 pounds), peeled, halved lengthwise,
seeded, and cut into 1-inch cubes
*   1/2 small head green cabbage (about 1 1/4 pounds), chopped (about 1 quart)
*   1 pound baking potatoes (about 2), peeled and cut into 1 1/2-inch chunks
*   4 ears of corn, halved
*   2 quarts water (or seafood stock)
*   1 cup heavy cream
*   1 cup frozen peas (optional)

## Preparation

In a large pot, heat the oil over moderate heat. Add the shrimp and 1 teaspoon
of the salt and cook, stirring frequently, until the shrimp are pink and firm,
about 5 minutes. Remove with a slotted spoon. When the shrimp are cool enough to
handle, peel them and set aside.

Add the onion, garlic, cayenne, paprika (or aji amarillo), cumin, oregano, and
another teaspoon of the salt to the pot. Cook, stirring occasionally, until the
onion is translucent, about 5 minutes. Add the Tabasco, squash, cabbage,
potatoes, corn, and water to the pot. Cover and bring to a boil. Reduce the heat
and simmer, partially covered, until the potatoes are tender, about 15 minutes.

Add the cream and simmer for 10 minutes. Stir in the peeled shrimp, the
remaining teaspoon of salt, and the peas, if using. Cook until the shrimp are
just heated through, about 2 minutes.

*Food & Wine, tweaked*

