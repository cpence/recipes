---
title: "Prune Danish"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - breakfast
tags:
  - danish
  - pastry
---

## Ingredients

*   1/2 recipe [Danish pastry dough]( {{< relref "danish-pastry-dough" >}} )
*   1 can (12 ounces) prune filling, or 1 jar (8 ounces) Lekvar (thick jam)
*   1 egg, slightly beaten
*   1/2 c. light corn syrup

## Preparation

1\. Roll Danish pastry dough out on a lightly floured pastry cloth or board to a 20x15-inch rectangle; trim edges even; cut in twelve 5-inch squares with a sharp knife.

2\. Place a rounded tablespoonful prune filling or Lekvar onto center of each square, overlap, 2 opposite corners over filling about 1 longer.  Remove to wire rack; cool.

3\. Place pastries, 2 inches part, on greased cooky sheet.  Let rise in warm place, away from draft, until double in bulk, about 3o minutes.  Brush with beaten egg.

4\. Place in hot oven (400 degrees); immediately reduce heat to 350.  Bake 20 minutes.  Heat corn syrup just until warm in a small saucepan; brush over pastries; bake 5 minutes longer.  Remove to wire rack; cool.  Makes 12 pastries.

_Family Circle Cookbook_
