---
title: "Coffee-Ginger Mocktail"
author: "pencechp"
date: "2018-12-31"
categories:
  - cocktails
tags:
  - coffee
  - mocktail
---

## Ingredients

*   1-2 oz. cold brew concentrate, _or_ 4 oz. nice cold brew coffee
*   1 slice lime
*   handful ice
*   top quality ginger ale/beer (Trader Joe's is nice for those in the US)

## Preparation

Place the slice of lime in a highball glass and muddle it. Add a handful of ice and the coffee. Fill the rest of the glass with the ginger ale.

_Idea due to Black & Bloom, Groningen, NL_
