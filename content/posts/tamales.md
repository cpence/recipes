---
title: "Tamales"
date: 2022-03-04T10:28:33+01:00
categories:
  - main course
tags:
  - mexican
  - pork
---

## Ingredients

_For the chile sauce:_

- 8 oz. red chile pods (e.g., red New Mexico)
- 6 c. water
- 6 tbsp. flour
- 4 cloves garlic
- 1 tbsp. salt

_For the pulled pork:_

- 7-8 lb. pork butt or pork shoulder
- 2 1/2 c. water
- 1 tbsp. salt

_For the masa:_

- 2 lb. lard
- 2 tsp. baking powder, divided
- 2 tbsp. salt, divided
- 5 lb. masa
- 2-3 c. broth (from above)
- 1/2 c. red chile sauce (if making red chile pork tamales)

_For the tamales:_

- 6 tbsp. flour
- corn husks

## Preparation

_Make the chile sauce:_

Remove stems, seeds, and veins from the chile pods. Place in a colander and rinse well with cool water.

Add the chiles to a large pot and add enough water so they are just covered. Bring water to a boil. Lower the heat, cover, and simmer for about 20 minutes. After 10 minutes turn the chiles over with tongs to make sure the chiles soften evenly. Drain cooked pods and allow time to cool down before blending. Discard water.

Fill blender with 3 cups of water, half of the cooled chile pods, 3 tablespoons flour, 2 cloves garlic, and half of the salt. Blend until smooth. Strain sauce through a fine sieve to remove skins and seeds; discard skins and seeds. Repeat blending and straining process with remaining water, pods, flour, garlic, and salt. If necessary, season with more salt.

**Note:** You can make a double batch of this sauce and keep some frozen.

_Make the pulled pork:_

Place pork, water, and salt in a slow cooker and cook for 6 to 8 hours. Remove, cool, and shred. As you shred, set aside the fat that you remove, if any.

Combine the cooled broth from the pork and the leftover fat in a blender, and blend. Reserve for use when making the tamale masa and filling.

**Note:** This will also keep, covered, for 1 week in the fridge, or freeze.

Place 1 lb. lard in a large mixer and mix until fluffy, scraping the sides regularly. Add half the baking powder and half the salt, and mix.

Add half the masa, and mix. Slowly add half the broth and half the chile sauce (if using), and mix. At this point, the mixture should be the consistency of smooth peanut butter. If not, add some more broth. Test the masa by taking 1/2 tsp. and dropping it into a cup of warm water. If it floats, it's ready. If it sinks, add a little more lard, beat for a minute, and test it again.

Repeat those steps for the other half of the ingredients. Cover the masa and set aside.

_Make the filling:_

Heat 6 tbsp. of the broth in a large skillet. Add flour and cook for at least 4 to 5 minutes. Add one batch of the red chile sauce (around 6 1/2 c.) and cook for 10 minutes. Add the pork and stir. Simmer for at least 10 minutes. Let cool before making tamales.

_Make the tamales:_

Soak the corn husks in water for an hour before using, and rinse well. Keep them in water while you work on the tamales.

Place the wide end of the husk in the palm of your hand, narrow end at the top. Starting at the middle of the husk, spread 2 tbsp. of masa with the back of a spoon in a rectangle or oval shape, using a downward motion toward the bottom edge. Do not spread to the ends; leave about a 2" border on the left and right sides.

Spoon 1 1/2 tbsp. of your filling down the center of the masa. Fold both sides to the center. Finish off by bringing the pointed end of the husk down toward the filled end. Make sure that closure is snug. Secure by tying a thin strip of corn husk around the tamal.

Use a deep pot or tamale steamer to steam tamales. Cover with a tight-fitting lid and steam for 2 1/2 to 3 hours. To test if done, try taking off a corn husk; if they come off without sticking, the tamales are done.

_Muy Bueno_
