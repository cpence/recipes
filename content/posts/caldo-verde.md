---
title: "Caldo Verde"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - portuguese
  - soup
---

## Ingredients

*   1/4 cup extra-virgin olive oil
*   12 ounces Spanish chorizo, cut into 1/2-inch pieces
*   1 onion, chopped fine
*   4 garlic cloves, minced
*   Salt and pepper
*   1/4 teaspoon red pepper flakes
*   2 pounds potatoes, peeled and cut into 3/4-inch pieces
*   4 cups chicken broth
*   4 cups water
*   1 pound collard greens, stemmed and cut into 1-inch pieces
*   2 teaspoons white wine vinegar

## Preparation

Heat 1 tablespoon oil in Dutch oven over medium-high heat until shimmering. Add chorizo and cook, stirring occasionally, until lightly browned, 4 to 5 minutes. Transfer chorizo to bowl and set aside. Reduce heat to medium and add onion, garlic, 1 1/4 teaspoons salt, and pepper flakes and season with pepper to taste. Cook, stirring frequently, until onion is translucent, 2 to 3 minutes. Add potatoes, broth, and water; increase heat to high and bring to boil. Reduce heat to medium-low and simmer, uncovered, until potatoes are just tender, 8 to 10 minutes.

Transfer 3/4 cup solids and 3/4 cup broth to blender jar. Add collard greens to pot and simmer for 10 minutes. Stir in chorizo and continue to simmer until greens are tender, 8 to 10 minutes longer.

Add remaining 3 tablespoons oil to soup in blender and process until very smooth and homogeneous, about 1 minute. Remove pot from heat and stir pureed soup mixture and vinegar into soup. Season with salt and pepper to taste, and serve. (Soup can be refrigerated for up to 2 days.)

_CI_
