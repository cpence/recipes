---
title: "Tarte salée aux asperges, jambon et ricotta"
date: 2021-08-09T10:14:00+02:00
categories:
  - main course
  - side dish
tags:
  - asparagus
  - ham
---

## Ingredients

- 5 tranches de jambon cuit
- 3 gros œufs
- 1 kg (2 bottes) d'asperges blanches
- 3 oignons jeunes
- 1 paquet de 10 carrés de pâte feuilletée surgelée (450g)
- 1 càs bombée de farine
- 250g de ricotta
- poivre et sel

## Preparation

Préchauffez le four à 1809 C et sortez-en la grille. Sur une grande feuille
papier cuisson, formez un grand rectangle (environ 36x24cm) en rassemblant 6
carrés de pâte feuilletée (préablement déglelés): mouillez légèrement les côtés
et faites-les se chevaucher sur environ 1 cm. Appuyez avec les dents d'une
fourchette pour les souder. Badigeonnez les bords du grand rectangle avec 1 oeuf
battu. Taillez chacun des carrés de pâte restants en 6 bandelettes et
déposez-les sur les bords pour former un cadre. Badigeonnez à nouveau d'oeuf
battu et ajoutez une 2e couche de bandelettes. Badigeonnez-les d'oeuf battu,
piquez bien tout le fond de tarte à la fourchette et faites glisser le tout
(tarte + papier) sur la grille du four. Faites cuire 12 min.

Pendant ce temps, supprimez la base dure des asperges et pelez-les. Faites-les
cuire environ 8 min à l'eau bouillante salée (elles doivent être « al dente »).
Rincez-les à l'eau froide et séchez-les.

Mélangez la farine et l'oeuf battu restant au fouet, puis ajoutes les 2 autres
oeufs et la ricotta; melangez bien et rectifiez l'assasionnement.

Garnissez le fond de tarte avec 4 tranches de jambon. Recouvrez avec les 3/4 de
la crème, enfoncez-y les asperges en rangs bien serrés, puis nappez avec le
reste de la crème. Poivrez et faites dorer 20 min au four.

Émincez finement les oignons jeunes et la tranche de jambon restante.
Parsemez-en la tarte et servez chaud. Accompagnez d'une salade.

_Delhaize_
