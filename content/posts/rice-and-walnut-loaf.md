---
title: "Rice and Walnut Loaf"
author: "pencechp"
date: "2014-11-11"
categories:
  - main course
tags:
  - cashew
  - rice
  - untested
  - vegetarian
  - walnut
---

## Ingredients

*   1 1/4 c. cooked brown rice
*   1/4 c. walnuts, roasted and chopped
*   2 tbsp. sunflower seeds, lightly roasted
*   1/4 c. minced onion
*   1/4 c. minced celery
*   6 eggs, beaten
*   1 tbsp. dijon mustard
*   1 tsp. salt
*   1/2 tsp. pepper
*   1/2 tsp. sage
*   1 c. grated cheddar cheese
*   3 tbsp. olive oil
*   3 lg. onions, diced
*   3/4 c. cashew butter
*   1/2 c. tamari
*   2 1/2 c. water
*   1/4 tsp. pepper
*   1/4 tsp. sage

## Preparation

Preheat oven to 350. Combine the first 11 ingredients (until and including the cheese) and mix well. Put in a greased 8 1/2x4 1/2" loaf pan. Bake until lightly browned and pulling away from the sides a bit, 60-90 minutes.

Meanwhile. heat the oil in a large skillet over medium heat. Add onions and cook until caramelized. Add cashew butter, tamari, water, and spices, and cook until heated through. Place in blender or food processor and blend until smooth.

_Penzey's Spices_
