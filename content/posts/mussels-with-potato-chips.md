---
title: "Mussels with Potato Chips"
author: "pencechp"
date: "2014-01-15"
categories:
  - appetizer
tags:
  - mussels
  - spanish
---

## Ingredients

*   1 bag nicest potato chips you can find
*   1 can tinned mussels, preferably either ceviched or stewed
*   Pimenton de la vera (smoked Spanish paprika) to taste
*   olive oil

## Preparation

Arrange whole, unbroken chips on a serving dish. Top each chip with a mussel and finish with a pinch of pimenton.  Finish the plate with a few drops of the sauce from the mussels and a drizzle of olive oil.

_Jose Andres Foods_
