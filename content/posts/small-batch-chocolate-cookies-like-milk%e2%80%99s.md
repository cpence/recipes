---
title: "Small Batch Chocolate Cookies (Like Milk’s)"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - chocolate
  - cookie
  - smallbatch
---

## Ingredients

*   1 1/3 ounces unsweetened chocolate (37 grams)
*   1 tablespoon plus 2 teaspoon unsalted butter (20 grams)
*   2 tablespoons plus 2 teaspoons flour (21 grams)
*   2 teaspoons cocoa powder (4 grams)
*   1/8 teaspoon baking powder (.7 grams)
*   1/8 teaspoon salt (.75 grams)
*   1 large egg
*   1/2 teaspoon vanilla extract
*   1/3 cup granulated sugar (65 grams)
*   1/3 pound bittersweet chocolate (150 grams)

## Preparation

In a bowl set over a saucepan of simmering water, melt the unsweetened chocolate
and butter. Remove from the heat and cool slightly.

Meanwhile, stir together the flour, cocoa powder, baking powder and salt. Set
aside.

Combine the egg, vanilla and sugar in a bowl and mix with a fork. For puffier
cookies, beat egg mixture with an electric mixer on high for 1 ½ minutes (until
fluffy).

Add the melted chocolate to the egg mixture and mix just until combined. Stir in
the dry ingredients and mix just until combined, then stir in the bittersweet
chocolate. Chill batter for one hour.

Heat the oven to 350 degrees. Line a cookie sheet with parchment paper.

Divide the dough into 6 portions and shape the portions into balls. They should
be about 1 ¼ inches each. Place the balls on cookie sheet and space about 2
inches apart. Bake for 10-12 minutes (I baked mine about 13) and rotate the pan
halfway. Take them out before you think they're done, or they'll get overcooked.

Cool on cookie sheet for a few minutes, then transfer to a wire rack to cool
completely.

Makes 6

_L.A. Times, via Cookie Madness_
