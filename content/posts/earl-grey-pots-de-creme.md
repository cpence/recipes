---
title: "Earl Grey Pots de Creme"
author: "pencechp"
date: "2011-09-03"
categories:
  - dessert
tags:
  - custard
  - tea
---

## Ingredients

*   1 cup heavy cream
*   1 cup whole milk
*   2 Tbsp. Earl Grey tea leaves (about 5 tea bags)
*   1/2 cup granulated sugar
*   4 egg yolks
*   1/2 tsp. grated lemon zest
*   Pinch salt

## Preparation

In a small saucepan, bring cream and milk just to a boil over medium heat.  Turn off heat and stir in tea.  Cover and let steep for at least 30 minutes or up to 2 hours.

Heat oven to 325 degrees.  Arrange six 4-ounce oven-proof ramekins in a roasting pan and add water to come halfway up the sides of the ramekins.

In a medium bowl, whisk together sugar, egg yolks, lemon zest and salt.  Strain infused cream into the egg yolks.  Extract as much liquid as possible without pressing on tea leaves.  Discard tea.  Whisk thoroughly to combine.

Divide custard mixture among cups.  Cover pan tightly with foil, poking a few holes to let steam escape.  Carefully transfer pan to oven.  Bake until custards are set but still slightly wobbly in centers, about 3o minutes.  Transfer ramekins to a wire rack to cool, then refrigerate for at least two hours before serving.

_Martha Stewart, via Austin American Statesman_
