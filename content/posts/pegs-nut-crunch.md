---
title: "Peg's Nut Crunch"
author: "juliaphilip"
date: "2013-06-06"
categories:
  - dessert
tags:
  - chocolate
  - pecan
  - philip
---

## Ingredients

*   1 1/4 c sugar
*   3/4 c butter
*   1 1/2 tsp salt
*   1/4 c water
*   1/2 c unblanched almonds (optional)
*   1/2 tsp baking soda
*   1 c semi-sweet chocolate chips
*   1/2 c nuts

## Preparation

Bring to a boil the first 5 ingredients. Heat to 290 F (or until the mixture turns golden brown), stirring constantly. Take off heat and stir in baking soda. Pour into a greased 9 x 15 jelly roll pan and spread out. Melt chocolate, then pour over the candy and put the nuts on top. Allow to cool overnight (in the refrigerator or outside if desired). Break into pieces. Make about 1 1/2 pounds.
