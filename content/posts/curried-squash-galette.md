---
title: "Curried Squash Galette"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
tags:
  - pie
  - squash
  - untested
---

## Ingredients

_For dough:_

*   1 1/4 cups all-purpose flour
*   Kosher salt and pepper
*   1 stick unsalted butter, frozen
*   Ice water

_For filling:_

*   1 pound butternut squash—peeled, seeded and cut into 1/4-inch-thick slices
*   1 pound kabocha squash—peeled, seeded and cut into 1/4-inch-thick slices
*   1 red onion, cut through the core into 1/2-inch wedges
*   1/4 cup extra-virgin olive oil
*   2 teaspoons Madras curry powder
*   Kosher salt and pepper
*   1/2 cup sour cream
*   1/2 cup shredded Manchego, plus more for serving

## Preparation

In a large bowl, whisk the flour with 3/4 teaspoon each of salt and pepper. Working over the bowl, grate the frozen butter on the large holes of a box grater. Gently toss the grated butter in the flour. Stir in 1/3 cup of ice water until the dough is evenly moistened. Scrape out onto a work surface, gather up any crumbs and knead gently just until the dough comes together. Pat into a disk, wrap in plastic and refrigerate until chilled, about 1 hour.

Meanwhile, preheat the oven to 425°. On a large rimmed baking sheet, toss the butternut and kabocha squash and the onion with the olive oil and curry powder. Season generously with salt and pepper. Roast for 15 to 20 minutes, until the squash is tender but not falling apart. Let cool.

Increase the oven temperature to 450°. On a lightly floured work surface, roll out the dough to a 14-inch round. Carefully transfer to a parchment paper–lined baking sheet. Spread the sour cream over the dough, leaving a 1 1/2-inch border. Sprinkle 1/4 cup of the cheese on top. Arrange the squash and onion over the sour cream and sprinkle the remaining 1/4 cup of cheese on top. Fold the pastry edge up and over the vegetables to create a 1 1/2-inch border.

Bake the squash galette for 30 to 35 minutes, until the crust is browned; let cool slightly. Sprinkle with shredded cheese, cut into wedges and serve warm.

_Food & Wine_
