---
title: "Moroccan Salad"
author: "pencechp"
date: "2017-10-29"
categories:
  - side dish
tags:
  - mediterranean
  - salad
---

## Ingredients

_For salad:_

*   2 small eggplants
*   1 tsp. salt
*   2 tomatoes, cut in large dice
*   4 scallions, sliced thin
*   1 cucumber, cut in large dice
*   1 green bell pepper, diced
*   1 red bell pepper, diced
*   1/4 c. parsley, minced
*   spinach leaves
*   yogurt for topping

_For dressing:_

*   2 garlic cloves, crushed
*   3 tbsp. lemon juice
*   3 tbsp. tahini
*   1/2 c. olive oil
*   1/4 c. red wine vinegar
*   2 tsp. cumin seeds
*   1/2 tsp. ground pepper
*   1/2 tsp. oregano
*   salt to taste

## Preparation

Peel eggplant and slice into 1/2" rounds. Place on baking sheet, sprinkle with salt, and let stand for 30 minutes. Meanwhile, mix together all ingredients for dressing.

Rinse and dry eggplant. On an oiled baking sheet, broil eggplant until it browns lightly on both sides. Avoid overcooking. Eggplant should be tender enough to cut with a fork. Cut the warm eggplant into bite-size pieces. Toss with dressing and let cool for several hours, unrefrigerated.

Just before serving, toss all the other vegetables gently with the eggplant and dressing. Serve over spinach leaves, with yogurt on top.
