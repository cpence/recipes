---
title: "Mediterranean Meat Pies"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
  - side dish
tags:
  - lamb
  - mediterranean
  - untested
---

## Ingredients

*   [Mediterranean pie dough]( {{< relref "mediterranean-pie-dough" >}} )
*   2 tbl olive oil
*   1 onion, finely chopped
*   1 lb ground or minced lamb shoulder
*   1/2 c chopped fresh Italian parsley
*   1/4 c pine nuts, toasted
*   1/4 tsp ground allspice
*   1/2 tsp ground cinnamon
*   1 tsp salt
*   1/2 tsp freshly ground pepper
*   yogurt for serving

## Preparation

Make the dough as directed. Preheat oven to 400 F. Lightly oil a baking sheet.

To make the filling, in saute pan over medium heat warm the oil. Add the onion and saute until tender, about 5 min. Transfer to a bowl. Add the lamb, parsley, nuts, spices and seasonings and mix well.

Divide the dough into 12 portions and roll out in 3 in rounds. Top each round with a heaping spoonful of the filling, then bring up the three edges of the round to meet in the center and press together to seal. The pasties will be triangular.

Place on the prepared baking sheet. Brush the pastries with oil. Bake until golden, 15-20 min. Serve warm with yogurt.
