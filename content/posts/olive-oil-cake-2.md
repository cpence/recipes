---
title: "Olive Oil Cake"
date: 2023-04-16T17:36:23+02:00
categories:
  - dessert
tags:
  - cake
  - untested
---

## Ingredients

- ¾ cup/177 milliliters extra-virgin olive oil, plus more for greasing the pan
- ½ cup/118 milliliters Earl Grey tea, or use coffee, dry red wine, orange juice or water
- ½ cup/50 grams Dutch-processed cocoa powder
- ½ teaspoon ground cinnamon
- ¼ teaspoon fine sea salt
- 1 cup/200 grams granulated sugar
- 3 large eggs, at room temperature
- 2 teaspoons vanilla extract
- 1 cup plus 2 tablespoons/135 grams all-purpose flour
- ½ teaspoon baking soda

## Preparation

1. Heat the oven to 325 degrees. Grease a 9-inch round pan and line the bottom with parchment paper.

2. In a medium saucepan over high heat, bring tea or other liquid to a simmer, then turn off heat. Whisk in cocoa, cinnamon and salt until smooth, then set aside to cool.

3. In the bowl of an electric mixer fitted with the paddle attachment, combine sugar, olive oil, eggs and vanilla. Beat for about 3 minutes. Reduce speed and pour in cocoa mixture, scraping down the sides of the bowl. Gradually beat in flour and baking soda until just incorporated.

4. Scrape batter into prepared pan and bake until the sides are set but it’s still slightly damp in the center, 35 to 45 minutes. A cake tester should come up clean but with a few sticky chocolate crumbs clinging to it. Transfer cake pan to a wire rack and let cake cool completely in pan.

_NYT_
