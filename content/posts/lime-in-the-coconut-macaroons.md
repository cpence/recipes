---
title: "Lime-in-the-Coconut Macaroons"
author: "pencechp"
date: "2013-05-12"
categories:
  - dessert
tags:
  - coconut
  - cookie
  - lime
  - untested
---

## Ingredients

*   2 large egg whites
*   1/4 cup sugar
*   1 tablespoon finely grated lime zest
*   1/4 teaspoon kosher salt
*   7 ounces unsweetened coconut shavings (about 4 cups)

## Preparation

Place racks in upper and lower thirds of oven; preheat to 325°F. Whisk egg whites and sugar in a large bowl just until frothy. Whisk in lime zest and salt. Add coconut and fold to coat.

Drop heaping tablespoonfuls of coconut mixture onto 2 parchment paper-lined rimmed baking sheets. Bake, rotating pans halfway through from top to bottom and back to front, until golden brown, 18-22 minutes. Let cool on baking sheets.

_Bon Appetit, March 2013_
