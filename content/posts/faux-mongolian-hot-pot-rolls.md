---
title: "Faux Mongolian Hot Pot Rolls"
author: "pencechp"
date: "2010-05-08"
categories:
  - bread
tags:
  - chinese
---

Thaw a loaf of dough for about 3 hours on a plate covered with pam-sprayed plastic wrap - it should be thawed, still cool, and not starting to rise much.

Divide dough into 12 pieces. Flatten each piece and put it in your palm to form a cup shape. Then, using a marble-sized piece of dough, spread sesame oil on the surface.

Gather edges together, seal, turn over and pat flatish, place on greased baking sheet. Brush tops of rolls with sesame oil, sprinkle heavily with sesame seeds, cover with cloth and let stand for a couple of hours. They don't need to rise much, so you have a lot of flexibility with when you get them into the oven.

Bake for about 20 minutes at 350 or until golden brown.
