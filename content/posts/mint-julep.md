---
title: "Mint Julep"
author: "pencechp"
date: "2013-05-12"
categories:
  - cocktails
tags:
  - mint
  - whiskey
---

## Ingredients

*   4 cups bourbon
*   2 bunches fresh mint
*   1 c. water
*   1 c. sugar

## Preparation

Remove ~40 small mint leaves, wash, and place in bowl.  Cover with 1/4 c. bourbon and soak for 15 minutes.  Gather the leaves in paper towel and wring out over the bowl.  Dip the bundle again, and repeat the process several times.

Prepare 1:1 simple syrup using the water and sugar; cool.

Pour 3 1/2 c. bourbon into a large bowl or pitcher.  Add 1 c. of the simple syrup.  Add the mint extract 1 tbsp. at a time until there is a mild mint taste to the mix (each batch is different).  \[CP: I used all of it the first time I made the recipe.\]  Refrigerate for at least 24 hours to meld flavors.

Fill a glass 1/2 full with shaved ice.  Add a mint sprig.  Fill the rest of the way with shaved ice.  Fill glass with julep mixture and serve.

_Bill Samuels, Food Network_
