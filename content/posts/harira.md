---
title: "Harira"
date: 2024-08-05T08:58:53+02:00
categories:
  - main course
tags:
  - soup
  - moroccan
  - untested
---

The Harira is the national soup.

During 30 days of Ramadan, every house prepares this perfumed soup, imprenating
the streets of its scent at the sundown

It is eaten along with dates, or honey sweeties (Chabakkia, briouats with
almonds and honey).

There are many varieties of this soup, but the classical one is the following.
It should be prepared in two times:

1. Bouillon (Bubbling)
2. Tédouira.

## Ingredients for Bubbling

- 250g of beef or muton meat cut into little pieces
- juice of 1 lemon
- 4 or 5 little fleshy bones
- 500 g of whole litthe onions
- a half coffee spoonful of safran (half natural and half artificial)
- 1 coffee spoonful of pepper
- 1 butter walnut
- 250 g of lentil
- salt
- 1 & half liter of water for the bubbling

## Preparation

Cook the lentil in salted water, darin the juice of 1 citron. Put it apart. Cook
the other ingredients in an under cover saucepan on a little fire, after
boiling, remove the onions when they are cooked to keep them complete.

1 hour past, verify that meat is cooked then remove it fron fire, put the lentil
and the onions in the same container.

## Ingredients for Tédouira

- 1 bouquet of coriander
- 1 bouquet of parsley
- 1 kg and a half of moulded tomatoes or 1 box of concentrated and mixed
  tomatoes in 1 litre and a half of water
- 2 litres and a half of water
- 1 3/4 filled cup tea of lemon juice
- 1 butterwalnut
- 200 g of flour
- salt

## Preparation

Boil water with the moulded or concentrated tomatoes, add butter, then boil for
quarter an hour, add the bubbling juice over the meat, lentil, onions, ect…in
the saucepan.

Out of fire, put the mixed flour in 1 litre of water, while shaking it fast to
avoid having clots. Put it back on fire, then shake the mixture till boiling.

Add the parsley and corrainder cut to little pieces or crushed with mortar with
the necessary quantity of salt for condiment.

Dilute with 1 liter of water, and pour into the marmite, keep shaking (we can
also crush them in a mixer full of water). Verify the condiment and remove it
from fire when the soup is very hot.

The harira should be velvety, not thick.

**N.B.** We can replace meat by the fellings of chiken.

_The website of the government of Morocco, printed out by my grandmother on
November 22, 1999_
