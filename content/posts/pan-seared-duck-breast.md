---
title: "Pan Seared Duck Breast with Orange Pan Sauce"
date: 2021-08-09T18:09:05+02:00
categories:
  - main course
tags:
  - duck
---

## Ingredients

- 4 duck breasts, 4 to 5 ounces (112 to 140g) each
- Kosher salt
- 1/2 cup dry white wine (4 fluid ounces; 120ml)
- 1 1/2 cups homemade chicken stock or store-bought stock combined with 1
  tablespoon powdered gelatin (12 fluid ounces; 360ml)
- 4 tablespoons unsalted butter (2 ounces; 60g)
- 1/4 cup (60ml) freshly squeezed orange juice from 1 orange
- 1 teaspoon freshly grated orange zest
- Freshly ground black pepper, to taste

## Preparation

With a sharp knife, gently score duck breast skin in a tight crosshatch pattern,
keeping the scores 1/8 inch apart. If you prefer a little fat left on the
breasts after cooking, just barely score the skin; to render more fat, score
more deeply, taking care not to expose the flesh.

Season duck breasts with salt, heavily on the skin side and lightly on the flesh
side.

Place duck breasts, skin side down, in a large, cold sauté pan. Place pan over
low to medium-low heat. To keep the edges from curling up, press duck breasts
down with the help of a smaller sauté pan. After about 5 minutes, the fat should
begin to gently bubble. If the fat is either silent or spitting, adjust heat
accordingly. Maintain the gentle bubble of fat, pouring out excess rendered fat
throughout the cooking process, until much of the fat has rendered, skin is
golden brown, and duck's internal temperature is 125°F (52°C), about 15 minutes.

Increase heat to medium and further brown skin if needed, about 1 minute, before
flipping and cooking on the flesh side. For medium-rare meat, cook until breast
registers 130°F (54°C) on an instant-read thermometer, about 1 to 2 minutes.
Continue cooking until duck registers 140°F (60°C) for medium or 155°F (68°F)
for well-done. Remove duck from pan and set aside to rest.

For the Pan Sauce: Over high heat, deglaze sauté pan with white wine. Scrape up
any brown bits stuck to pan and let wine reduce until pan is almost dry and only
1 to 2 tablespoons remain, about 2 minutes. Add chicken stock and let reduce by
half, until sauce is sticky and rich, about 2 minutes. Remove sauce from heat
and swirl in butter until melted and evenly incorporated. Season sauce with
orange juice and zest, salt, and black pepper. Serve with duck breast.

_Serious Eats_
