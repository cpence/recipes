---
title: "Pickled Okra"
author: "pencechp"
date: "2017-07-20"
categories:
  - side dish
tags:
  - okra
  - pickle
---

## Ingredients

*   2 pounds young, small to medium okra pods
*   4 small dried chiles, split in 1/2
*   2 teaspoons mustard seeds
*   12 sprigs fresh dill
*   4 cloves garlic, whole
*   1 teaspoon whole peppercorns
*   1/4 cup kosher salt
*   2 cups rice wine vinegar
*   2 cups bottled water

## Preparation

Wash the okra and trim the stem to 1/2-inch. Place 1 chile, 1/2 teaspoon mustard seeds, 3 sprigs of dill, 1 clove of garlic and 1/4 teaspoon peppercorns in the bottom of each of 4 sterilized pint canning jars. Divide the okra evenly among the 4 jars, standing them up vertically, alternating stems up and down.

In a medium saucepan over medium heat, bring the salt, vinegar and water to a boil. Once boiling, pour this mixture over the okra in the jars, leaving space between the top of the liquid and the lid. Seal the lids. Set in a cool dry place for 2 weeks.

_Alton Brown_
