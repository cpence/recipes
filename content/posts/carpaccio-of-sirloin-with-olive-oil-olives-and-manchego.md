---
title: "Carpaccio of Sirloin with Olive Oil, Olives, and Manchego"
author: "pencechp"
date: "2010-07-21"
categories:
  - main course
tags:
  - beef
  - spanish
---

## Ingredients

*   12 oz rump of beef
*   4 fl oz olive oil
*   2 oz cured Manchego cheese
*   12 black olives
*   Spanish sea salt
*   Freshly-ground black pepper

## Preparation

Freeze the piece of beef for half an hour then slice very thinly by machine. Arrange the slices on the plates and drizzle with oil. Sprinkle with flakes of Manchego cheese, Spanish sea salt and freshly-ground black pepper. Chop the olives very finely, sprinkle over the meat and serve.

_Spain GourmeTour Magazine, via LaTienda.com_
