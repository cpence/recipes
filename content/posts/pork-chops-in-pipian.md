---
title: "Pork Chops in Pipian"
author: "pencechp"
date: "2016-02-24"
categories:
  - main course
tags:
  - mexican
  - pork
  - untested
---

## Ingredients

*   4 medium-thick pork chops, bone-in or boneless
*   Kosher salt and freshly ground black pepper
*   2 tablespoons all-purpose flour
*   2 tablespoons neutral oil
*   8 chiles de árbol
*   3 plum tomatoes
*   1 small onion, peeled and thickly sliced
*   3 cloves garlic, unpeeled
*   ½ cup raw, hulled, unsalted pumpkin seeds
*   ⅓ cup unsalted peanuts
*   ⅓ cup hulled sesame seeds
*   ½ teaspoon ground cinnamon
*   ¼ teaspoon ground cloves
*   ¼ teaspoon ground allspice (or 2 allspice berries)
*   1 canned chipotle pepper
*   2 tablespoons neutral oil, lard or chicken fat
*   1 cup chicken broth, homemade or low-sodium
*   1 tablespoon kosher salt
*   1 tablespoon light brown sugar
*   1 tablespoon cider vinegar

## Preparation

Make the sauce: Remove the stems from the chiles de árbol, and gently roll the chiles between your fingers to remove the seeds. Discard seeds. Set a bare skillet over high heat for 5 minutes, then add the chiles. Toast until they are darkened and fragrant, approximately 4 to 5 minutes. Place them in a bowl, cover with 2 cups boiling or very hot water, and set aside to soak.

Return the skillet to high heat. Add the tomatoes, onion and garlic, and cook, turning occasionally, until charred, approximately 10 minutes. Put the vegetables on a plate, and set aside to cool, then slip the skins off the cloves of garlic.

Return the skillet to medium-low heat. Place the pumpkin seeds, peanuts and sesame seeds in the skillet, and cook, stirring and shaking the pan continuously, until they are toasted and fragrant, approximately 2 to 4 minutes. Put the seeds and nuts in a bowl, and stir in the cinnamon, cloves and allspice.

Put the chiles and soaking liquid in a blender with the tomatoes, onion, garlic, the nut-seed mixture and the chipotle. Purée until smooth. Add the oil, lard or chicken fat to a large, heavy-bottomed pot, and heat over medium heat until it is nearly smoking. Add the purée. It will sputter a lot. Lower the heat, and stir, cooking the mixture down to a thick paste. It will continue to sputter and pop. Add the broth to the paste, and stir, then season with the salt, sugar and vinegar, and cook for another 15 minutes or so, until it resembles a thick, creamy soup. Lower heat to a bare simmer.

Make the pork chops: Season the pork chops aggressively with salt and pepper, and dust them with the flour. Add the oil to the skillet, and heat over medium-high heat until nearly smoking. Add the chops, and let them cook undisturbed, in batches if necessary, until crisp and well browned, about 5 minutes per side. Set them aside to rest for 5 minutes or so. Serve a chop per person on a generous amount of sauce, with tortillas to mop it up. Extra sauce can be used to braise chicken, lamb or more pork, or as a topping for enchiladas.

_The New York Times_
