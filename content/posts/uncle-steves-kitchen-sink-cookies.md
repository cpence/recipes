---
title: "Uncle Steve's Kitchen Sink Cookies"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - cookie
  - untested
---

## Ingredients

*   2 sticks butter, room temperature
*   1 c. packed light brown sugar
*   1/2 c. sugar
*   2 eggs, room temperature
*   2 tsp. vanilla
*   1 tbsp. molasses
*   2 c. all-purpose flour
*   1 tsp. baking soda
*   1 tsp. salt
*   1 c. rolled oats
*   1 c. coconut flakes
*   1 c. bittersweet chocolate chips
*   1 c. peanut butter chips
*   1 c. butterscotch chips
*   1 c. Heath chips
*   1 c. semisweet chocolate chips
*   1 c. macadamia nuts, chopped
*   1 c. pecans, chopped

## Preparation

Preheat  oven to 350 F.  In a stand mixer or large mixing bowl, cream together butter, shortening, and sugars.  Add eggs and vanilla and mix thoroughly.  Stir in molasses.  Combine the baking soda, salt, and flour.  Stir into butter mixture.  Add oats and coconut and stir until well blended.  In a separate bowl, toss together the chips and nuts.  Add chips and nuts to batter and stir until well combined.  Form 1.5-ounce balls and place on a prepared cookie sheet.  Bake for 15-17 minutes.  Remove and let cool on a cookie sheet for 10 minutes, then a wire rack.

_Holly Allen_
