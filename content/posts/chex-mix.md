---
title: "Chex Mix"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - miscellaneous
tags:
  - pence
  - snack
---

## Ingredients

*   1 16 oz. box wheat Chexs
*   1 3 oz. can chow mein noodles
*   1 cup (or more) cashews
*   1/3 cup vegetable oil
*   3 tablespoons soy sauce
*   1 teaspoon garlic powder
*   1 teaspoon onion powder

## Preparation

Heat oven to 250.  Combine first three ingredients in a 13 x 9 inch pan.

Combine remaining four ingredients in a small bowl; quickly pour over cereal mixture.  Stir to coat evenly.

Bake 1 hour, stirring every 20 minutes; cool.  Makes 10 cups.
