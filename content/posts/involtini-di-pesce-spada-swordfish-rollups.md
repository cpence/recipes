---
title: "Involtini di Pesce Spada (Swordfish Rollups)"
author: "pencechp"
date: "2014-01-04"
categories:
  - main course
tags:
  - fish
  - italian
---

## Ingredients

*   1/4 cup extra-virgin olive oil, plus more for coating
*   1 small red onion, finely chopped
*   2 3/4 cups unseasoned dried breadcrumbs, divided
*   1 lemon, half juiced, half thinly sliced
*   1 orange, half juiced, half thinly sliced
*   1 tablespoon dried currants \[CP: can sub cranberries, white raisins\]
*   1 tablespoon pine nuts \[CP: can sub pecans\]
*   1/3 cup chopped fresh mint
*   fine sea salt and black pepper
*   1 pound swordfish, sliced into 8 thin pieces \[CP: can sub flounder\]
*   12 bay leaves, preferably fresh

## Preparation

Preheat the oven to 350 degrees. Drizzle the bottom of a medium baking dish with olive oil.

Combine the 1/4 cup olive oil and onion in a medium skillet and cook over medium-high heat until softened, about three minutes. Remove from the heat and stir in 3/4 cup breadcrumbs, mixing everything together until the breadcrumbs have absorbed the oil. Return to low heat and toast the breadcrumbs slightly. Remove from the heat and stir in the lemon and orange juices, the currants, pine nuts, and mint. Season with salt and pepper to taste.

Lay a piece of swordfish on a work surface and put a heaping tablespoon of the breadcrumb filling (squeeze it in your hand to compact it) in the center and roll up. Repeat with the remaining swordfish and filling.

Pour some olive oil into a shallow bowl and fill another shallow pan with the remaining 2 cups breadcrumbs. Dip each roll-up first in the oil, then dredge in the breadcrumbs until lightly coated. Place the swordfish roll-ups snugly in the baking dish and tuck the bay leaves and lemon and orange slices between the rolls. Drizzle with some more olive ol and bake until the fish is cooked through, about 10-15 minutes.

_Fabrizia Lanza, via Ciao Chow Linda_
