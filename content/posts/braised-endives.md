---
title: "Braised Endives"
date: 2020-04-11T13:34:32+02:00
categories:
    - side dish
tags:
    - belgian
---

## Ingredients

*   8 endives
*   2 tbsp. olive oil
*   1 c. water + 1/2 cube beef bouillon, or 1 c. beef stock
*   salt and pepper
*   herbes de provence

## Preparation

Heat the olive oil in a skillet. Trim off the ends of the endives and discard any damaged or dirty leaves. Cut the endives in half length-wise, and place them in the skillet, flat side down.

Cook them over medium heat for fifteen minutes, turning every five minutes and making sure that they don't burn.

Add half the liquid (if using bouillon, don't add it yet, just add water). Salt just a bit, especially if your stock is salty. Add pepper and herbes de provence. Cook for another fifteen minutes, turning regularly.

Add the rest of the liquid (and the bouillon cube, if using). Let simmer and reduce, ensuring that nothing sticks, until all the liquid clings to the endives.

*Le journal des femmes, translated*

