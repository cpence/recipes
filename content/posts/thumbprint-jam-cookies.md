---
title: "Thumbprint Jam Cookies"
author: "pencechp"
date: "2012-05-12"
categories:
  - dessert
tags:
  - cookie
  - jam
---

## Ingredients

*   1 c. unsalted butter, room temperature
*   1 c. sugar
*   1 egg plus 1 egg yolk, room temperature
*   1 tsp. vanilla
*   2 2/3 c. flour
*   3/4 tsp. salt
*   1/2 c. walnuts, very finely chopped
*   1/3 c. jam/jelly/preserve/fruit butter/etc.

## Preparation

1\. Cream butter using a mixer with a paddle attachment on medium. Add sugar, beat until smooth. Add egg, yolk, vanilla one at a time, mix until incorporated.

2\. Combine flour, salt, and ground walnuts in a separate bow and mix.  Slowly add to the butter.  Mix until dough pulls away from the slide of the bowl.  Knead a few times, wrap in plastic and chill 1 hour.

3\. Preheat oven to 350.  Lightly grease or parchment a cookie sheet.

4\. Roll dough into 1" balls.  Place on cookie sheet 2" apart.  Make an indentation in the center of each with your thumb.  Fill with a generous 1/4 tsp. of jam.

5\. Bake until edges are golden brown, 14-15 minutes. Remove from oven and allow to rest for a minute before transferring to a rack to cool.  Top with additional jam if desired.

Makes 3 dozen.

_Stonewall Kitchen_
