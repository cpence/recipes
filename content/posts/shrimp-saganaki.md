---
title: "Shrimp Saganaki"
author: "pencechp"
date: "2013-03-13"
categories:
  - appetizer
tags:
  - shrimp
  - untested
---

## Ingredients

*   1 tablespoon olive oil
*   4 scallions, white part only, minced
*   2 garlic cloves, chopped
*   1 cup drained diced canned tomatoes
*   1/2 cup dry white wine
*   1/3 cup vegetable broth
*   2 tablespoons ouzo or other unsweetened anise-flavored liqueur
*   1 teaspoon chopped flat-leaf parsley plus more for garnish
*   1 teaspoon chopped fresh dill plus more for garnish
*   Pinch of dried oregano, preferably Greek
*   Kosher salt and freshly ground black pepper
*   12 medium shrimp (about 1/2 pound), peeled, deveined (head on if desired)
*   1 4-ounce block feta
*   Slices of country-style white bread, toasted

## Preparation

Heat oil in a medium heavy skillet over medium-low heat. Add scallions and garlic; cook, stirring often, until soft, about 3 minutes. Add tomatoes and cook, stirring occasionally, until reduced by half, 4-6 minutes. Remove from heat; add wine, broth, ouzo, 1 teaspoon parsley, 1 teaspoon dill, and oregano. Season with salt and pepper. Cook over medium-high heat, stirring often, until tomato mixture is reduced by one-third, about 5 minutes.

Reduce heat to medium. Season shrimp with salt and pepper. Add shrimp to skillet, arranging around edges. Place block of feta in center of skillet. Cover and simmer until shrimp are cooked through and feta is warm, 4-6 minutes.

Remove skillet from heat. Transfer shrimp and feta to a large shallow bowl, if desired. Garnish with parsley and dill. Serve with toast alongside.

_Serves 4 (appetizer)_

_Bon Appetit, November 2012_
