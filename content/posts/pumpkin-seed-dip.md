---
title: "Pumpkin Seed Dip"
date: 2020-11-10T14:57:40+01:00
categories:
    - side dish
    - appetizer
tags:
    - mexican
    - pumpkin
    - untested
---

## Ingredients

*   5 ounces raw pepitas (about 1 cup)
*   2 tablespoons canola oil
*   ½ cup shallots (finely chopped)
*   1 jalapeño (stemmed, seeded and finely chopped)
*   3 cloves garlic (minced)
*   Kosher salt
*   ¼ cup parsley (lightly packed)
*   ¼ cup cilantro (lightly packed)
*   2 tablespoons lime juice (freshly squeezed)
*   1 tablespoon extra-virgin olive oil
*   ¼ teaspoon orange zest (finely grated)
*   Tortilla chips (for serving)

## Preparation

In a large skillet, toast the pumpkin seeds over moderate heat, tossing
occasionally, until lightly golden, about 5 minutes. Transfer to a food
processor.

In the skillet, heat the canola oil until shimmering. Add the shallots,
jalapeño, garlic and a generous pinch of salt and cook over moderate heat,
stirring occasionally, until softened, about 5 minutes. Transfer the mixture to
the food processor and let cool.

Add the parsley, cilantro, lime juice, olive oil, orange zest and 1/4 cup of
water to the food processor and puree until nearly smooth. Season with salt.
Transfer the sikil pak to a bowl and serve with tortilla chips.

*Food & Wine*

