---
title: "Mustard Sauce for Corned Beef"
author: "pencechp"
date: "2013-03-18"
categories:
  - miscellaneous
tags:
  - sauce
  - untested
---

## Ingredients

*   1 c. white wine
*   1 tsp. minced garlic
*   1/2 c. diced onion
*   1 c. heavy cream
*   1 c. chicken stock
*   1 c. dijon
*   1 tsp. whole grain mustard
*   1 tsp. salt
*   pinch pepper
*   2-3 tsp. flour (or corn starch?)

## Preparation

In a small saucepan over medium heat, boil white wine, garlic, and onion for 5 min. Add cream and stock. Simmer for another 5 min. Whisk in mustards, salt, and pepper. Simmer for 3-4 minutes. Whisk in flour and simmer until sauce begins to thicken.  \[CP: It's the thickening step that is problematic; the recipe's original 1 tsp. flour is _no where near_ enough to thicken this much stuff.  Try up to 1 tbsp. flour, or even switch over to corn starch.\]
