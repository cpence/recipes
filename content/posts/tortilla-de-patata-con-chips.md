---
title: "Tortilla de Patata con Chips"
author: "pencechp"
date: "2014-01-15"
categories:
  - breakfast
  - main course
tags:
  - potato
  - spanish
  - tapas
  - untested
---

## Ingredients

*   7 large eggs
*   1 bag nice plain potato chips (6.7 oz)
*   2 tsp. salt
*   4 tbsp. olive oil

## Preparation

Break 6 eggs into a mixing bowl.  Add 2/3 of the bag of chips and stir, crushing them a little as you blend the eggs together.  Allow the mixture to sit until the chips absorb most of the egg, about 5 minutes.  Beat the remaining egg in a small bowl and add it to the egg-chip mixture.  Add the salt.

Heat 2 tbsp. of oil in a small saute pan (no more than 6" in diameter), over medium heat.  Add the egg mixture and stir briskly a few times with a wooden spoon to prevent them from sticking.  Shake the pan in a circular motion for 10 seconds to keep the mixture loose as the eggs start to stick together.  Lower the temperature and cook for 1 minute more.

Place a plate over the pan and invert the pan/plate so the tortilla winds up on the plate, raw side down.  If the pan looks dry, add the last 2 tbsp. of olive oil.  Return the tortilla to the pan by sliding it back in, raw side down.  Cook for another 60 seconds, until the bottom is fully set.

Slide onto a clean serving plate, garnish with a sprinkle of salt and maybe a few more chips.

_Jose Andres Foods_
