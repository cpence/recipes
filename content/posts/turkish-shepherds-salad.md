---
title: "Turkish Shepherd's Salad"
author: "pencechp"
date: "2012-07-21"
categories:
  - main course
  - side dish
tags:
  - salad
  - turkish
  - vegan
  - vegetarian
---

## Ingredients

*   1 pound tomatoes, diced
*   3/4 pound cucumbers (1 European or 4 Persian), diced
*   1 green pepper, preferably a long green Italian frying pepper, seeded and diced
*   1/2 small red onion, sliced, soaked in cold water for 5 minutes, drained and rinsed
*   1/4 cup (loosely packed) coarsely chopped flat-leaf parsley
*   1 tablespoon chopped dill
*   2 tablespoons chopped mint
*   1 teaspoon sumac
*   1/2 to 1 teaspoon Turkish or Aleppo pepper
*   Salt to taste
*   3 tablespoons fresh lemon juice
*   3 tablespoons extra virgin olive oil
*   1 to 2 ounces feta, crumbled (1/4 to 1/2 cup) (optional)
*   Black olives as desired (optional)
*   Romaine lettuce leaves and pita bread for serving (optional)

## Preparation

Combine all of the ingredients except the olives and romaine in a large bowl and refrigerate for 30 minutes. After 30 minutes toss together, taste and adjust seasonings. Garnish with olives and serve, with pita bread and romaine lettuce if desired.

_Martha Rose Shulman, New York Times_
