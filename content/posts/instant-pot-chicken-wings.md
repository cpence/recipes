---
title: "Instant Pot Chicken Wings"
author: "pencechp"
date: "2018-02-07"
categories:
  - main course
tags:
  - chicken
  - instantpot
---

## Ingredients

*   Between 1 and 2 pounds of chicken wings (1-2 doz.)
*   Seasoning for wings (dry or wet)

## Preparation

If using dry spices, roll each wing in the spice mix.

Place 1 c. of water in the bottom of the instant pot. Add the steamer basket on its stand. Place the wings in the basket, standing them upright on their ends. Close and lock the pot, and set for 10 minutes at high pressure. Quick release the vent.

Remove the wings to a baking sheet. If using a sauce seasoning, coat the wings in it now. Broil for 10 minutes to crisp up the skin.

_Adapted from A Real Food Journey_
