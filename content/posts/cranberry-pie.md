---
title: "Cranberry Pie"
author: "pencechp"
date: "2018-01-01"
categories:
  - dessert
tags:
  - cranberry
  - pie
  - untested
---

## Ingredients

*   1 quart whole cranberries
*   1 1/4 cups (250 g) sugar
*   2 1/2 teaspoons cornstarch
*   1 pinch freshly ground nutmeg
*   1/4 teaspoon salt
*   1/2 teaspoon fresh orange zest or 1 tablespoon orange liqueur
*   1/2 cup chopped walnuts (optional)
*   1 knob butter the size of a small walnut, cut into small pieces for dotting the top of the filling
*   1 - 2 teaspoons sugar for sprinkling on top of the pie
*   two pie crusts

## Preparation

Place 3 cups of the cranberries in a food processor and pulse until they are slightly chopped. In a medium bowl, place the chopped and remaining whole cranberries, sugar, cornstarch, nutmeg, salt, zest or liqueur, and optional walnuts, and mix well. In a pie plate lined with an unbaked pie crust, pour in the cranberry filling and dot with butter. Roll out the remaining crust, lay it over the fruit, and cut 5 to 6 vents on top, or cut strips and make a lattice top. Trim excess dough from the edges and crimp. Chill the pie for a minimum of 1 hour before baking.

Lightly brush an egg wash over the entire pie, including the edges. In an oven preheated to 375°F (190°C), bake on the middle rack for about 40 minutes. When there are about 10 minutes of bake time left, open the oven, pull the pie out, and quickly and evenly sprinkle the top of the pie with sugar. Close the oven and bake until the crust is just golden, or until you see steady bubbling coming out between the vents. Remove the pie from the oven and cool completely before serving.

_Food Schmooze_
