---
title: "White Barbecue Sauce"
author: "pencechp"
date: "2013-10-21"
categories:
  - miscellaneous
tags:
  - barbecue
  - sauce
  - untested
---

## Ingredients

*   1 1/2 cups mayonnaise
*   1/4 cup water
*   1/4 cup white wine vinegar
*   1 tablespoon coarsely ground pepper
*   1 tablespoon Creole mustard
*   1 teaspoon salt
*   1 teaspoon sugar
*   2 garlic cloves, minced
*   2 teaspoons prepared horseradish

## Preparation

Whisk together all ingredients until blended. Store in the refrigerator up to 1 week.

_Southern Living, August, 2005_
