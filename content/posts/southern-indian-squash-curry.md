---
title: "Southern Indian Squash Curry"
author: "pencechp"
date: "2013-02-22"
categories:
  - main course
tags:
  - indian
  - squash
  - sweetpotato
  - vegan
  - vegetarian
---

## Ingredients

*   1 lg. butternut squash (or 4 medium sweet potatoes)
*   1 tbsp. flaked almonds
*   2 tbsp. oil
*   2 medium onions, chopped 
*  1 tsp. garlic powder
*  3 slices ginger, or 1/2-1 tsp. dried ginger
*  1 red chili, deseeded and sliced, or 1/2 tsp. crushed red pepper
*  1 tsp. ground coriander
*  1 tsp. ground cumin
*  1 tsp. mustard seeds
*  1/2 tsp. turmeric
*  3/4 c. coconut milk (about half of a standard 14.5 oz can)
*  1-1.5 c. baby spinach, roughly chopped
*  2 tbsp. cilantro, chopped

## Preparation

Peel the squash or sweet potatoes. Discard the seeds and cut the flesh into 1-inch pieces. Toast the almonds in a non-stick frying pan over medium heat for 5-7 minutes and set aside. Heat the oil in a large saucepan and cook the onions until lightly browned, stirring regularly, 12-15 minutes. Add the garlic, ginger, chili, coriander, cumin, mustard seeds, and turmeric. Reduce heat to low and cook until fragrant, 3-5 minutes, stirring regularly. Add the squash or sweet potatoes and coconut milk. Stir and cover, simmer for 20 minutes until just tender. Add the spinach and cilantro, and stir until the spinach is just wilted. Serve with rice, topped with almonds.

_Penzey's Spices_
