---
title: "Fudge"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - chocolate
  - pence
---

## Ingredients

*   18 oz. chocolate chips
*   1 can sweetened condensed milk
*   1 1/2 tsp. vanilla
*   pinch salt
*   1/2 cup nuts, optional

## Preparation

Melt chocolate (I do it in the microwave).  Add remaining ingredients and stir until smooth.  Spread in 8x8x2" pan and cool.
