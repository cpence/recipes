---
title: "Rabbit Curry"
author: "pencechp"
date: "2016-07-12"
categories:
  - main course
tags:
  - curry
  - rabbit
---

## Ingredients

*   1 tbsp. olive oil
*   1 tsp. ghee or butter
*   1 rabbit, cut into serving pieces
*   salt and pepper
*   1 large onion, chopped
*   2 tbsp. minced or grated fresh ginger
*   1 tbsp. pressed garlic
*   1 sm. Anaheim or serrano chile
*   1 tbsp. curry powder
*   1 tsp. garam masala
*   1/2 tsp. turmeric
*   1/2 tsp. cayenne pepper
*   1 c. low-sodium chicken broth (or as needed)
*   1 can coconut milk
*   1 lime
*   fresh cilantro, chopped rice, for serving

## Preparation

1\. Preheat the oven to 350 degrees. In a large dutch oven over medium high heat, melt the ghee and oil. Sprinkle the rabbit with salt and pepper. When the oil is hot, brown the rabbit on all sides, about 10-15 minutes.

2\. While the rabbit is browning, pulse half of the onion, ginger, and garlic together in a food processor until finely minced.

3\. Reduce the heat under the rabbit and add the rest of the onion. Cook until golden, about 5 minutes. Add the mix from the food processor, sautee for 2-3 minutes. Stir in the curry powder, garam masala, turmeric, and cayenne. Cook, stirring constantly, for 2 minutes, then stir in coconut milk, chicken stock, and the juice and hull of 1/2 of the lime. Bring to a simmer. Cover and place in the oven to cook for 90 minutes.

4\. Remove the pot from the oven. Stir in the juice of the other 1/2 lime and the cilantro. Taste and adjust for seasoning, possibly adjusting thickness with more broth. Serve over rice.

_Lindy's Fusion, extensively modified by Mom_
