---
title: "Curried Sweet Potato Shepherd's Pie"
author: "pencechp"
date: "2014-11-11"
categories:
  - main course
tags:
  - casserole
  - sweetpotato
---

## Ingredients

*   2 baking potatoes, cubed
*   3 medium sweet potatoes, peeled and cubed a bit smaller than the white potatoes
*   3 tbsp. butter, divided
*   3 tbsp. curry powder, divided
*   1 12-oz can coconut milk, divided
*   1/4-1 tsp. salt, to taste
*   1/4-1/2 tsp. pepper, to taste
*   1 tbsp. vegetable oil
*   1 tbsp. ground cumin
*   2 tsp. ground coriander
*   2 tsp. smoked paprika
*   1 lg. onion, diced
*   3 garlic cloves, minced
*   1 lb. ground beef or pork
*   1 lb. mixed peas and carrots (frozen or fresh)
*   2 tbsp. golden raisins

## Preparation

Preheat oven to 400. Fill a large pot with water and bring to a boil. Add potatoes and sweet potatoes and cook until tender, around 15 min. Drain well and mash with 2 tbsp. butter, 1 tbsp. curry, 1/2 c. coconut milk, salt, and pepper.

In a large skillet, heat remaining butter and oil over medium heat. Add remaining curry powder, cumin, coriander, paprika, onions, and garlic. Cook for 1-2 minutes, until spices are fragrant. Add ground meat and cook until browned, stirring to break up clumps. When nicely browned, drain off excess fat. Add peas, carrots, and golden raisins. Cook for 1-2 minutes, until the vegetables have thawed (if frozen). Add remaining coconut milk and cook for 2 minutes. Mixture should be thick and saucy. Taste and add salt and pepper as desired.

Spoon the meat mixture into an ungreased 9x13 pan and press down gently. Spoon the potato mixture over the top and spread to cover. With a fork, make tine marks all over the top. Cover with foil and bake for 30 minutes. Remove foil and bake until the top is lightly browned and meat is bubbly, about another 15 minutes.

_Penzey's Spices_
