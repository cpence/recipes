---
title: "Qibe (Spiced Butter)"
author: "pencechp"
date: "2012-12-02"
categories:
  - miscellaneous
tags:
  - ethiopian
---

## Ingredients

_Note: This recipe results in **much less** final product than you would expect. 1/2 c. butter might only produce 2–3 tbsp. of qibe, following the recipe exactly. Consider taking the slurry from the bottom and pressing it through cheesecloth or coffee filters, so that you get more results._

*   1/2 c. butter (1 stick)
*   1 tbsp. powdered ginger
*   2 tsp. ground fenugreek
*   1 tsp. ground cardamom or 1 tbsp. whole black cardamom pods
*   1 tsp. ground cumin

## Preparation

Melt the butter over low heat in a small, heavy pot.  Add the spices and stir.  Let cook over very low heat for about 15 minutes.  Remove from the heat and let cool.  Skim off the foam on the top and discard.  Carefully pour the butter off the top into a container with a lid, leaving as much of the spices (especially whole cardamom pods) and liquid at the bottom behind.  What you want is the stuff in the middle.  Store in the fridge.

_Penzey's Spices_
