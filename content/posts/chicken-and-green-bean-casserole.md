---
title: "Chicken and Green Bean Casserole"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - main course
tags:
  - chicken
  - greenbean
---

## Ingredients

*   1 lb. Chicken, cut into strips
*   1 lb. Green beans, ends snapped off and in bite size pieces
*   1 package sliced mushrooms
*   1 large can or 2 small cans of condensed cream of mushroom soup
*   1 can French’s fried onions

## Preparation

Preheat the oven to 375°.  Mix together the green beans, mushrooms, soup, and half the fried onions.  Arrange the chicken on the bottom of a casserole dish.  Pour the green bean mixture over the chicken.  Bake the casserole for about 45 minutes, or until the soup is bubbling.  Sprinkle the rest of the fired onions on top of the casserole and cook for another 5 minutes, until the onions brown.
