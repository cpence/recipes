---
title: Vegetarian "Foie Gras"
author: "pencechp"
date: "2015-07-31"
categories:
  - side dish
tags:
  - pate
  - vegan
  - vegetarian
---

## Ingredients

- 12 medium-sized button mushrooms (100g, about 1 cup)
- 2 tablespoons olive oil
- 2 tablespoons butter, salted or unsalted (or equiv. olive oil)
- 1 small onion, peeled and diced
- 2 cloves garlic, peeled and minced
- 2 cups (400g) cooked (about 200g dry) green lentils
- 1 cup (140g) toasted walnuts or pecans
- 2 tablespoons freshly squeezed lemon juice
- 1 tablespoon soy sauce or tamari
- 2 teaspoons minced fresh rosemary
- 2 teaspoons fresh thyme, minced
- 2 tablespoons fresh sage or flat leaf parsley
- 2 teaspoons Cognac or brandy
- 1 teaspoon brown sugar
- 1/8 teaspoon cayenne pepper
- salt and freshly ground black pepper

## Preparation

1. Wipe the mushrooms clean. Slice off a bit of the stem end (the funky parts)
   and slice them. Heat the olive oil and butter in a skillet or wide saucepan.
   Add the onions and garlic, and cook, stirring frequently, until the onions
   become translucent, 5 to 6 minutes. Add the mushrooms and cook, stirring
   occasionally, until they’re soft and cooked through, another 5 to 8 minutes.
   Remove from heat.
2. In a food processor, combine the cooked lentils, nuts, lemon juice, soy
   sauce, rosemary, thyme, sage or parsley, Cognac (if using), brown sugar, and
   cayenne. Scrape in the cooked mushroom mixture and process until completely
   smooth. Taste, and add salt, pepper, and additional cognac, soy sauce, or
   lemon juice, if it needs balancing.
3. Scrape the pâté into a small serving bowl and refrigerate for a few hours,
   until firm.

_David Lebovitz_
