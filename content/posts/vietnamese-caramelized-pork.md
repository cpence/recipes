---
title: "Vietnamese Caramelized Pork"
author: "pencechp"
date: "2010-11-11"
categories:
  - main course
tags:
  - pork
  - vietnamese
---

## Ingredients

*   1½ cups sugar
*   2½ pounds pork belly or butt, sliced into thin, inch-long strips
*   ½ teaspoon freshly ground black pepper
*   2 tbsp. fish sauce
*   2 heaping teaspoons minced garlic
*   1 dash sesame oil
*   1 medium Vidalia onion, sliced
*   4 scallions, sliced, green part only
*   Rice for serving

## Preparation

1\. Cover bottom of a large, heavy skillet with one cup sugar and place over medium low heat. As soon as it melts and turns golden, add pork, raise heat to medium, and stir until coated. (Sugar will become sticky and may harden, but it will re-melt as it cooks, forming a sauce.)

2\. Stir in remaining sugar, salt, pepper and fish sauce. Cover and cook 2 minutes. Uncover, stir in garlic and oil and lower to simmer to reduce sauce for about 20 minutes \[CP: Or much longer! Reduce until the sauce has roughly the consistency of your usual Asian-food sticky brown sauce.\].

3\. Stir in Vidalia onions and cook until translucent, 5 to 7 minutes. Pork should be caramelized; if not, raise heat and sauté while sauce further reduces. Transfer to serving bowl, and sprinkle with scallion greens.

Yield: 4 servings.

_Michael and Thao Huynh, New York Times_
