---
title: "Creamy Oyster Stew"
author: "pencechp"
date: "2017-02-21"
categories:
  - main course
tags:
  - oyster
  - stew
---

## Ingredients

*   3 tablespoons unsalted butter
*   1 large rosemary sprig
*   1 fennel bulb—cored and finely diced, stems finely chopped
*   1 quart heavy cream
*   2 small leeks, white and pale green parts only, thinly sliced
*   Eight 1-inch-thick slices of brown bread
*   1 cup frozen baby peas, thawed
*   1 teaspoon finely grated lemon zest
*   2 tablespoons fresh lemon juice
*   2 1/2 dozen shucked oysters, with their liquor
*   Salt and freshly ground pepper
*   2 tablespoons minced chives

**Preparations**

In a small saucepan, melt the butter with the rosemary over low heat. Remove from the heat and let the rosemary butter cool to room temperature; discard the rosemary.

In a large saucepan, combine the chopped fennel stems with the cream and bring to a boil. Simmer over low heat for 20 minutes. Strain the cream and discard the chopped fennel stems. Return the cream to the saucepan. Add the diced fennel and simmer over moderate heat until tender, about 8 minutes. Add the leeks and simmer for 2 minutes. Remove the pan from the heat and let stand for 10 minutes.

Preheat the oven to 350°. Brush the bread with the rosemary butter and arrange on a baking sheet. Bake the bread for about 8 minutes, until lightly toasted.

Bring the cream back to a boil. Stir in the peas, lemon zest and lemon juice. Remove from the heat and stir in the oysters and their liquor. Let the stew stand for 1 minute. Season with salt and pepper.

Ladle the stew into bowls and stand a piece of toast in each bowl. Garnish with the chives and serve.

_Food & Wine_
