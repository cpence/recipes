---
title: "Cocoa-Dusted Lamb Chops with Pumpkin Quinoa Cakes"
author: "pencechp"
date: "2017-03-30"
categories:
  - main course
tags:
  - lamb
  - pumpkin
  - quinoa
---

## Ingredients

_Pumpkin Quinoa Cakes_

*   1 cup pumpkin, cooked
*   1 1/2 cups quinoa, cooked
*   1/2 cup sautéed onion
*   1 teaspoon red pepper flakes
*   1 teaspoon salt (to taste)
*   1 tablespoon fresh chopped thyme
*   1/2 cup buttered breadcrumbs

_Lamb_

*   4 lamb chops
*   1 tablespoon dark chocolate powder
*   1 tablespoon onion powder
*   1 tablespoon white pepper
*   2 tablespoons kosher salt
*   1/2 cup olive oil
*   1 tablespoon smoked paprika

## Preparation

_Pumpkin Quinoa Cakes:_ Combine all ingredients in a bowl and mix thoroughly. Pat out 1/4 cup of mixture into a patty and dear on both sides in about two tablespoons of oils until seated & heated through on both sides.

_Lamb:_ Combine all ingredients in a bowl and mix until combined. Add chops to the mixture and let marinate overnight. Roast in the oven until desired internal temperature is reached (about 120 degrees because of thinness).

_Edible Nashville_
