---
title: "Tonic and Bitters"
date: 2022-11-28T10:14:47+01:00
categories:
  - cocktails
tags:
  - mocktail
  - untested
---

## Ingredients

- 4 to 6 dashes (about 1/4 to 3/8 teaspoon) Angostura bitters
- 1/4 lime
- cold tonic water

## Preparation

Fill chilled glass with ice cubes. Add bitters and squeeze out the lime juice, dropping the shell into the glass. Top with tonic water, stirring gently.

_Splendid Table_
