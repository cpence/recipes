---
title: "Asian Vegetable Soup"
author: "pencechp"
date: "2010-03-08"
categories:
  - main course
tags:
  - chinese
  - soup
---

## Ingredients

*   32 oz. vegetable broth
*   32 oz. chicken broth
*   1/3 c. soy sauce
*   Leftover turkey (or other meats), around 1 lb.
*   2 bunches kale
*   2 heads bok choi
*   1/4 c. sliced fresh ginger
*   1/4 tsp. crushed red pepper flakes
*   8 oz. sliced mushrooms
*   2 tbsp. olive oil
*   around 3-4 c. bean sprouts
*   1 or 2 tsp. five spice powder
*   cilantro
*   sesame rolls or vermicelli noodles, for serving

## Preparation

Combine vegetable broth, chicken broth, soy sauce, and turkey.  Heat the broth
to a simmer, and add the kale and bok choi.  Add enough water just to cover the
greens.  Cook until the greens have wilted and cooked down.  Add the sliced
ginger and the crushed red pepper.  Cook until the greens are done.

While cooking, sauté the mushrooms in the olive oil until slightly reduced and
they've released a bit of their liquid. Once the greens are done, add the
mushrooms and the sprouts, and test the broth for seasoning (salt and pepper,
mostly).  Add the five spice powder, and cook for a few minutes to settle the
flavors.

Serve, garnished with cilantro and a bit of chili oil (if desired).

_C. Pence_
