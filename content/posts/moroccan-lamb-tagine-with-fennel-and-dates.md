---
title: "Moroccan Lamb Tagine with Fennel and Dates"
author: "pencechp"
date: "2014-03-10"
categories:
  - main course
tags:
  - lamb
  - moroccan
  - untested
---

## Ingredients

*   2 tbsp. olive oil
*   1 red onion, thinly sliced
*   1 fennel bulb, thinly sliced
*   2 garlic cloves, thinly sliced
*   1 1/2 lb. lamb fillet, cut into thick pieces
*   1 tsp. ground ginger
*   2 tsp. ground cumin
*   2 tsp. ground coriander
*   1/4 tsp. cayenne pepper
*   1/4 tsp. salt
*   1 c. pitted dates, chopped
*   1 1/2 c. water
*   cilantro to garnish

## Preparation

Heat 1 tbsp. of oil in a tagine on medium. Fry onion, fennel, and garlic until just beginning to brown. Transfer to a plate.

Add the rest of the oil and fry the lamb until evenly browned.

Add all the spices and the salt to the meat and stir well, cooking one minute.

Return onion, fennel, and garlic to the tagine. Add dates and 1/4 c. water. Stir well.

Cover and cook very gently, stirring occasionally, for 3-3 1/2 hours. The spices will thicken the liquid, so check after 1 1/2 hours and add the rest of the water little by little.

Serve with couscous.

_Le Creuset tagine booklet_
