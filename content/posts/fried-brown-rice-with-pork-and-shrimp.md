---
title: "Fried Brown Rice with Pork and Shrimp"
author: "pencechp"
date: "2015-11-01"
categories:
  - main course
tags:
  - chinese
  - pork
  - shrimp
  - untested
---

## Ingredients

*   2 c. short-grain brown rice
*   Salt
*   10 oz. boneless pork ribs, trimmed
*   1 tbsp. hoisin sauce
*   2 tsp. honey
*   ⅛ tsp. five-spice powder
*   Small pinch cayenne pepper
*   4 tsp. vegetable oil
*   8 oz large (26-30) shrimp, peeled, deveined, tails removed, and cut into ½-inch pieces
*   3 eggs, lightly beaten
*   1 tbsp. toasted sesame oil
*   6 scallions, white and green parts separated and sliced thin on bias
*   2 garlic cloves, minced
*   1½ tsp. grated fresh ginger
*   2 tbs. soy sauce
*   1 c. frozen peas

## Preparation

1\. Bring 3 quarts water to boil in large pot. Add rice and 2 teaspoons salt. Cook, stirring occasionally, until rice is tender, about 35 minutes. Drain well and return to pot. Cover and set aside.

2\. While rice cooks, cut pork into 1-inch pieces and slice each piece against grain 1/4 inch thick. Combine pork with hoisin, honey, five-spice powder, cayenne, and 1/2 teaspoon salt and toss to coat. Set aside.

3\. Heat 1 teaspoon vegetable oil in 12-inch nonstick skillet over medium-high heat until shimmering. Add shrimp in even layer and cook without moving them until bottoms are browned, about 90 seconds. Stir and continue to cook until just cooked through, about 90 seconds longer. Push shrimp to 1 side of skillet. Add 1 teaspoon vegetable oil to cleared side of skillet. Add eggs to clearing and sprinkle with 1/4 teaspoon salt. Using rubber spatula, stir eggs gently until set but still wet, about 30 seconds. Stir eggs into shrimp and continue to cook, breaking up large pieces of egg, until eggs are fully cooked, about 30 seconds longer. Transfer shrimp-egg mixture to clean bowl.

4\. Heat remaining 2 teaspoons vegetable oil in now-empty skillet over medium-high heat until shimmering. Add pork in even layer. Cook pork without moving it until well browned on underside, 2 to 3 minutes. Flip pork and cook without moving it until cooked through and caramelized on second side, 2 to 3 minutes. Transfer to bowl with shrimp-egg mixture.

5\. Heat sesame oil in now-empty skillet over medium-high heat until shimmering. Add scallion whites and cook, stirring frequently, until well browned, about 1 minute. Add garlic and ginger and cook, stirring frequently, until fragrant and beginning to brown, 30 to 60 seconds. Add soy sauce and half of rice and stir until all ingredients are fully incorporated, making sure to break up clumps of ginger and garlic. Reduce heat to medium-low and add remaining rice, pork mixture, and peas. Stir until all ingredients are evenly incorporated and heated through, 2 to 4 minutes. Remove from heat and stir in scallion greens. Transfer to warmed platter and serve.
