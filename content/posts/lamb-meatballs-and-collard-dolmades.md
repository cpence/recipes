---
title: "Lamb Meatballs and Collard Dolmades"
author: "pencechp"
date: "2013-06-13"
categories:
  - appetizer
  - main course
tags:
  - greek
  - greens
  - lamb
  - untested
---

## Ingredients

*   1/4 cup medium-grind bulgur
*   1 pound ground lamb
*   1 small onion, chopped
*   2 cloves garlic, minced
*   1 tablespoon ground cumin
*   1/4 cup chopped fresh mint
*   Salt and ground black pepper
*   2 tablespoons olive oil, more as needed
*   16 to 20 untorn collard leaves
*   Lemon wedges, for garnish

## Preparation

Soak bulgur in hot water to cover until tender, 15 to 30 minutes. Drain bulgur, then squeeze out as much water as possible. Combine bulgur with lamb, onion, garlic, cumin, mint, salt and pepper. Shape into 1-inch meatballs, handling mixture as little as possible.

Put olive oil in a large skillet (preferably cast iron) over medium-high heat; when hot, add meatballs and cook for 6 to 8 minutes, turning every couple of minutes. Serve immediately or cool and proceed with recipe.

Bring a large pot of water to boil and salt it. Trim stem ends of collard leaves and discard. Put half the leaves in the boiling water and cook for 1 to 2 minutes, or until they are just pliable. Use a slotted spoon to remove leaves from water and transfer to a colander; run leaves under cool water; drain and gently squeeze to remove most of the excess water, leaving them just damp enough so they will stick together when rolled. Repeat with other leaves.

Cut leaves in half by running a sharp knife along each side of stem, removing stem in process; trim top and bottom, making a large, rectangular-shaped leaf. Lay one leaf down with widest part facing you. Put a meatball in middle of leaf, bring two sides of leaves together and roll like a burrito to seal it. Put each stuffed leaf, seam side down, on a serving plate. Repeat, cooking and stuffing remaining leaves. Serve with lemon wedges.

_Serves 4_

_The New York Times_
