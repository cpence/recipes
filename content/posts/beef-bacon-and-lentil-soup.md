---
title: "Beef, Bacon, and Lentil Soup"
author: "pencechp"
date: "2011-10-21"
categories:
  - main course
tags:
  - beef
  - lentil
  - soup
---

## Ingredients

*   1 1/2 c. dried lentils
*   5 c. cold water
*   8 oz. bacon, diced
*   1 lb. ground chuck
*   1 lg. onion, chopped
*   1/2 c. chopped bell pepper
*   1 lg. carrot, chopped
*   1 lg. tomato, chopped
*   1/2 tbsp. butter
*   1/2 tbsp. flour
*   2 c. beef stock
*   1 1/2 tsp. salt
*   1/8 tsp. black pepper
*   2 tbsp. vinegar

## Preparation

Combine the lentils and water in a soup pot and bring to a boil. Lower the heat, cover, and simmer for 45 minutes.  While the lentils simmer, brown the bacon in a skillet and remove to a paper-towel lined plate to drain. Discard the bacon drippings. Add the ground chuck to the skillet and cook until nicely browned. Drain well. Add the beef, bacon, and vegetables to the lentils. Melt the butter in the skillet, blend in the flour, and then add the salt, pepper, and vinegar. Cook, stirring constantly, until thickened and bubbly, about 3 minutes. Pour in the beef stock, stir well to blend, and then add to the soup pot and bring to a boil. Lower the heat, cover, and simmer for 1 1/2 to 2 hours or until the flavors are well-blended.

_Penzey's Spices_
