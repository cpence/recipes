---
title: "Cranberry-Orange Relish"
author: "juliaphilip"
date: "2017-01-03"
categories:
  - side dish
tags:
  - cranberry
---

## Ingredients

*   1 bag fresh cranberries
*   1 whole navel orange, preferably organic, skin included, washed and cut into chunks
*   1/2 c shelled pecans
*   1/3 c mild honey, such as clover

## Preparation

Place all ingredients in a food processor and pulse, then blend until you have a uniform, finely chopped mixture with a crunchy texture. Chill until ready to serve.

_New York Times_
