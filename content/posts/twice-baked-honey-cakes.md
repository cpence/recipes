---
title: "Twice Baked Honey Cakes"
date: 2022-09-03T11:25:31+02:00
categories:
  - dessert
tags:
  - honey
  - cake
  - untested
---

## Ingredients

- 1 3/4 cup flour
- 1 1/2 tsp baking powder
- 1/2 tsp salt
- 1/2 cup (1 stick) butter, room temperature
- zest of 1 lemon
- 1/4 tsp freshly ground nutmeg
- 3/4 cup milk
- 2 eggs
- 3/4 cup honey + 1/4 cup honey for drizzling on top
- 1 tsp vanilla

## Preparation

Preheat the oven to 350F. Butter the inside of the muffin wells _extremely_
well. Set aside.

In a medium bowl, whisk together the flour, baking powder, salt, nutmeg and
lemon zest. Cut in the room temperature butter to the flour mixture until it
looks like a sandy/gravely mixture. Set aside.

In a small bowl whisk together the milk, eggs, honey, and vanilla. Pour the
liquids over the dry mixture and combine until just combined - do not over-mix.

Spoon the batter into 12 muffin wells - it will be between 1/4 cup and 1/3 cup
of batter each.

Bake for 16 minutes, or until mostly done but not quite golden enough.

Remove from the muffin pan and place the muffins on a silpat lined rimmed baking
sheet.

Warm the remaining 1/4 cup honey in the microwave for about 10 seconds.

Using a silicone pastry brush, coat the tops of the cakes with the honey. Allow
to sit for about 5 minutes to let the honey soak into the cakes.

Bake for an additional 8-10 minutes, or until the cakes are golden brown.

_The Gingered Whisk_
