---
title: "Black Lily"
author: "pencechp"
date: "2012-12-26"
categories:
  - cocktails
tags:
  - fernet
---

## Ingredients

*   1.5 oz. Cointreau
*   1 oz. Fernet Branca
*   .75 oz. Fresh Lime Juice

## Preparation

Shaken, strained over ice in a rocks glass. Garnished with an orange twist.

_Kyle Ford, via San Francisco Cocktail Week_
