---
title: "Fudge"
author: "pencechp"
date: "2011-02-07"
categories:
  - dessert
tags:
  - chocolate
---

## Ingredients

*   2 1/2 cups bittersweet chocolate chips
*   1 can sweetened condensed milk
*   3 TBSP butter
*   1 tsp vanilla
*   2 tsp cinnamon
*   (kosher salt)

## Preparation

You know what to do. Sprinkle with salt after spreading in pan.

_Giada de Laurentiis_
