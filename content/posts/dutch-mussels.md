---
title: "Dutch Mussels"
author: "pencechp"
date: "2010-07-13"
categories:
  - appetizer
  - main course
tags:
  - dutch
  - mussels
---

## Ingredients

*   4 1/2 (2 kg) pounds fresh mussels in the shell
*   6.7 ounces (200 mL) dry white wine
*   1/2 pound leeks (white and light green parts) cleaned
*   4 1/2 ounces lightly smoked streaky bacon, thickly sliced
*   1 tablespoon sunflower oil
*   1 ounce unsalted butter
*   Fine sea salt and freshly milled black pepper

## Preparation

Clean the mussels and put them in a sauteuse pan with the wine. Bring to a boil, and cook just long enough for all the shells to open. Drain and remove the mussels from the pan.

Halve the leeks lengthwise and cut them in finger-thick slices. Cut bacon slices in very small pieces. Heat the oil in a large skillet, turn down the heat to medium, add bacon, and stir-fry until the bacon begins to render its fat (don't let it get crisp).

Add leeks and stir-fry until they just begin to soften. Lower the heat, add the butter, and when melted, stir in the mussels carefully. Simmer together just long enough for the mussels to warm up. Add salt to taste and be generous with the pepper. Serve on a heated platter with warm French bread or ciabatta. Any leftover mussels can be used the next day to stuff an omelet.

Serves 4-6.

_Florine Boucher, via National Geographic_
