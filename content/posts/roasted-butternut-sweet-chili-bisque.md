---
title: "Roasted Butternut Sweet Chili Bisque"
author: "pencechp"
date: "2017-03-30"
categories:
  - main course
tags:
  - soup
  - squash
  - untested
---

## Ingredients

*   1 small butternut squash, peeled, diced
*   1 onion, chopped
*   6 garlic cloves
*   1 tablespoon olive oil
*   2 granny smith apples, peeled and chopped
*   6 cups chicken or vegetable stock
*   ½ cup heavy cream
*   ½ cup coconut milk
*   2 tablespoons chopped fresh parsley
*   2 tablespoon chopped fresh basil
*   Garnish: crème fraiche and sweet chili sauce

## Preparation

Preheat oven to 400F. Toss squash, onion and garlic with olive oil and spread in a roasting pan or baking sheet. Season with salt and pepper. Roast for 20 minutes, add diced apple, and continue roasting another 10-20 minutes.

Place squash mixture into blender with about half the chicken stock; puree. Add remaining chicken stock, cream, coconut milk and herbs; pulse until smooth. Alternatively use an immersion blender.

Return to saucepan and reheat. Serve with a dollop of crème fraiche and garnish with sweet chili.

_Edible Nashville_
