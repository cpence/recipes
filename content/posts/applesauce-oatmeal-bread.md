---
title: "Applesauce Oatmeal Bread"
author: "juliaphilip"
date: "2017-05-04"
categories:
  - bread
tags:
  - applesauce
  - oatmeal
---

## Ingredients

*   1 cup sugar
*   2 large eggs
*   1/2 cup vegetable oil
*   1 1/2 teaspoons vanilla or boiled cider
*   1/2 cup King Arthur White Whole Wheat Flour
*   1 cup King Arthur Unbleached All-Purpose Flour
*   1/4 teaspoon baking powder
*   1/2 teaspoon baking soda
*   1 1/2 teaspoons cinnamon
*   1/2 teaspoon allspice
*   1/4 teaspoon nutmeg
*   1/2 teaspoon salt
*   1/2 cup rolled oats
*   3/4 cup applesauce
*   1/2 cup chopped walnuts

## Preparation

Preheat the oven to 350°F. Lightly grease a 9" x 5" loaf pan.

In a large bowl, mix together the sugar, eggs, oil and vanilla.

In a separate bowl, mix together the flours, baking powder and soda, and spices, and add them to the wet ingredients in the bowl.

Mix in the rolled oats, applesauce, and walnuts.

Pour the mixture into the prepared pan. Bake the bread for 55 to 60 minutes, until a toothpick inserted into the center comes out clean. Remove the bread from the oven, and cool completely.

Store cooled bread, well wrapped, at room temperature for several days. Freeze for longer storage.

_King Arthur Flour_
