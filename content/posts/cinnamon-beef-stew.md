---
title: "Cinnamon Beef Stew"
author: "juliaphilip"
date: "2010-02-07"
categories:
  - main course
tags:
  - beef
  - indian
  - stew
---

## Ingredients

*   1 cup plain yogurt
*   1 tablespoon smoked paprika
*   1 teaspoon curry powder
*   2 teaspoons garam masala
*   1 tablespoon soy sauce
*   2 teaspoons powdered ginger
*   3 pounds flank steak, cut into 3/4-inch-cubes
*   2 tablespoons ghee (clarified butter) or extra-virgin olive oil
*   1 large yellow onion, finely chopped
*   1 tablespoon minced garlic
*   2 cinnamon sticks, roughly 2 inches each
*   3 bay leaves
*   2 small tomatoes, peeled, seeded and diced, or 1 small can tomatoes, drained
*   Salt, to taste

## Preparation

1. In a large bowl, combine the yogurt, paprika, curry powder, garam masala, soy
   sauce and ginger. Add the beef and turn to coat. Cover the bowl and
   refrigerate to marinate several hours or overnight.

2. When ready to cook, in a Dutch oven over medium-high, heat the ghee or
   oil. Add the onion and garlic, then saute until translucent, about 5 minutes.

3. Add the cinnamon sticks and bay leaves, and continue to cook until the onions
   brown, about another 5 minutes.

4. Add the beef, marinade and tomatoes. Increase heat to high and bring to a
   boil. Reduce heat to medium and simmer, uncovered and stirring occasionally,
   for 20 minutes.

5. Reduce heat to a low simmer, cover and cook for another 40 minutes.

6. Remove the cinnamon sticks and bay leaves, then season with salt.

_Nimala Narine_
