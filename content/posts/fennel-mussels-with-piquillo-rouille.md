---
title: "Fennel Mussels with Piquillo Rouille"
author: "pencechp"
date: "2010-12-17"
categories:
  - main course
tags:
  - fennel
  - mussels
---

## Ingredients

_Rouille:_

*   1/4 c. plus 2 tbsp. fish stock or clam broth
*   large pinch saffron threads
*   one 1/4-inch thick slice of peasant bread, crust removed
*   1 large egg yolk
*   1 jarred piquillo pepper, chopped
*   2 garlic cloves, chopped
*   2 tsp. fresh lemon juice
*   1/2 c. extra virgin olive oil
*   cayenne pepper
*   salt

_Mussels:_

*   2 tbsp. extra-virgin olive oil
*   2 heads of fennel, cored and diced, plus 1/4 c. chopped fronds
*   1 med. onion, diced
*   4 garlic cloves, minced
*   1/2 tsp. crushed red pepper
*   1/2 c. Pernod or absinthe
*   1 1/4 c. fish stock or clam broth
*   4 lb. mussels, scrubbed and debearded
*   1 tbsp. butter
*   1/2 c. chopped parsley
*   salt

## Preparation

1. Make the rouille: In a microwave-safe bowl, heat 2 tbsp. of the fish stock at high power.  Crumble in the saffron and let cool.  In a large bowl, soften the bread in the remaining 1/4 c. of fish stock.
2. In a food processor, puree the soaked bread, egg yolk, piquillo, garlic, and lemon juice until smooth.  With the machine on, slowly pour in 1/4 c. of the oil and the saffron fish stock.  Slowly pour in the remaining 1/4 c. of oil.  Season with cayenne and salt.
3. Prepare the mussels: In a large pot, heat the oil.  Add the diced fennel, onion, and garlic and cook over moderately high heat, stirring, until softened, 7 minutes.  Add the red pepper and cook for 30 seconds.  Add the Pernod and boil over high heat for 2 minutes.  Add the fish stock and bring to a boil.  Add the mussels, cover and cook, shaking the pot, until they open, 5 minutes.  Pile the mussels in a bowl; discard any that don't open.  Stir the butter into the broth, add the fennel fronds and parsley; season with salt.  Pour the broth over the mussels and serve with the rouille.

_Food and Wine, April 2010_
