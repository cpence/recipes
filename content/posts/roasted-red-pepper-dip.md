---
title: "Roasted Red Pepper Dip"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - bellpepper
  - dip
  - untested
  - vegetarian
---

## Ingredients

*   3 large red bell peppers
*   2 cloves garlic, unpeeled
*   1/2 to 1 cup ricotta cheese, drained
*   1/2 to 1 teaspoon salt

## Preparation

Heat broiler or grill. Roast peppers and garlic cloves on a foil-covered broiler rack, turning as needed, until skins blacken on all sides, about 30 minutes. Place peppers in a small paper bag (or place in a bowl and cover with plastic wrap) until cool. Remove skin and seeds. Place peppers in a food processor; squeeze roasted garlic out of skins into the processor. Pulse until nearly pureed. Add 1/2 cup ricotta cheese and salt to taste; pulse until well-blended, adding more cheese if needed for dipping consistency.

Note: This dip goes well with toasted Italian-bread slices or your favorite chips. Add more garlic or additional chopped herbs, if you like.

Makes 3/4 cup.

_Chicago Tribune_
