---
title: "Brown Rice, Tomato and Basil Salad"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - side dish
tags:
  - rice
  - tomato
  - vegan
  - vegetarian
---

## Ingredients

*   2 1/4 cups water
*   1 cup long-grain brown rice (such as Texmati)
*   2 teaspoons coarse salt
*   2 tablespoons Champagne vinegar or white wine vinegar
*   2 teaspoons sugar
*   2 tablespoons olive oil
*   1 pound tomatoes, cut into 1/2-inch pieces
*   1 cup (packed) fresh basil leaves, finely chopped

## Preparation

Bring 2 1/4 cups water to boil in heavy medium saucepan. Mix in rice and salt. Cover, reduce heat to low and simmer until rice is tender and water is absorbed, about 40 minutes. Transfer rice to large bowl; fluff with fork and cool.

Whisk vinegar and sugar in small bowl. Gradually whisk in oil. Pour over rice. Add tomatoes and basil and toss to combine. Season with salt and pepper. (Can be made 6 hours ahead. Cover; chill. Bring to room temperature before serving.)

_Bon Appetit, August 2000_
