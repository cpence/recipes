---
title: "Cilantro Rice"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - pence
  - rice
---

Cook needed amount of rice (white or brown) for the folks you are feeding.  Cool.  Toss with sour cream or yogurt to coat (you don't want it to be gummy or soupy) and a healthy quantity of chopped cilantro.  Put in a well-greased casserole and top with grated parmesan.  Cover with foil and bake 20-30 minutes.  Remove foil and cook until golden on top.
