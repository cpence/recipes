---
title: "Pumpkin-Walnut Bread"
author: "pencechp"
date: "2010-05-09"
categories:
  - breakfast
tags:
  - pumpkin
---

## Ingredients

*   2 cups all purpose flour
*   1 teaspoon baking soda
*   1 teaspoon baking powder
*   1 teaspoon salt
*   1/2 teaspoon ground cinnamon
*   1/2 teaspoon ground cloves
*   1/2 teaspoon ground ginger
*   1/2 cup (1 stick) unsalted butter, room temperature
*   3/4 cup plus 1 tablespoon sugar
*   2 large eggs, room temperature
*   1 cup canned pure pumpkin
*   1 1/2 teaspoons grated lemon peel
*   1 teaspoon vanilla extract
*   1/2 cup sour cream
*   1/2 cup whole milk
*   1 1/2 cups chopped walnuts

## Preparation

Position rack in center of oven; preheat to 325°F. Butter 9x5x3-inch metal loaf pan. Sift first 7 ingredients into medium bowl. Using electric mixer, beat butter in large bowl until light. Gradually beat in 3/4 cup sugar. Beat in eggs 1 at a time. Beat in pumpkin, lemon peel and vanilla. Whisk sour cream and milk in small bowl. Beat flour and sour cream mixtures alternately into batter in 2 additions each. Fold in nuts. Transfer batter to pan; smooth top. Sprinkle with 1 tablespoon sugar.

Bake bread until tester inserted into center comes out clean, about 1 hour 10 minutes. Cool in pan 10 minutes. Turn out onto rack; cool. (Can be made 2 days ahead. Wrap in foil; store at room temperature.)

_Bon Appetit, October 2000_
