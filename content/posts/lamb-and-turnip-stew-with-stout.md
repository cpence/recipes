---
title: "Lamb and Turnip Stew with Stout"
author: "pencechp"
date: "2012-10-28"
categories:
  - main course
tags:
  - lamb
  - stew
---

## Ingredients

*   Oil (vegetable, peanut, canola or olive, anything will work well)
*   2 pounds boneless lamb shoulder or leg, cut into 1-inch cubes
*   Kosher salt
*   4 tablespoons all-purpose flour
*   1 large or 2 small onions, cut into 1/2-inch dice
*   3 ribs celery, cut into 1/2-inch dice
*   3 turnips, peeled and cut into 1/2-inch dice
*   1/2 cup tomato puree
*   2 pints stout beer (recommended: Guinness)
*   1 small bunch marjoram, tied together with string
*   2 bay leaves
*   3 tablespoons chopped flat-leaf parsley

## Preparation

Coat a wide, large heavy-bottomed pot with oil and heat over high heat. Toss 1/2 of the lamb cubes generously with salt and half of the flour. Add immediately to the hot oil and brown well on all sides. When the lamb is really brown on all sides, remove it from the pot and reserve. If the oil begins to smoke, lower the heat and continue. Repeat this process with the remaining lamb and flour. Put all the browned lamb on a plate.

Remove the excess oil from the pot, add a little fresh oil and heat it over medium-high heat. Add the onions and celery and season with salt. Cook until the onions start to soften and are very aromatic, about 7 to 8 minutes. Add the turnips, stir to combine and cook for 2 to 3 minutes. Return the lamb to the pan and add the tomato puree, stout, marjoram, and bay leaves. Stir to combine, taste for seasoning and adjust, if needed. Bring the liquid to a boil, then reduce the heat it to a simmer. Cover the pot halfway with a lid and simmer the stew for about 1 1/2 to 2 hours, checking and stirring occasionally. Remove the lid during the last 15 to 20 minutes of the cooking time to allow the liquid to reduce and thicken. Taste and adjust the seasoning, if needed (it probably will). When it's done, the lamb will be tender and full-flavored but not falling apart or stringy. Discard the bay leaves and transfer to a serving bowl. Garnish with parsley and serve.

_Anne Burrell_
