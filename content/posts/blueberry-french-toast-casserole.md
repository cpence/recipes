---
title: "Blueberry French Toast Casserole"
author: "pencechp"
date: "2010-05-09"
categories:
  - breakfast
tags:
  - blueberry
---

## Ingredients

*   20 oz. of bread, cubed
*   8 oz. cream cheese, cubed
*   1 cup fresh or frozen blueberries
*   12 eggs, beaten
*   2 cups milk
*   1 tsp. vanilla
*   1/3 cup maple syrup
*   Powdered sugar

## Preparation

Place half the bread cubes in a greased 9x13 pan. Sprinkle cream cheese evenly over bread. Do the same with the blueberries. Cover with remaining bread.

In a large bowl, whisk eggs, milk, vanilla and syrup. Drizzle egg mixture over bread. Cover with foil and refrigerate at least 2 hours or overnight.

Heat oven to 350. Bake 30 minutes, covered. Uncover and bake 30 minutes more, or until center is firm and top is golden. Lightly dust with powdered sugar. Serve with blueberry syrup.
