---
title: "Spinach with Toasted Sesame Seeds"
author: "pencechp"
date: "2013-06-13"
categories:
  - main course
  - side dish
tags:
---

## Ingredients

*   1 tbsp. sesame seeds
*   4 tbsp. rice wine vinegar
*   1 tbsp. soy sauce
*   1 tsp. sugar
*   1-2 tbsp. vegetable oil
*   2 cloves garlic, minced
*   1 cup shredded carrots (~2 medium)
*   12 oz. fresh spinach
*   3 green onions, thinly sliced

## Preparation

In a small, non-stick pan, toast the sesame seeds over medium heat until caramel-colored, stirring frequently.  Let cool.  In a small bowl, combine the rice wine vinegar, soy sauce, and sugar and set aside.  Heat a pan over medium heat.  Add the vegetable oil and saute the garlic for 1 minute.  Add carrots and cook for another minute.  Then add the spinach a little at a time until wilted.  (Don't overcook the vegetables.)  Let cool for 2 minutes.  Pour the vinegar mixture over the vegetables and toss.  Add green onions and sesame seeds.  Serve over rice.

_Penzey's_
