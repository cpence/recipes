---
title: "Cognac"
date: 2021-12-23T01:50:14+01:00
categories:
  - cocktails
tags:
  - cognac
---

## Ingredients

- 2 oz Cognac Pierre Ferrand
- .5 oz Punt e Mes
- .25 oz Cranberry Liqueur Clear Creek
- .25 oz Sherry Lustau East India Solera
- 1 dash Bittermens Xocolatl Mole Bitters
- lemon peel

## Preparation

Build in a rocks glass. Stir, fill with ice, lemon peel garnish.

_Bittermen's_
