---
title: "Daisy"
author: "pencechp"
date: "2012-05-28"
categories:
  - cocktails
tags:
  - brandy
  - lemon
---

## Ingredients

*   3-4 dashes \[6-8 mL\] simple syrup
*   2-3 dashes \[3-6 mL\] orange cordial (Cointreau)
*   juice of 1/2 lemon
*   1 small wine-glass \[3 oz?\] brandy

## Preparation

Shake over ice, strain.  Top with soda.

_"Scientific Bar-Keeping," 1884, Joseph W. Gibson (via Webtender)_
