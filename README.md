# Recipe Site

This is our family recipe site, in the form of a Hugo blog and available at
<https://pence.envs.net/recipes>.

There's a few things here that might be of interest to someone else:

- The `bin` and `assets/js` folders include the implementation of basic
  client-side search using lunr, with indexes GZip-compressed and unpacked on
  the client side with zlibjs. All of those indices are updated automatically on
  the build branch by the `build` script in the root.
- The `layouts` folder includes some extremely simplistic layouts that might be
  useful for people who want to make their own recipe collection.

To make a new recipe, run:

```sh
hugo new content/posts/post-slug.md
```

All the metadata fields will be filled in automatically, you just have to clean
up the categories and customize the tags.

## Copyright

All code is released under the MIT license; recipe content is public domain and
cannot be copyrighted in most world jurisdictions.
